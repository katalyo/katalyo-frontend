//**************************************************************************************************
//*************************************** pinia store map - ES6 module ****************************
//**************************************************************************************************

//import all stores here
import { useAuthUserStore } from '@/stores/authUser';
import { useAlertStore,alertMessage } from '@/stores/alert';
import { usePageStore } from '@/stores/page';
import { useAppMenuStore } from '@/stores/appMenu';

let ktPiniaInstance

class PiniaStoreMap{

    constructor () {
    //map stores here
      this.mappings = {
        "authStore":useAuthUserStore,
        "alert":useAlertStore,
        "page":usePageStore,
        "appMenu":useAppMenuStore,
        
      }
     
     }
    //get store instance
    getStore(storeName) {
       return this.mappings[storeName]
    }
    
    
 }
 var KatalyoPinia  = (function(new_instance)
 {

   if (ktPiniaInstance===undefined || new_instance) ktPiniaInstance = new PiniaStoreMap()
   return ktPiniaInstance;
 });
 export default KatalyoPinia;

