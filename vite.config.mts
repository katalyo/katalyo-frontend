import { fileURLToPath, URL } from "node:url";

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import vueJsx from "@vitejs/plugin-vue-jsx";

// https://vitejs.dev/config/
export default defineConfig({
  publicDir: false,
  plugins: [vue(), vueJsx()],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },

  },
  build: {
		sourcemap: true,
		minify:false,
		rollupOptions: {
      
			external: [fileURLToPath(new URL("/config/index.json", import.meta.url)),fileURLToPath(new URL("/config/config.json", import.meta.url))],
		  
		//	input: {
		//	main: fileURLToPath(new URL("index.html", import.meta.url)),
		//	config : fileURLToPath(new URL("src/json/config.json", import.meta.url)),
		//	},
		},
	},
  __VUE_PROD_DEVTOOLS__: 'true'
});
