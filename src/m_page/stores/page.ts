import { ref, computed } from "vue"; 
import { defineStore } from "pinia";
import { useCookies } from "vue3-cookies";
import KatalyoApi from '@/g_libs/kt-api';
import { useAlertStore,alertMessage } from '@/g_stores/alert';
import { useAuthUserStore } from '@/m_auth/stores/authUser'
import { useResourceFormStore } from '@/m_resource/stores/resourceForm'
import { useResourceFlowStore } from '@/m_resource/stores/resourceFlow'
import { useApplicationStore } from '@/g_stores/application'
import { useAppMenuStore } from '@/g_stores/appMenu'
import type { Router } from 'vue-router';
//using options API for now (if there's a need switch to composition API)

/*
class pageExeState{
  instanceId: number;
  prevResourceId: number;
  defId:string;
  pageState: any; 
  
 
  constructor (_instanceId:number,_defId:string,_pState:pageExeState){
    this.instanceId = _instanceId
    this.defId = _defId 
    this.pageState = _pState
  }

}
*/
class pageDef{
  
  defId:string;
  //pState: pageExeState; 
  widgets:Record<PropertyKey,any>;
  form:Array<any>;
  constructor (page:any){
    
    let def,params
    
    if (page.definition ===undefined) throw Error('Page definition data is missing')
    else def = page.definition
    
    if (page.params ===undefined) throw Error('Page params data is missing')
    else params = page.params
    
    if (page.form ===undefined && page.pagetype!='systempage') throw Error('Page form is missing')
    else this.form = page.form

    if (def.uid !=undefined) this.defId = def.uid
    else if (params.uid) this.defId = params.uid
    else throw Error('Page definition id is not defined')  
    this.widgets={}
  }
  clearWidgets(){
    this.widgets = {}
  }

}

class pageExe{
  
  instanceId: string;
  prevResourceId: string;
  defId:string;
  pDef:pageDef;
  params:any;
  pageState: any;
  isCssCreated:boolean;
  hasPageMenu:boolean;
  pageMenuVisible:boolean;
  pageMenuId:string;
  pageMenuVersion:number;
  forbidden:boolean;
  forbiddenMsg:string;
 
  constructor (page:any){
    
    let instance,params,def
    if (page.instance ===undefined) throw Error('Page instance data is missing')
    else instance = page.instance
    
    if (page.params ===undefined) throw Error('Page params data is missing')
    else this.params = page.params

    if (page.definition ===undefined) throw Error('Page definition data is missing')
      else def = page.definition

    if (def.uid !=undefined && !def.PublicPage) this.defId = def.uid
    else if (this.params.uid) this.defId = this.params.uid
    else throw Error('Page definition id is not defined')
    
    if (instance.id !=undefined) this.instanceId = instance.id
    else throw Error('Page instance id is not defined')
  
    this.prevResourceId = instance.prevresource
    this.pageState = {}
    this.isCssCreated=false
    this.hasPageMenu=false
    this.pageMenuVisible=false
    this.pageMenuId=''
    this.pageMenuVersion=0
    this.forbidden=false
    this.forbiddenMsg=''
    this.pDef = new pageDef(page)
  }
  setPageDef(pDef:pageDef){
    this.pDef = pDef
    if (this.pDef.widgets==undefined) this.pDef.widgets={}
  }
  setPrevPage(prevPageId:string){
    this.prevResourceId = prevPageId
  }
}

export const usePageStore = defineStore({
  
  id: "page",

  state: () => ({
    
        currentPage:{} as pageExe,
        currentPageState:{} as any,
        currentPageWidgets:{} as any,
        //lastTaskExeState:{} as Record<PropertyKey,any>,
        lastResourceExeState:{} as any,
        pagesExe:{} as Record<PropertyKey,pageExe>,
        pagesDef:{} as Record<PropertyKey,pageDef>,
        pageLoadSequence: [] as Array<string>,
        fetchPageInProgress : false as boolean,
        loadCurrentPageCompleted:false as boolean,
        loadCurrentPageInProgress:false as boolean,
        openPageInProgress:false as boolean,
        processWidgetsInProgress:false as boolean,
        widgetsToLoad:[] as Array<string>,
        actionInProgress:{} as Record<PropertyKey,boolean>,
        //widgets: {} as Record<PropertyKey,any>,
        api:{} as Record<PropertyKey,any>,
        $router:{} as Router,
    }),
 
  getters: {
    getCurrentPage: (state) => {return state.currentPage},
    getCurrentPageForm: (state) => {return state.currentPage.pDef?.form},
    getCurrentPageState: (state) => {return state.currentPage.pageState},
    getCurrentPageWidgetState: (state) => {return (widgetId:string)=> state.currentPage.pageState[widgetId] ?? {}},
    getLoadCurrentPageCompleted: (state) => {return state.loadCurrentPageCompleted},
    getOpenPageInProgress: (state) => {return state.openPageInProgress},
    getPageDef: (state) => {return (pageId:string)=> state.pagesDef[pageId]},
    getPageExe: (state) => {return (pageInstanceId:number)=> state.pagesExe[pageInstanceId]},
    getPrevResourceExeState: (state) => {
      const prevPageId:string = state.currentPage.prevResourceId//state.pageLoadSequence[state.pageLoadSequence.length-1]
      return state.pagesExe[prevPageId]},
    getWidgetsOnPage: (state) => {return (pageId:string)=> state.pagesDef[pageId].widgets},
    getWidgetOnCurrentPage: (state) => {return (widgetId:string)=> state.currentPage.pDef?.widgets[widgetId]},
    getActionInProgress: (state) => {return (actionName:string)=> state.actionInProgress[actionName]},
    getWidgetOnPage: (state) => {return (pageId:string,widgetId:string)=> state.pagesDef[pageId].widgets[widgetId]},
    },

  actions: {
   
       async fetchCurrentPageById(pageId: string) {
            
            try {

                this.fetchPageInProgress=true
                const api_main = KatalyoApi()
                const api = api_main.KatalyoApiClass
                const api_response:any = await api.get(this.api.server+'resource-def-all/'+pageId+'/');   

                
                if (api_response.status===200)
                {
                  const page = api_response.data
                  //this.currentPage=page
                  //this.currentPage.pageState = this.currentPageState
                  //this.pagesDef[pageId] = this.currentPage
                }else{
                  //store error in alertStore
                  const alertStore = useAlertStore()

                  alertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status))
                }

            } catch (err:any) {
                const alertStore = useAlertStore()
                alertStore.error(new alertMessage(err.message,err.message,err.status))
            }finally{
              this.fetchPageInProgress=false
              return this.currentPage
            }
    },
    
    async setCurrentPageById(pageInstanceId: string) {
            
            try {
                this.loadCurrentPageCompleted=false
                if (this.pagesExe[pageInstanceId] !== undefined)
                {
                    if (this.currentPage?.instanceId!==undefined) this.pageLoadSequence.push(this.currentPage?.instanceId)
                    this.currentPage = this.pagesExe[pageInstanceId]
                    if (this.currentPage.pageState!==undefined) this.currentPageState = this.currentPage.pageState
                    else this.currentPageState={}
                }else{
                      //store error in alertStore
                      const alertStore = useAlertStore()
                      const errMsg = new alertMessage('Page instance not found','Page instance does not exist',500)
                      alertStore.error(errMsg)
                }
                
            } catch (err:any) {
                const alertStore = useAlertStore()
                alertStore.error(new alertMessage(err.message,err.message,err.status))

            }finally{
              this.fetchPageInProgress=false
              return this.currentPage
            }
    },
     async fetchPageDefinition(pageId: number) {
            
            try {

                this.fetchPageInProgress=true
                const api_main = KatalyoApi()
                const api = api_main.KatalyoApiClass
                const api_response:any = await api.get(this.api.server+'resource-def-all/'+pageId+'/');   

                
                if (api_response.status===200)
                {
                  this.pagesDef[pageId] = api_response.data
                 
                }else{
                  //store error in alertStore
                  const alertStore = useAlertStore()
                  const errMsg = new alertMessage(api_response.data,api_response.data,api_response.status)
                  alertStore.error(errMsg)
                }

            } catch (err:any) {
                const alertStore = useAlertStore()
                alertStore.error(new alertMessage(err.message,err.message,err.status))

            }finally{
              this.fetchPageInProgress=false
              return this.pagesDef[pageId] 
            }
    },
    async openSecuredPage(pageId: string, pageVersion:number | undefined, resourceInstanceId:string,prevResourceInstanceId:string='') {
            
      try {     
            this.loadCurrentPageCompleted=false
            if (pageId==undefined) pageId=''
            if (resourceInstanceId==undefined) resourceInstanceId='0'
            let prevResourceId=this.currentPage?.instanceId
            if (!prevResourceId && prevResourceInstanceId) prevResourceId=prevResourceInstanceId
            const applicationStore = useApplicationStore()
            
            if (this.pagesExe[resourceInstanceId] !== undefined && resourceInstanceId!='0') {
                if (this.currentPage?.instanceId!==undefined) this.pageLoadSequence.push(this.currentPage?.instanceId)
                this.currentPage = this.pagesExe[resourceInstanceId]
                if (this.currentPage.pageState!==undefined) this.currentPageState = this.currentPage.pageState
                else this.currentPageState={}
            }
            else {
              this.openPageInProgress=true
              const api_main = KatalyoApi()
              const api = api_main.KatalyoApiClass
              const api_response:any = await api.get(this.api.server+'open-secured-page-2/'+pageId+'/'+ pageVersion + '/'+ resourceInstanceId+'/'+prevResourceId+'/',applicationStore.currentEnv);   
   
              if (api_response.status===200) {
                const page = api_response.data
                //const resourceInstanceId:number = page?.instance?.id || 0
                let prevPage:any ={}
                page.instance = {id:resourceInstanceId,prevresource:prevResourceId}
                prevPage = Object.assign({prevPageId:this.currentPage.instanceId})
                if (resourceInstanceId!='0') this.pagesExe[resourceInstanceId]=new pageExe(page)
              
                if (resourceInstanceId!==undefined)   {
                    const authUser = useAuthUserStore()
                    
                    this.currentPage=this.pagesExe[resourceInstanceId]
                    this.currentPage.pageState = this.currentPageState

                    if  (page.params?.saas && page.params?.saasEnv) api_main.set_env(page.params?.saasEnv)
                    else api_main.set_env(applicationStore.currentEnv)

                    if (this.currentPage.prevResourceId===undefined || this.currentPage.prevResourceId===null) 
                          this.currentPage.setPrevPage(prevPage?.prevPageId)
                    this.pageLoadSequence.push(this.currentPage.instanceId)
                    
                    this.$router.replace({ 
                        query: { 
                          app_ref: applicationStore.getCurrentApp.uid,
                          env: applicationStore.currentEnv,
                          rinstance:resourceInstanceId,
                          prinstance:prevResourceId,
                          session_id: authUser.currentUser?.sessionid} 
                        })
                    
                        // here get page menu exe
                    let type:string = '1'
                    let menu_id:string =page.definition?.ResourceExtended?.rlinks[0]?.Resource2
                    let m_ver:number = page.definition?.ResourceExtended?.rlinks[0]?.Resource2_Version ?? 0

                    if (menu_id)
                      {
                        if (this.currentPage.pageMenuId!=menu_id)
                        {
                          const menuStore = useAppMenuStore()
                          if (menuStore.getMenu(menu_id)==undefined){
                              const page_menu = await menuStore.fetchMenuExe(menu_id,type,m_ver,'page');
                              
                             
                            }
                            this.currentPage.hasPageMenu = true
                            this.currentPage.pageMenuVisible = false
                            this.currentPage.pageMenuId=menu_id 
                            this.currentPage.pageMenuVersion=m_ver 
                        }
                      }
                    
                }
               
               } else if (api_response.status==403) {

                this.currentPage.forbidden=true
                this.currentPage.forbiddenMsg=api_response.data
               }
               
               else{  //store error in alertStore
                  const alertStore = useAlertStore()
                  const errMsg = new alertMessage(api_response.data,api_response.data,api_response.status)
                  alertStore.error(errMsg)
              }
            }
          
      } catch (err:any) {
          const alertStore = useAlertStore()
          alertStore.error(new alertMessage(err.message,err.message,err.status))

      } finally{
        this.openPageInProgress=false
        this.loadCurrentPageCompleted=true
        return this.currentPage.instanceId
      }
},
async openSaaSPage(pageId: string, pageVersion:number | undefined, resourceInstanceId:string) {
            
  try {     
        this.loadCurrentPageCompleted=false
        if (pageId==undefined) pageId=''
        if (resourceInstanceId==undefined) resourceInstanceId='0'
        let prevResourceId=this.currentPage?.instanceId

        if (this.pagesExe[resourceInstanceId] !== undefined && resourceInstanceId!='0') {
            if (this.currentPage?.instanceId!==undefined) this.pageLoadSequence.push(this.currentPage?.instanceId)
            this.currentPage = this.pagesExe[resourceInstanceId]
            if (this.currentPage.pageState!==undefined) this.currentPageState = this.currentPage.pageState
            else this.currentPageState={}
        }
        else {
          this.openPageInProgress=true
          const api_main = KatalyoApi()
          const api = api_main.KatalyoApiClass
          const api_response:any = await api.get(this.api.server+'open-saas-page/'+pageId+'/'+ pageVersion + '/'+ resourceInstanceId+'/'+prevResourceId+'/');   

          if (api_response.status===200) {
            const page = api_response.data
            //const resourceInstanceId:number = page?.instance?.id || 0
            let prevPage:any ={}
            page.instance = {id:resourceInstanceId,prevresource:prevResourceId}
            prevPage = Object.assign({prevPageId:this.currentPage.instanceId})
            if (resourceInstanceId!='0') this.pagesExe[resourceInstanceId]=new pageExe(page)
            
            if (resourceInstanceId!==undefined)   {
                const authUser = useAuthUserStore()
                const applicationStore = useApplicationStore()
                this.currentPage=this.pagesExe[resourceInstanceId]
                this.currentPage.pageState = this.currentPageState
                if (this.currentPage.prevResourceId===undefined || this.currentPage.prevResourceId===null) 
                      this.currentPage.setPrevPage(prevPage?.prevPageId)
                this.pageLoadSequence.push(this.currentPage.instanceId)
                
                this.$router.replace({ 
                    query: { 
                      app_ref: applicationStore.getCurrentApp.uid,
                      env: applicationStore.currentEnv,
                      rinstance:resourceInstanceId,
                      prinstance:prevResourceId,
                      session_id: authUser.currentUser?.sessionid} 
                    })
                
                    // here get page menu exe
                let type:string = '1'
                let menu_id:string =page.definition?.ResourceExtended?.rlinks[0]?.Resource2
                let m_ver:number = page.definition?.ResourceExtended?.rlinks[0]?.Resource2_Version ?? 0

                if (menu_id)
                  {
                    if (this.currentPage.pageMenuId!=menu_id)
                    {
                      const menuStore = useAppMenuStore()
                      if (menuStore.getMenu(menu_id)==undefined){
                          const page_menu = await menuStore.fetchMenuExe(menu_id,type,m_ver,'page');
                          
                         
                        }
                        this.currentPage.hasPageMenu = true
                        this.currentPage.pageMenuVisible = false
                        this.currentPage.pageMenuId=menu_id 
                        this.currentPage.pageMenuVersion=m_ver 
                    }
                  }
                
            }
           
           } else{  //store error in alertStore
              const alertStore = useAlertStore()
              const errMsg = new alertMessage(api_response.data,api_response.data,api_response.status)
              alertStore.error(errMsg)
          }
        }
      
  } catch (err:any) {
      const alertStore = useAlertStore()
      alertStore.error(new alertMessage(err.message,err.message,err.status))

  } finally{
    this.openPageInProgress=false
    this.loadCurrentPageCompleted=true
    return this.currentPage.instanceId
  }
},
async openPublicPage(orgId:number,pageId: string, pageVersion:number | undefined, resourceInstanceId:string,prevResourceInstanceId:string='') {
            
  try {     
        this.loadCurrentPageCompleted=false
        if (pageId==undefined) pageId=''
        if (resourceInstanceId==undefined) resourceInstanceId='0'
        let prevResourceId=this.currentPage?.instanceId || '0'
        if (!prevResourceId && prevResourceInstanceId) prevResourceId=prevResourceInstanceId
        
        if (this.pagesExe[resourceInstanceId] !== undefined && resourceInstanceId!='0') {
            if (this.currentPage?.instanceId!==undefined) this.pageLoadSequence.push(this.currentPage?.instanceId)
            this.currentPage = this.pagesExe[resourceInstanceId]
            if (this.currentPage.pageState!==undefined) this.currentPageState = this.currentPage.pageState
            else this.currentPageState={}
        }
        else {
          this.openPageInProgress=true
          const api_main = KatalyoApi()
          const api = api_main.KatalyoApiClass
          const api_response:any = await api.get(this.api.server+'open-public-page/'+orgId+'/'+pageId+'/'+ pageVersion + '/'+ resourceInstanceId+'/'+prevResourceId+'/');   

          if (api_response.status===200) {
            const page = api_response.data
            //const resourceInstanceId:number = page?.instance?.id || 0
            let prevPage:any ={}
            //page.instance = {id:resourceInstanceId,prevresource:prevResourceId}
            prevPage = Object.assign({prevPageId:this.currentPage.instanceId})
            if (resourceInstanceId!='0') this.pagesExe[resourceInstanceId]=new pageExe(page)
            
            if (resourceInstanceId!==undefined)   {
                const authUser = useAuthUserStore()
                const applicationStore = useApplicationStore()
                this.currentPage=this.pagesExe[resourceInstanceId]
                this.currentPage.pageState = this.currentPageState
                if (this.currentPage.prevResourceId===undefined || this.currentPage.prevResourceId===null) 
                      this.currentPage.setPrevPage(prevPage?.prevPageId)
                this.pageLoadSequence.push(this.currentPage.instanceId)

                authUser.setUserData(page.api_user,api_main)
                
                this.$router.replace({ 
                    query: { 
                      app_ref: applicationStore.getCurrentApp.uid,
                      env: applicationStore.currentEnv,
                      rinstance:resourceInstanceId,
                      session_id: authUser.currentUser?.sessionid} 
                    })
                
                    // here get page menu exe
                let type:string = '1'
                let menu_id:string =page.definition?.ResourceExtended?.rlinks[0]?.Resource2
                let m_ver:number = page.definition?.ResourceExtended?.rlinks[0]?.Resource2_Version ?? 0

                if (menu_id)
                  {
                    if (this.currentPage.pageMenuId!=menu_id)
                    {
                      const menuStore = useAppMenuStore()
                      if (menuStore.getMenu(menu_id)==undefined){
                          const page_menu = await menuStore.fetchMenuExe(menu_id,type,m_ver,'page');
                          
                         
                        }
                        this.currentPage.hasPageMenu = true
                        this.currentPage.pageMenuVisible = false
                        this.currentPage.pageMenuId=menu_id 
                        this.currentPage.pageMenuVersion=m_ver 
                    }
                  }
                
            }
           
           } else{  //store error in alertStore
              const alertStore = useAlertStore()
              const errMsg = new alertMessage(api_response.data,api_response.data,api_response.status)
              alertStore.error(errMsg)
          }
        }
      
  } catch (err:any) {
      const alertStore = useAlertStore()
      alertStore.error(new alertMessage(err.message,err.message,err.status))

  } finally{
    this.openPageInProgress=false
    this.loadCurrentPageCompleted=true
    return this.currentPage.instanceId
  }
},
async checkResourceAccess(pageId: string, pageVersion:number | undefined, resourceInstanceId:string,prevResourceInstanceId:string='') {
  
  let hasAccess=false
  try {     
        this.loadCurrentPageCompleted=false
        if (pageId==undefined) pageId=''
        if (resourceInstanceId==undefined) resourceInstanceId='0'
        let prevResourceId=this.currentPage?.instanceId
        if (!prevResourceId && prevResourceInstanceId) prevResourceId=prevResourceInstanceId
        const applicationStore = useApplicationStore()
        
        if (this.pagesExe[resourceInstanceId] !== undefined && resourceInstanceId!='0') {
            if (this.currentPage?.instanceId!==undefined) this.pageLoadSequence.push(this.currentPage?.instanceId)
            this.currentPage = this.pagesExe[resourceInstanceId]
            if (this.currentPage.pageState!==undefined) this.currentPageState = this.currentPage.pageState
            else this.currentPageState={}
        }
        else {
          this.openPageInProgress=true
          const api_main = KatalyoApi()
          const api = api_main.KatalyoApiClass
          const api_response:any = await api.get(this.api.server+'check-resource-access/'+pageId+'/'+ pageVersion + '/'+ resourceInstanceId+'/'+prevResourceId+'/',applicationStore.currentEnv);   

          if (api_response.status===200) {
            const page:any = {pagetype:'systempage',definition:{id:pageId},params:{uid:pageId}}
            hasAccess = true
            //const resourceInstanceId:number = page?.instance?.id || 0
            let prevPage:any ={}
            page.instance = {id:resourceInstanceId,prevresource:prevResourceId}
            prevPage = Object.assign({prevPageId:this.currentPage.instanceId})
            if (resourceInstanceId!='0') this.pagesExe[resourceInstanceId]=new pageExe(page)
          
            if (resourceInstanceId!==undefined)   {
                const authUser = useAuthUserStore()
                
                this.currentPage=this.pagesExe[resourceInstanceId]
                this.currentPage.pageState = this.currentPageState

                if  (page.params?.saas && page.params?.saasEnv) api_main.set_env(page.params?.saasEnv)
                else api_main.set_env(applicationStore.currentEnv)

                if (this.currentPage.prevResourceId===undefined || this.currentPage.prevResourceId===null) 
                      this.currentPage.setPrevPage(prevPage?.prevPageId)
                this.pageLoadSequence.push(this.currentPage.instanceId)
                
                this.$router.replace({ 
                    query: { 
                      app_ref: applicationStore.getCurrentApp.uid,
                      env: applicationStore.currentEnv,
                      rinstance:resourceInstanceId,
                      prinstance:prevResourceId,
                      session_id: authUser.currentUser?.sessionid} 
                    })
 
            }
           
           } else if (api_response.status==403) {

            this.currentPage.forbidden=true
            this.currentPage.forbiddenMsg=api_response.data
           }
           
           else{  //store error in alertStore
              const alertStore = useAlertStore()
              const errMsg = new alertMessage(api_response.data,api_response.data,api_response.status)
              alertStore.error(errMsg)
          }
        }
      
  } catch (err:any) {
      const alertStore = useAlertStore()
      alertStore.error(new alertMessage(err.message,err.message,err.status))

  } finally{
    this.openPageInProgress=false
    this.loadCurrentPageCompleted=true
    return hasAccess
  }
},
togglePageMenuVisibility() {

  this.currentPage.pageMenuVisible = !this.currentPage.pageMenuVisible

},
    addWidgetToPage(widgetId:string,widgetRef: Record<PropertyKey,any>) {
            
            try {

              if (this.currentPage.pDef!=undefined)
              {
                if (this.currentPage.pDef?.widgets===undefined) this.currentPage.pDef.clearWidgets()

                this.currentPage.pDef.widgets[widgetId] = widgetRef
                //this.currentPageWidgets[widgetId] = widgetRef
                this.widgetsToLoad.push(widgetId)
              }

            } catch (err:any) {
                const alertStore = useAlertStore()
                alertStore.error(new alertMessage(err.message,err.message,err.status))

            }
    },
    addWidgetsToPage(widgets:Array<any>) {
            
      try {
        let i:number
        for (i=0;i<widgets.length;i++)
          {
            this.addWidgetToPage(widgets[i].UuidRef,widgets[i])
          }

      } catch (err:any) {
          const alertStore = useAlertStore()
          alertStore.error(new alertMessage(err.message,err.message,err.status))

      }
},
    resetCurrentPageWidgets() {
            
            try {
              if (this.currentPage.pDef!=undefined)
              {
                if (this.currentPage.pDef.widgets===undefined) this.currentPage.pDef.clearWidgets()
               
              }
              this.currentPageWidgets= {}
              this.widgetsToLoad = []

            } catch (err:any) {
                const alertStore = useAlertStore()
                alertStore.error(new alertMessage(err.message,err.message,err.status))

            }
    },

    setState(widgetId:string,widgetState:any) {
            
      try {
    
            
            if (this.currentPage.pageState===undefined) this.currentPage.pageState={}
            //Object.assign(this.currentPage.pageState,{[widgetId]:widgetState})
            const newState:any = Object.assign({},widgetState)
             this.currentPage.pageState[widgetId]=newState
           // this.pagesExe[this.currentPage.instanceId][widgetId]= widgetState

      } catch (err:any) {
        
        const alertStore = useAlertStore();
        alertStore.error(new alertMessage(err.message,err.message,err.status))
    
      }
    },

    setWidgetLoaded(widgetId: string) {
            
            try {
                this.loadCurrentPageCompleted=true
                //if (this.widgetsToLoad.length>0) this.loadCurrentPageCompleted=false
                if (widgetId!==undefined) this.widgetsToLoad = this.widgetsToLoad.filter(element => !widgetId.includes(element))
                //this.widgetsToLoad.splice(this.widgetsToLoad.indexOf(widgetId),1)
                if (this.widgetsToLoad.length===0) this.loadCurrentPageCompleted=true

            } catch (err:any) {
                const alertStore = useAlertStore()
                alertStore.error(new alertMessage(err.message,err.message,err.status))
            }
    },
    /*
    setPageInstance(instanceId:number) {
            
      try {

          this.currentPage.instanceId = instanceId 
      } catch (err:any) {
          const alertStore = useAlertStore()
          alertStore.error(new alertMessage(err.message,err.message,err.status))
      }
},
*/
setLastPageState(pageInstanceId:string,widgetId:string,resourceExeState: Record<PropertyKey,any>) {
            
  try {

    if (resourceExeState==undefined) resourceExeState={}
     
      this.lastResourceExeState[pageInstanceId] = resourceExeState
      this.lastResourceExeState[pageInstanceId].widgetId = widgetId
    
  } catch (err:any) {
      const alertStore = useAlertStore()
      alertStore.error(new alertMessage(err.message,err.message,err.status))
  }
},

    async processWidgets(flowId:string) {
            
      let fnResult:any={}
            
      try {

            
            this.processWidgetsInProgress=true
            
            //form validation

            const formStore = useResourceFormStore()
            let is_invalid:boolean = false
            let form_data:any = {resourceData:{}}
            let form_widgets:any = formStore.getFormWidgets(this.currentPage.instanceId) || {}
            let invalidWidgetKey:string = ''
            for (const key in form_widgets)
            {
              if (!formStore.getSkipProcess(key, this.currentPage.instanceId)) {

                is_invalid=formStore.validateForm(key, this.currentPage.instanceId)
              
                if (is_invalid) 
                  
                {
                  invalidWidgetKey = key
                  break
                }
                else{
                  if (formStore.formData[this.currentPage.instanceId][key]?.data) form_data.resourceData[key]=[formStore.formData[this.currentPage.instanceId][key]?.data]
                }
              }
            }
            if (!is_invalid)
            {
              const api_main = KatalyoApi()
              const api = api_main.KatalyoApiClass
              
              const userStore = useAuthUserStore()
              const lang:number = userStore.getCurrentUser?.lang?.id
              if (this.currentPage.prevResourceId ==undefined) this.currentPage.prevResourceId = '0'
              //ToDo - get page version here (for now hardcoded)
              const api_response:any = await api.post(this.api.server+'process-widgets/'+this.currentPage.defId+'/'+this.currentPage.instanceId+'/0/',form_data);   

              const alertStore = useAlertStore()
              if (api_response.status===200)
              {
                
               // this.taskWidgets[widgetId].taskExeResponse = api_response.data
               let rMsg:string='Process widgets executed successfuly!'
               if (api_response.data?.msg) rMsg=api_response.data?.msg
               alertStore.success(new alertMessage(rMsg,rMsg,api_response.status))
               fnResult = {status:0, result:api_response, status_message:'ok', actionIdx:0}
               
               }else{
                //store error in alertStore            
                alertStore.error(new alertMessage(api_response.data.msg,api_response.data.data,api_response.status))
                fnResult = {status:-1, result:null, status_message:'Nok', actionIdx:0}
              }
            }else{
              
              const alertStore = useAlertStore()
              alertStore.warning(new alertMessage(form_widgets[invalidWidgetKey]?.ErrorMsg || 'Form validation failed','Form validation failed',400,'pageStore.processWidgets'))
              fnResult = {status:-1,result:null,status_message:'ok',actionIdx:0}
            }

            for (const key in form_widgets)
            {
              const callbacks:any = formStore.getCallBackFn(key, this.currentPage.instanceId)
              if (fnResult.status==0 && callbacks && callbacks['processWidgetsSuccess']) {
                callbacks['processWidgetsSuccess'](fnResult.result)
                 
              }else if (fnResult.status!=0 && callbacks && callbacks['processWidgetsError']) {
  
                callbacks['processWidgetsError'](fnResult.result)
              }
            }

      } catch (err:any) {
          const alertStore = useAlertStore()
          alertStore.error(new alertMessage(err.message,err.message,err.status))
          fnResult = {status:-1,result:null,status_message:'nok',actionIdx:0}

      }finally{
        this.processWidgetsInProgress=false
       
        return fnResult
      }
    },
  }
});