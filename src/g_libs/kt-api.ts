/**
 * Katalyo api class (kt-api.ts) implements REST functionality in Katalyo platform using fetch
 * @license MIT 
 * @build: 2021-12-14
 * @version 0.1
 * https://gitlab.com/katalyo/kt-api
 * Copyright (c) 2021 Katalyo
**/

 'use strict';
        
          /**
         * Class KatalyoApi is a class where all parameters for fetch calls are defined
         * @since 0.1
         */
        let ktApi:KatalyoApiMain
        
        class KatalyoApiMain {
            KatalyoApiClass: KatalyoApiTokenAuth | KatalyoApiSso
            //env:string | null

            constructor() {
              this.KatalyoApiClass = new KatalyoApiTokenAuth();
             // this.env=null
              //this.KatalyoApiSso = null;     
            };
            
            
            add_auth_token(csrf_token:string,auth_data:string)
            {
                this.KatalyoApiClass.set_csrf(csrf_token); 
                this.KatalyoApiClass.set_auth_data(auth_data); 
            }

            set_env(env:string | number)
            {
                if (env) this.KatalyoApiClass.set_env(env.toString())
            }

            add_auth_sso(csrf_token:string)
            {
             this.KatalyoApiClass = new KatalyoApiSso(csrf_token); 
            }

            get_env()
            {
                return this.KatalyoApiClass.env
            }
            get_csrf()
            {
                return this.KatalyoApiClass.csrf
            }
            get_auth_data()
            {
                return this.KatalyoApiClass.auth_data
            }
            set_response_handlers(resp_handlers:any)
            {
                this.KatalyoApiClass.set_response_handlers(resp_handlers)
             
                
            }
        
        };
        
        /**
         * Class KatalyoApiSso is a class for sending fetch using sso
         * @since 0.1
         * @method add_header is used to add new headers
         * @method send is used to send the call to the server 
         */
        
        class KatalyoApiSso {
            url:string=''
            env:string | undefined
            method:string = 'GET'
            headers:Record<PropertyKey,string>={}
            csrf:string | undefined
            auth_data:string | undefined
            response_handlers:any= {}

            constructor(csrf_token:string) {
                this.headers['X-CSRFToken'] = csrf_token;
                this.headers['Content-Type']= 'application/json';
            };
            
            
            add_header(header:Record<PropertyKey,string>)
            {
                for (let key in header) {
                    if (header.hasOwnProperty(key)) this.headers[key] = header[key];
                }
        
            }

            set_env(env:string)
            {
                this.env=env
            }

            set_header(header:string,header_value:string)
            {
                this.headers[header]= header_value;
            }
            get_csrf()
            {
                return this.csrf;
            }
            set_csrf(csrf_token:string)
            {
                this.csrf = csrf_token;
                this.headers['X-CSRFToken'] = csrf_token;
            }
            set_auth_data(auth_data:string)
            {
                this.auth_data=auth_data
                
            }

            set_response_handlers(resp_handlers:any)
            {
                this.response_handlers = resp_handlers
             
                
            }
            async delete(url:string,env:number | string='')
            {
                if (env){
                    url+= url.indexOf('?') >= 0 ?  '&' : '?';
                    url+='env='+env.toString();
                }
                else if (this.env) {
                    url+= url.indexOf('?') >= 0 ?  '&' : '?';
                    url+='env='+this.env;
                }
                
              return fetch(url,{
                                headers: this.headers,
                                method: 'DELETE'
                                })
                                .then(async response => 
                                    {
                                        const contentType = response?.headers?.get('Content-Type')
                                        let resp:Record<PropertyKey,any> = {}
                                        if (contentType==='text/html' || contentType==='text/plain')   
                                        {
                                            resp.data = await response.text()
                                            resp.status = response.status
                                        }
                                        else  if (contentType==='application/json')    
                                        {
                                            resp.data = await response.json()
                                            resp.status = response.status
                                            if (resp.status==401) resp.data=resp.data.detail   //special case for 401 error
                                        }
                                        else resp = response
                                        
                                        if (this.response_handlers[resp?.status]) this.response_handlers[resp?.status](resp)

                                        return resp;
                                })
                                .catch(error => {
                                    return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }
                                })
            }
            
            async get(url:string)
            {
                  
              return fetch(url,{
                                headers: this.headers,
                                method: 'GET' })
                                .then(async response =>
                                     {
                                        const contentType:string | null = response?.headers?.get('Content-Type')
                                        let resp:Record<PropertyKey,any> = {}
                                        if (contentType==='text/html' || contentType==='text/plain')   
                                        {
                                            resp.data = await response.text()
                                            resp.status = response.status
                                        }
                                        else  if (contentType==='application/json')    
                                        {
                                            
                                            resp.data = await response.json()
                                            resp.status = response.status
                                        
                                            if (resp.status==401) resp.data=resp.data.detail   //special case for 401 error
                                        }
                                        else resp = response

                                        if (this.response_handlers[resp?.status]) this.response_handlers[resp?.status](resp)
                                        
                                        return resp;

                                })
                                .catch(error => {
                                    return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }
                                })
            }
            
            async get_json(url:string)
            {

              return fetch(url,{
                               // headers: this.headers,
                                method: 'GET',
                                //cache:'force-cache'
                                })
                                .then(async response => {
                                        // handle the response
                                const contentType = response?.headers?.get('Content-Type')
                                let resp:Record<PropertyKey,any> = {}
                                if (contentType==='application/json')    
                                {
                                    resp.data = await response.json()
                                    resp.status = response.status
                                }
                                else resp = response
                                
                                if (this.response_handlers[resp?.status]) this.response_handlers[resp?.status](resp)

                                return resp;
                                })
                                .catch(error => {
                                    return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }
                                })
            }


            async get_css(url:string)
            {

              return fetch(url,{
                               // headers: this.headers,
                                method: 'GET',
                                //cache:'force-cache'
                                })
                                .then(async response => {
                                        // handle the response
                                const contentType = response?.headers?.get('Content-Type')
                                let resp:Record<PropertyKey,any> = {}
                                if (contentType==='text/css')    
                                {
                                    resp.data = await response.text()
                                    resp.status = response.status
                                }
                                else resp = response
                                
                                if (this.response_handlers[resp?.status]) this.response_handlers[resp?.status](resp)

                                return resp;
                                })
                                .catch(error => {
                                    return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }
                                })
            }
            
            async post(url:string,post_data:Record<PropertyKey,any>) 
            {
                url+= url.indexOf('?') >= 0 ?  '&' : '?'

                return fetch(url,{
                                headers: this.headers,
                                method: 'POST',
                                credentials:'include',
                                body: JSON.stringify(post_data)
                                })
                    .then(async response => {
                                // handle the response
                                const contentType = response?.headers?.get('Content-Type')
                                let resp:Record<PropertyKey,any> = {}
                                if (contentType==='text/html' || contentType==='text/plain'){   
                                    resp.data = await response.text()
                                    resp.status = response.status
                                }
                                else  if (contentType==='application/json')    {
                                    resp.data = await response.json()
                                    resp.status = response.status
                                }else resp = response
                                
                                if (this.response_handlers[resp?.status]) this.response_handlers[resp?.status](resp)

                                return resp;
                                })
                    .catch(error => {
                                   
                                        return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }
                            
                                })
            
            }
            
            
            
          async fake_post(err_code:string) //SIMULATE
            {
                let url:string = "https://httpstat.us/"+ err_code   
                return fetch(url,{  headers: this.headers,    method: 'GET',   })
                .then(response => {        return response;         })
                    .catch(error => {
                                        return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }                         
                                })
            }
            
            async put(url:string,put_data:Record<PropertyKey,any>) 
            {
                   
                return fetch(url,{
                                headers: this.headers,
                                method: 'PUT',
                                body: JSON.stringify(put_data)
                                })
                    .then(async response => {
                                     // handle the response
                                const contentType = response?.headers?.get('Content-Type')
                                let resp:Record<PropertyKey,any> = {}
                                if (contentType==='text/html' || contentType==='text/plain') {
                                    resp.data = await response.text()
                                    resp.status = response.status
                                }
                                else  if (contentType==='application/json')  {
                                    resp.data = await response.json()
                                    resp.status = response.status
                                }else resp = response
                               
                                if (this.response_handlers[resp?.status]) this.response_handlers[resp?.status](resp)

                                return resp;
                                })
                    .catch(error => {
                                   
                                       return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }
                            
                                })
            
            }
            async send(url:string,method:string,post_data:Record<PropertyKey,any>)
            {
              
              return fetch(url,{
                            headers: this.headers,
                            method: method,
                            body: JSON.stringify(post_data)
                                })
                                .then(async response => {
                                        // handle the response
                                const contentType = response?.headers?.get('Content-Type')
                                let resp:Record<PropertyKey,any> = {}
                                if (contentType==='text/html' || contentType==='text/plain') {
                                    resp.data = await response.text()
                                    resp.status = response.status
                                }
                                else  if (contentType==='application/json') {
                                    resp.data = await response.json()
                                    resp.status = response.status
                                }else resp = response
                                
                                if (this.response_handlers[resp?.status]) this.response_handlers[resp?.status](resp)

                                return resp;
                                     
                                })
                                .catch(error => {
                                    
                                   return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }
                                  
                                })
            }
        
        };
        
        /**
         * Class KatalyoApiTokenAuth is a class for sending fetch using Token auth
         * @since 0.1
         * @method add_header is used to add new headers
         * @method send is used to send the call to the server 
         */
        
        class KatalyoApiTokenAuth {
            url:string=''
            method:string = 'GET'
            env:string | undefined
            csrf:string | undefined
            auth_data:string | undefined
            headers:any = {}
            response_handlers:any= {}
            
            constructor(csrf_token?:string, auth_data?:string,env?:string) {
                this.headers['X-CSRFToken'] = csrf_token;
                this.headers['Authorization'] = auth_data;
                this.headers['Content-Type']= 'application/json';
                this.env = env;
                this.csrf = csrf_token
                this.auth_data=auth_data
            };
            
            add_header(header:Record<PropertyKey,string>)
            {
               
                 for (let key in header) {
                    if (header.hasOwnProperty(key)) this.headers[key] = header[key];
                }
            }
            
            set_header(header:string,header_value:string)
            {
                this.headers[header]= header_value;
            }
            get_csrf()
            {
                return this.csrf;
            }

            set_env(env:string)
            {
                this.env=env
            }

            get_auth_data()
            {
                return this.auth_data;
            }

            set_csrf(csrf_token:string)
            {
                this.csrf = csrf_token;
                this.headers['X-CSRFToken'] = csrf_token;
            }
            
            set_auth_data(auth_data:string)
            {
                this.auth_data=auth_data
                this.headers['Authorization'] = auth_data;
                
            }

            set_response_handlers(resp_handlers:any)
            {
                this.response_handlers = resp_handlers
             
                
            }

            
            
            async get(url:string,env:number | string='')
            {   
                if (env){
                    url+= url.indexOf('?') >= 0 ?  '&' : '?';
                    url+='env='+env.toString();
                }
                else if (this.env) {
                    url+= url.indexOf('?') >= 0 ?  '&' : '?';
                    url+='env='+this.env;
                }
                
              return fetch(url,{
                                headers: this.headers,
                                method: 'GET'
                                })
                                .then(async response => 
                                    {
                                        const contentType = response?.headers?.get('Content-Type')
                                        let resp:Record<PropertyKey,any> = {}
                                       
                                        if (contentType==='text/html' || contentType==='text/plain')   
                                        {
                                            resp.data = await response.text()
                                            resp.status = response.status
                                        }
                                        else  if (contentType==='application/json')    
                                        {
                                            resp.data = await response.json()
                                            resp.status = response.status
                                            if (resp.status==401) resp.data=resp.data.detail   //special case for 401 error
                                        }
                                        else resp = response

                                        if (this.response_handlers[resp?.status]) this.response_handlers[resp?.status](resp)
                                        
                                        return resp;
                                })
                                .catch(error => {
                                    return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }
                                })
            }

            async delete(url:string,env:number | string ='')
            {   
                if (env){
                    url+= url.indexOf('?') >= 0 ?  '&' : '?';
                    url+='env='+env.toString();
                }
                else if (this.env) {
                    url+= url.indexOf('?') >= 0 ?  '&' : '?';
                    url+='env='+this.env;
                }
                
              return fetch(url,{
                                headers: this.headers,
                                method: 'DELETE'
                                })
                                .then(async response => 
                                    {
                                        const contentType = response?.headers?.get('Content-Type')
                                        let resp:Record<PropertyKey,any> = {}
                                        if (contentType==='text/html' || contentType==='text/plain')   
                                        {
                                            resp.data = await response.text()
                                            resp.status = response.status
                                        }
                                        else  if (contentType==='application/json')    
                                        {
                                            resp.data = await response.json()
                                            resp.status = response.status
                                            if (resp.status==401) resp.data=resp.data.detail   //special case for 401 error
                                        }
                                        else resp = response
                                        
                                        if (this.response_handlers[resp?.status]) this.response_handlers[resp?.status](resp)

                                        return resp;
                                })
                                .catch(error => {
                                    return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }
                                })
            }
            
            async get_json(url:string,env:number | string='')
            {   
                if (env){
                    url+= url.indexOf('?') >= 0 ?  '&' : '?';
                    url+='env='+env.toString();
                }
                else if (this.env) {
                    url+= url.indexOf('?') >= 0 ?  '&' : '?';
                    url+='env='+this.env;
                }
                
                
              return fetch(url,{
                               // headers: this.headers,
                                method: 'GET',
                                //cache:'force-cache'
                                })
                                .then(async response => {
                                        // handle the response
                                const contentType = response?.headers?.get('Content-Type')
                                let resp:Record<PropertyKey,any> = {}
                                if (contentType==='application/json')    
                                {
                                    resp.data = await response.json()
                                    resp.status = response.status
                                }
                                else resp = response
                                
                                if (this.response_handlers[resp?.status]) this.response_handlers[resp?.status](resp)
                                
                                return resp;
                                })
                                .catch(error => {
                                    return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }
                                })
            }
            async get_css(url:string,env:number | string='')
            {   
                if (env){
                    url+= url.indexOf('?') >= 0 ?  '&' : '?';
                    url+='env='+env.toString();
                }
                else if (this.env) {
                    url+= url.indexOf('?') >= 0 ?  '&' : '?';
                    url+='env='+this.env;
                }
                
              return fetch(url,{
                               // headers: this.headers,
                                method: 'GET',
                                //cache:'force-cache'
                                })
                                .then(async response => {
                                        // handle the response
                                const contentType = response?.headers?.get('Content-Type')
                                let resp:Record<PropertyKey,any> = {}
                                if (contentType==='text/css')    
                                {
                                    resp.data = await response.text()
                                    resp.status = response.status
                                }
                                else resp = response
                                
                                if (this.response_handlers[resp?.status]) this.response_handlers[resp?.status](resp)

                                return resp;
                                })
                                .catch(error => {
                                    return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }
                                })
            }
           async post(url:string,post_data:Object,env:number | string='') 
            {   
                if (env){
                    url+= url.indexOf('?') >= 0 ?  '&' : '?';
                    url+='env='+env.toString();
                }
                else if (this.env) {
                    url+= url.indexOf('?') >= 0 ?  '&' : '?';
                    url+='env='+this.env;
                }
                return fetch(url,{
                                headers: this.headers,
                                method: 'POST',
                                credentials: 'include',
                                body: JSON.stringify(post_data)
                                })
                    .then(async response => {
                                // handle the response
                                const contentType:string | null = response?.headers?.get('Content-Type')
                                let resp:Record<PropertyKey,any> = {}
                                if (contentType==='text/html' || contentType==='text/plain'){
                                    resp.data = await response.text()
                                    resp.status = response.status
                                }
                                else  if (contentType==='application/json')  {
                                    resp.data = await response.json()
                                    resp.status = response.status
                                }else resp = response
                                
                                if (this.response_handlers[resp?.status]) this.response_handlers[resp?.status](resp)

                                return resp;
                                })
                    .catch(error => {
                                   
                                        return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }
                                })
            }
            
            
             async fake_post(err_code:string) //SIMULATE
            {
                let url = "https://httpstat.us/"+ err_code   
                return fetch(url,{  headers: this.headers,    method: 'GET',   })
                .then(response => {        return response;         })
                    .catch(error => {
                                        return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }                         
                                })
            }
            
            async put(url:string,put_data:Record<PropertyKey,any>,env:number | string='') 
            {
                if (env){
                    url+= url.indexOf('?') >= 0 ?  '&' : '?';
                    url+='env='+env.toString();
                }
                else if (this.env) {
                    url+= url.indexOf('?') >= 0 ?  '&' : '?';
                    url+='env='+this.env;
                }

                return fetch(url,{
                                headers: this.headers,
                                method: 'PUT',
                                body: JSON.stringify(put_data)
                                })
                    .then(async response => {
                                    // handle the response
                                const contentType:string | null = response?.headers?.get('Content-Type')
                                let resp:Record<PropertyKey,any> = {}
                                if (contentType==='text/html' || contentType==='text/plain') {
                                    resp.data = await response.text()
                                    resp.status = response.status
                                }
                                else  if (contentType==='application/json') {
                                    resp.data = await response.json()
                                    resp.status = response.status
                                } else resp = response
                                
                                if (this.response_handlers[resp?.status]) this.response_handlers[resp?.status](resp)

                                return resp;
                                })
                    .catch(error => {
                                   
                                        return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }
                            
                                })
            
            }

            
            async send(url:string,method:string,post_data:Record<PropertyKey,any>,env:number | string='')
            {
                if (env){
                    url+= url.indexOf('?') >= 0 ?  '&' : '?';
                    url+='env='+env.toString();
                }

                else if (this.env) {
                    url+= url.indexOf('?') >= 0 ?  '&' : '?';
                    url+='env='+this.env;
                }
              return fetch(url,{
                            headers: this.headers,
                            method: method,
                            body: JSON.stringify(post_data)
                                })
                                .then(async response => {
                                       // handle the response
                                const contentType:string | null = response?.headers?.get('Content-Type')
                                let resp:Record<PropertyKey,any> = {}
                                if (contentType==='text/html' || contentType==='text/plain') {
                                    resp.data = await response.text()
                                    resp.status = response.status
                                }
                                else  if (contentType==='application/json') {
                                    resp.data = await response.json()
                                    resp.status = response.status
                                } else resp = response
                               
                                if (this.response_handlers[resp?.status]) this.response_handlers[resp?.status](resp)
                                    
                                return resp;
                                     
                                })
                                .catch(error => {
                                   return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }
                                  
                                })
            }
        
        

            
        };
     
        /**
         * fn for initializing KatalyoApi Class returns KatalyoApi object based on auth_type
         *  @since 0.1
         */
        var KatalyoApi  = (function(new_instance?: boolean)
        {

          if (ktApi===undefined || new_instance) ktApi = new KatalyoApiMain()
          return ktApi;
        });
        export default KatalyoApi;