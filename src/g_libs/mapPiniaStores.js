//**************************************************************************************************
//*************************************** pinia store map - ES6 module ****************************
//**************************************************************************************************

//import all stores here
import { useAuthUserStore } from '@/m_auth/stores/authUser';
import { useAlertStore,alertMessage } from '@/g_stores/alert';
import { usePageStore } from '@/m_page/stores/page';
import { useAppMenuStore } from '@/g_stores/appMenu';

let ktPiniaInstance

class PiniaStoreMap{

    constructor () {
    //map stores here
      this.mappings = {
        "authUser":useAuthUserStore,
        "alert":useAlertStore,
        "page":usePageStore,
        "appMenu":useAppMenuStore,
        
      }
     
     }
    //get store instance
    getStore(storeName) {
       return this.mappings[storeName]
    }
    
    
 }
 var KatalyoPinia  = (function(new_instance)
 {

   if (ktPiniaInstance===undefined || new_instance) ktPiniaInstance = new PiniaStoreMap()
   return ktPiniaInstance;
 });
 export default KatalyoPinia;

