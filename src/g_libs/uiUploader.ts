import {  reactive,toRef,ref,computed,defineAsyncComponent } from "vue";
import KatalyoApi from '@/g_libs/kt-api';

const api_main = KatalyoApi()
const api = api_main.KatalyoApiClass


class uiUploader {

    files: any[];
    options: Record<string, any>;
    activeUploads: number;
    uploadedFiles: number;
    uploaders: Record<string, any>;
    server:any

    constructor(uploaders: Record<string, any> = {}, id: number, api:any) {

        this.files = reactive([]);
        this.options = {};
        this.activeUploads = 0;
        this.uploadedFiles = 0;
        this.uploaders = uploaders
        this.uploaders[id] = this
        
        this.server=api.server.replace('api/', '')
    
    }
    

    addFiles(files: any[], id: string): void {
        if (!this.uploaders[id]) {
            this.uploaders[id] = { files: [] };
        }
        for (let i = 0; i < files.length; i++) {
            this.uploaders[id].files.push(files[i]);
        }
    }


    addFile(file: any, id: string): void {
        if (!this.uploaders[id])   this.uploaders[id] = { files: [] }
        this.uploaders[id].files.push(file);
 
    }


    getFiles(id: number) {
        return this.uploaders[id].files;
    }

    startUpload(id: number, options: any) {

        if (options != undefined) {
            if (options.url != undefined)   options.url = this.server + options.url //TODO - REPLACE WITH SERVER PARAM
            this.uploaders[id].options = options;

            //headers are not shared by requests
            var headers = this.uploaders[id].options.headers || {};

            for (var i = 0; i < this.uploaders[id].files.length; i++) {
                if (this.uploaders[id].activeUploads == this.uploaders[id].options.concurrency)      break;    
                if (this.uploaders[id].files[i].active)          continue;
                this.ajaxUpload(i, this.uploaders[id].files[i], this.uploaders[id].options.url, this.uploaders[id].options.data, id, headers);
            }
        }
    }

    removeFile(file: File, id: number) {
        this.uploaders[id].files.splice(this.uploaders[id].files.indexOf(file), 1);
    }

    removeAll(id: number) {
        //migor TODO - vidjeti zasto uopce kod importdata ovo poziva npr http://localhost:8080/index/#/resources/import/55/
        if (this.uploaders[id] != undefined && this.uploaders[id] != null) this.uploaders[id].files.splice(0, this.uploaders[id].files.length);
    }


    getHumanSize(bytes: number) {
        var sizes = ['n/a', 'bytes', 'KiB', 'MiB', 'GiB', 'TB', 'PB', 'EiB', 'ZiB', 'YiB'];
        var i = (bytes === 0) ? 0 : +Math.floor(Math.log(bytes) / Math.log(1024));
        let a = this
        return (bytes / Math.pow(1024, i)).toFixed(i ? 1 : 0) + ' ' + sizes[isNaN(bytes) ? 0 : i + 1];
    }

    ajaxUpload(fnum: any, file: any, url: string, data: any, id: number, headers: any) {
        var xhr: any, formData, prop, key = 'file';
        data = data || {};

        this.uploaders[id].activeUploads += 1;
        file.active = true;
        

        xhr = new window.XMLHttpRequest();

        // To account for sites that may require CORS
        if (data.withCredentials === true) {
            xhr.withCredentials = true;
        }

        formData = new window.FormData();
        xhr.open('POST', url);

        if (headers) {
            for (var headerKey in headers) {
                if (headers.hasOwnProperty(headerKey)) {
                    xhr.setRequestHeader(headerKey, headers[headerKey]);
                }
            }
        }

        // Triggered when upload starts:
        xhr.upload.onloadstart =  () => { };

        // Triggered many times during upload:
        xhr.upload.onprogress =  (event:any) => {
            if (!event.lengthComputable) return;

            // Update file size because it might be bigger than reported bythe fileSize:
            file.loaded = event.loaded;
            file.humanSize = this.getHumanSize(event.loaded);
         
            //if (angular.isFunction(this.uploaders[id].options.onProgress)) {
            this.uploaders[id].options.onProgress(file);
            //}
        };

        // Triggered when upload is completed:
        xhr.onload =  () => 
            {
            this.uploaders[id].activeUploads -= 1;
            this.uploaders[id].uploadedFiles += 1;
            this.startUpload(id, this.uploaders[id].options);
            //if (angular.isFunction(this.uploaders[id].options.onCompleted)) {
            this.uploaders[id].options.onCompleted(this,file, xhr.responseText, xhr.status);
            // }
            if (this.uploaders[id].uploadedFiles === this.uploaders[id].files.length) {
                this.uploaders[id].uploadedFiles = 0;
                //if (angular.isFunction(this.uploaders[id].options.onCompletedAll)) {
                //this.uploaders[id].options.onCompletedAll(this.uploaders[id].files);
                //}
            }
        };

        // Triggered when upload fails:
        xhr.onerror =  (e:any) => { 
            //if (angular.isFunction(this.uploaders[id].options.onError)) {
            this.uploaders[id].options.onError(e);
            // }
        };
        
        // Append additional data if provided:
        if (data) {
            for (prop in data) {
                if (data.hasOwnProperty(prop)) {
                    formData.append(prop, data[prop]);
                }
            }
        }


        //if (file.hasOwnProperty('fileVerificationId')) formData.append('blockchain_file_id', file.fileVerificationId);
        
        formData.append(key, file, file.name)// Append file data:
        xhr.send(formData);  // Initiate upload:

        return xhr;
    }

}

export default uiUploader