//**************************************************************************************************
//*************************************** utility - ES6 module ****************************
//**************************************************************************************************

class Utility {

   constructor () {
     
    }

//////// FUNCTIONS

decimal = function( inp:any, decimals:any)
{
  return Number(Math.round(Number(inp + 'e' + decimals)) + 'e-' + decimals)  
  //return Number.parseFloat(inp).toFixed(decimals);
}


   increment = function (date:any, days:number) 
   {
      if (!date || !days) {
        return null
      }
      
      const newDate = new Date(date); // Create a copy of the input date
      //debugger
      newDate.setDate(newDate.getDate() + days); // Increment the date by 10 days
        
      return newDate;
    }


    str = function (inp:any) {
      if (inp) return inp.toString()
      else return inp
    }

    concat = function (...inp:any[]) {
      let result=''
      inp.forEach(element => {
      result+=element.toString()

    });
    return result
        
    }


    sum = function (obj:any) {
      return Object.values(obj).reduce((a:any, b:any) => a + b, 0); 
    }
    
    getCurrentYear = function () {
      
      return new Date(Date.now()).getFullYear()
        
    }

    getSortMethod = function (args:string[])  //usage: array.sort( getSortMethod('-price', '+priority', '+name'));
    {
      var _args = args
      return function (a:any, b:any) {
          for (var x of args) {
              try {
                  var ax, bx, cx;
                  ax = a[x.substring(1)]
                  bx = b[x.substring(1)]
                  if (ax == null || ax == undefined)     return 1
                  if (bx == null || bx == undefined)     return -1
                  if (ax == bx)  continue
  
              } catch (error) {
                  console.error(error);
              }
  
              if (x.substring(0, 1) == "d") { cx = ax; ax = bx; bx = cx; } //switch when 'descending' sort
  
              if (typeof ax == "string") {
                  // let res = ax.localeCompare(bx, langCode || 'en-GB', { sensitivity: 'base' })
                  let res = ax.localeCompare(bx, 'en-GB', { sensitivity: 'base' })
                  return res
              } else 
                  return ax < bx ? -1 : 1;
          }
          return 0
      }
  }

   uuidv4() {
      return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
      });
   }
    uuidNow() {
      return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16)+'-'+Date.now();
      });
   }



    HSV2RGB(h:any, s:any, v:any) {
    var r, g, b, i, f, p, q, t;
    if (arguments.length === 1) {
        s = h.s, v = h.v, h = h.h;
    }
    i = Math.floor(h * 6);
    f = h * 6 - i;
    p = v * (1 - s);
    q = v * (1 - f * s);
    t = v * (1 - (1 - f) * s);
    switch (i % 6) {
        case 0: r = v, g = t, b = p; break;
        case 1: r = q, g = v, b = p; break;
        case 2: r = p, g = v, b = t; break;
        case 3: r = p, g = q, b = v; break;
        case 4: r = t, g = p, b = v; break;
        case 5: r = v, g = p, b = q; break;
    }
 return    "#" + Math.round(r * 255).toString(16) +    Math.round(g * 255).toString(16) + Math.round(b * 255) .toString(16) 
}


getNiceColors(howmany:number)
{
  let colors=[]
  let golden_ratio = 0.618033988749895
  let h = Math.random() // use random start value
   
  for (let i=0; i < howmany; i++) { 
       h += golden_ratio
       h %= 1  
       colors.push(  this.HSV2RGB( h, 0.5, 0.95) )   
  }      
  return colors             
}

 formatNumber(num:number, params:Record<string,any>)  
 {
    let newFormat; 
         
    if (params?.decimalPlaces)
      newFormat = new Intl.NumberFormat("de-DE", { useGrouping: params?.useGroupingSeparator ,  maximumFractionDigits: params?.chartFractionDigits ,  minimumFractionDigits: params?.decimalPlaces  });
    else 
      newFormat = new Intl.NumberFormat("de-DE", { useGrouping: params?.useGroupingSeparator  });               

  try {
         if (params?.disableFormatting) 
             return num          
           else  
             return newFormat.format(num)
         } catch (err) {
                // MessagingService.addMessage(error.msg,'error');
                // return 'Error!'
         }       
 }
  extractIdFromRecord(record:any,tablePrefix:string){

    let record_id:number

    if (record!==undefined)
    {   
        if (Array.isArray(record) && record.length>0)
        {
            if (tablePrefix+'_id' in record[0])
            {
                record_id=record[0][tablePrefix+'_id']
            
            }else record_id = record[0].id

        }else{
            if (tablePrefix+'_id' in record)
            {
                record_id = record[tablePrefix+'_id']
            
            }else record_id = record.id

        }
    }else record_id = 0

    return record_id   
  }
  processForm(formDefinition : Array<Record<PropertyKey,any>>) {
            
      let formWidgets:Array<any> = []

        for (let i=0;i<formDefinition.length;i++)
        {
            if ('layout' in formDefinition[i])
            {
                   
               formWidgets = [...formWidgets,...this.processForm(formDefinition[i].layout)]
            }
            else formWidgets.push(formDefinition[i])
        }
        return formWidgets
    
  }

  processAuditInfo (record:any,templateType:string,inTemplate:string,tablePrefix:string)
  {
      	let label:string=''
        if (record && inTemplate)
        {
          inTemplate = inTemplate.split('<<').join('{{')
          inTemplate = inTemplate.split('>>').join('}}')
          if(templateType=='create')
            {
          label= inTemplate.replace('{{user}}',record[tablePrefix+'_CreatedBy.name'] || record[tablePrefix+'_CreatedBy'])
          label = label.replace('{{date}}',new Date(record[tablePrefix+'_CreatedDateTime']).toLocaleDateString())
          label = label.replace('{{time}}',new Date(record[tablePrefix+'_CreatedDateTime']).toLocaleTimeString())
            }
          else if (templateType=='change' && record[tablePrefix+'_ChangedBy.name']!==null)
          {
            label = inTemplate.replace('{{user}}',record[tablePrefix+'_ChangedBy.name'] || record[tablePrefix+'_ChangedBy'])
            label = label.replace('{{date}}',new Date(record[tablePrefix+'_ChangedDateTime']).toLocaleDateString())
            label = label.replace('{{time}}',new Date(record[tablePrefix+'_ChangedDateTime']).toLocaleTimeString())
    
          }
        }
        
        return label
   }

   checkStyleSheet(url:string){
    let found = false;
    for(let i = 0; i < document.styleSheets.length; i++){
       if(document.styleSheets[i].href==url){
           found=true;
           break;
       }
    }
   return found
  }
    createCssLink(publishUrl:string,fileName:string)
    {
        let head = document.getElementsByTagName('HEAD')[0]
        let link = document.createElement('link') // Create new link Element
        // set the attributes for link element
        link.rel = 'stylesheet'  
        link.type = 'text/css' 
        link.href = `${publishUrl}${fileName}.css`
        if (!this.checkStyleSheet(link.href)) head.appendChild(link);  // Append link element to HTML head

        return link
    }

    check_condition(operator:string,val:any,res:any){

      let condition:boolean = false

      if (operator=='=' && res==val) condition=true 
      else if (operator=='>=' && res>=val) condition=true
      else if (operator=='<=' && res<=val) condition=true
      else if (operator=='>' && res>val) condition=true
      else if (operator=='<' && res<val) condition=true

      return condition
  }

  evaluate_condition(dataSrc:any,actionResults:any,condition:string,parse:any,evaluate:any){

    const context = {
          ...this,
          ...dataSrc,
          ...actionResults,
              Date,
              Array,
              Math,  
   } 

    let returnVal:any
    

    try {
      const ast:any = parse(condition)
      returnVal = evaluate(ast, context)
    } catch (error) {
      console.log(error)
    }

    return returnVal
  }

  eval_expr(dataSrc:any,expr:string,parse:any,evaluate:any){

    const context = {
          ...this,
          ...dataSrc,
              Date,
              Array,
              Math,  
   } 

    let returnVal:any
    

    try {
      const ast:any = parse(expr)
      returnVal = evaluate(ast, context)
    } catch (error) {
      console.log(error)
      returnVal = expr
    }

    return returnVal
  }

}

export default Utility;