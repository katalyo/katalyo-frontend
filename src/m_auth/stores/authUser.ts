//import { ref, computed } from "vue"; - check later if ref is needed
import { defineStore } from "pinia"
import { useCookies } from "vue3-cookies"
import KatalyoApi from '@/g_libs/kt-api'
import { useAlertStore,alertMessage } from '@/g_stores/alert'
import router from "@/g_router"
import { useApplicationStore } from "@/g_stores/application"

const {cookies} = useCookies()

//using options API for now (if there's a need switch to composition API)
let getInitialCurrentUser = ()=>{
  if (sessionStorage.getItem('user')) return JSON.parse(localStorage.getItem(sessionStorage.getItem('user') || '') || '{}')
  else JSON.parse(localStorage.getItem(localStorage.getItem(sessionStorage.getItem('server_sessionid') || '') || '') || '{}')
  
}
export const useAuthUserStore = defineStore({
  
  id: "authUser",

  state: () => ({
        // initialize state from local storage to enable user to stay logged in
        //ToDo - implement expiry date as sessionStorage does not have it by default
        //sessionStorage is shared by tabs which is maybe good - alternative is sessionStorage or cookie
        //use localStorage for storing user profiles
        allUsers: JSON.parse(localStorage.getItem('katalyo_users') || '[]') as Record<PropertyKey,any> | null,
        currentUser: getInitialCurrentUser() as Record<PropertyKey,any> | null,
        currentUserId: sessionStorage.getItem('user') as Record<PropertyKey,any> | null,
        isAuthenticated: (sessionStorage.getItem('isAuthenticated')==='true') as boolean,
        authExpiryDate : sessionStorage.getItem('authExpiryDate') as Date | string,
        //Date.parse
        authInProgress: false as boolean,
        logoutSuccess: false as  boolean,
        logoutInProgress: false as  boolean,
        apiInProgress: false as  boolean,
        schemas:[],
        api:{} as Record<PropertyKey,any>,
        // sessionId: cookies.get('sessionid')
        sessionId: sessionStorage.getItem('sessionid'),
        environments:[]
    }),
 
  getters: {
    getSessionId: (state) => {return state.sessionId},
  //  getCookieSessionid: (state) => {return state.sessionId},
    getCurrentUser: (state) => {return state.currentUser},
    getCurrentUserOrgId: (state) => {return state.currentUser?.organisation?.id},
    getIsAuthenticated: (state) => {return state.isAuthenticated},
    getIsSuperUser: (state) => {return state.currentUser?.is_superuser},
    getCurrentUserDefaultEnv: (state) => {return state.currentUser?.uep?.defaultenv},
    getCurrentUserLangId: (state) => {return state.currentUser?.lang},
    getcurrUserAdvProps: (state) => {return (filedName:string) => state.currentUser?.uep?.AdvancedProperties?.[filedName]},
    getAuthInProgress: (state) => {return state.authInProgress},
    getEnvironments: (state) => {
      if (state?.currentUser?.environments) 
          return state.currentUser.environments.reduce((acc:any, obj:any) =>  {
          acc[obj.id.toString()] = obj;
          return acc;
        }, {})
      else return {}
    },
    getEnvArray: (state) => {

          if (state.currentUser) return state.currentUser.environments
          else []
    }
  },
  actions: {

    
    async  PswReset (username:any,userId:any, password1:string, password2:string,digest:string, callback:any) {
  
      let url = '../api/change-password/';
      let post_data = {'username':username,'user_id':userId,'password1':password1,'password2':password2,'digest': digest ,'type':0};
      //let csrf_token = this.$cookies.get('csrftoken'); 
     // this.RestApiService.add_api_classes("token",csrf_token);

     const api = KatalyoApi().KatalyoApiClass
     let response = await api.post(this.api.server + url, post_data)

     let success = false;
     let message='';
     if (response.status===200) {
             //change-password success   
             success = true;
             message = response.data.msg
     }
     else message = response.data.msg
     
     let resp = {'data':response.data,'message':message,'status':response.status,'success':success}
     callback(resp);          
 
},

    async CheckPassChangeRequest (digest:any, callback:any) {
  
         let url = '../api/check-change-password/';
         let post_data = { 'digest': digest };
         //let csrf_token = this.$cookies.get('csrftoken'); 
        // this.RestApiService.add_api_classes("token",csrf_token);

        const api = KatalyoApi().KatalyoApiClass
        let response = await api.post(this.api.server + url, post_data)

          let success = false;
           let message='';
            let resp = {};
        if (response.status==200)
                 {
                         if (response.status===200) success = true;     
                             else message = 'Check failed';
                         let resp = {'data':response.data,'message':message,'status':response.status,'success':success}
                         callback(resp);
                 }else {
                         if (response.status===200) success = true;      
                             else message = 'Check failed';
                         resp = {'message':message,'status':response.status,'success':success}
                         callback(resp);
                 }           
    
 },
    async checkSignup(signupkey:any) {
      let url = 'check-signup/'
      const api = KatalyoApi().KatalyoApiClass
      let response = await api.post(this.api.server + url, { 'signupkey': signupkey })
   
      return response

    },
    async ResetPassword (user_id:any) {

      let url='reset-password-request/'+user_id+'/';
      const api = KatalyoApi().KatalyoApiClass
 
      let response = await api.get(this.api.server +url)
              
       if (response.status === 200) {   
                             // deferred.resolve({data: response.data, status: response.data.status});
                      } else {
                              //deferred.reject({data:response.data,status:response.status});
          }
    
         return response
},
async ChangePasswordRequest (email:any) {

  let url='change-password-request/'
  let post_data = {email: email}
  const api = KatalyoApi().KatalyoApiClass

  let response = await api.post(this.api.server +url, post_data)
          
   if (response.status === 200) {   
                         // deferred.resolve({data: response.data, status: response.data.status});
                  } else {
                          //deferred.reject({data:response.data,status:response.status});
      }

     return response
},

    async saveUserExtended (onboardingStatus:any)
     {
      const api = KatalyoApi().KatalyoApiClass
                  
      let url = 'usersextended/';
      let post_data = {'onboarding':onboardingStatus}
      const api_response:any= await api.post(this.api.server +url, post_data);  
   
      if (api_response.status!=200 ) {              
          const alertStore = useAlertStore();
          alertStore.error(new alertMessage('Error',api_response.message,api_response.status));  
      } 

      return api_response
  
    },

       async register(post_data: any)
        {
          const api = KatalyoApi().KatalyoApiClass
          const url = '../api/register/';
      
          const api_response:any= await api.post(this.api.server +url, post_data);  
             
          if (api_response.status!=200 ) {              
              const alertStore = useAlertStore();
              alertStore.error(new alertMessage('Error',api_response.data, api_response.status));  
          } else this.currentUser=api_response.data.data
    
        return api_response
        },
    
        async unregister(post_data: any)
        {
          const api = KatalyoApi().KatalyoApiClass
          const url = '../api/unregister/';
      
          const api_response:any= await api.post(this.api.server +url, post_data);  
             
          if (api_response.status!=200 ) {              
              const alertStore = useAlertStore();
              alertStore.error(new alertMessage('Error',api_response.message,api_response.status));  
          } else this.currentUser=api_response.data.data
    
        return api_response
        },
    
    async signup(post_data: any)
     {
      const api = KatalyoApi().KatalyoApiClass
      let url = 'signup/'
      const api_response:any= await api.post(this.api.server +url, post_data);     
     
    return api_response
  },

    async loginNewTab(session_data: string) 
    {      
      try {

          this.authInProgress=true
          const api_main = KatalyoApi(true)
          const api = api_main.KatalyoApiClass
      
          let csrf_token= cookies.get('csrftoken')
          let session_id= cookies.get('sessionid')
          let currentUser = this.currentUser

          if (api.get_csrf()===undefined) api.set_csrf(csrf_token)
        
          const api_response:any= await api.post(this.api.server+'login-new-tab/',  session_data);   

          if (api_response.status===200 ) {
            
              const user = api_response.data
              this.setUserData(user,api_main)

            
          } else {
            this.isAuthenticated = false
            sessionStorage.setItem('isAuthenticated', this.isAuthenticated.toString())

          }

      } catch (error) {

          sessionStorage.removeItem('user')                
          this.isAuthenticated = false   
          sessionStorage.setItem('isAuthenticated', this.isAuthenticated.toString())
      }finally{
        
        this.authInProgress=false
        return this.isAuthenticated
      }
},

//* **************** LOGIN  *************************

    async login(username: string,password: string, registration=false) {

      this.authInProgress=true
      let api_response:any
      let error_message=''
            
            try {

                const api_main = KatalyoApi(true)
                const api = api_main.KatalyoApiClass
                
                let csrf_token= cookies.get('csrftoken')
                let session_id= cookies.get('sessionid')
               
                if (api.get_csrf()===undefined)   api.set_csrf(csrf_token)
                
                if (!registration)
                   api_response = await api.post(this.api.server+'login/', { username, password })
                else 
                  api_response =  { data: this.currentUser, status:200 }
                
                if (api_response.status===200 ) {
                    const user = api_response.data  
                    this.setUserData(user,api_main)
                    this.authInProgress=false
                } else if (api_response.status===499) {
                  error_message='Server connection failed!'
                  this.authInProgress=false
                } else {
                  //store error in alertStore
                  this.authInProgress=false
                  this.isAuthenticated = false
                  error_message='Invalid credentials!'
                  sessionStorage.setItem('isAuthenticated', this.isAuthenticated.toString())

                }

            } catch (error) {
                //const alertStore = useAlertStore();
                //alertStore.error(error);
                sessionStorage.removeItem('user')                
                this.isAuthenticated = false
                this.authInProgress=false
                error_message='Login error!'
                sessionStorage.setItem('isAuthenticated', this.isAuthenticated.toString())
            } finally {
             
              return {isAuthenticated:this.isAuthenticated, error_message: error_message}
            }
    },
    async logout() {
            
            this.logoutInProgress = true
            if (!this.isAuthenticated){
              this.logoutSuccess = true 
              this.logoutInProgress = false
              return true
            }
           
            const api = KatalyoApi().KatalyoApiClass
            const api_response:any = await api.post(this.api.server+'logout/',{});    
            if (api_response.status===200)
            {
              this.clearAuthData()
              this.logoutSuccess = true
              this.logoutInProgress = false

              return true
            }else {
              this.logoutInProgress = false
              return false
            }
            
    },
    async changePassword($v:any,password:any) {

      try {

        const is_valid = await $v.$validate()

        if (is_valid)
        {
          this.apiInProgress=true
          const api_main = KatalyoApi()
          const api = api_main.KatalyoApiClass  
          const api_response:any = await api.post(this.api.server+'change-password-with-auth/',{password1:password.currentPass,password2:password.pass1});    
          
          const msgObject:alertMessage = new alertMessage(api_response.data.msg,api_response.data.msg,api_response.status,'Api','authUserStore.changePassword')
          const alertStore = useAlertStore();
          if (api_response.status===200)
          {
           
            alertStore.success(msgObject);
            if (this.currentUser!==null) this.currentUser.token = api_response.data.token
            this.setUserData(this.currentUser,api_main)
            
            
          }
          else
          {
            alertStore.error(msgObject);
          }
        }
      } catch (err:any) {
          const alertStore = useAlertStore();
          const errObject:alertMessage = new alertMessage('Error changing password',err.message,err.status)
          alertStore.error(errObject);

      }finally{
        this.apiInProgress=false
      }
          
      } ,
  
    setCurrentUserBySessionId(sessionId:string)
    {
      sessionStorage.setItem('server_sessionid',sessionId)
      sessionStorage.setItem('user',localStorage.getItem(sessionId) || '')  
  
      if (localStorage.getItem(sessionId)) {
          if (!this.currentUser) this.currentUser = JSON.parse(localStorage.getItem(sessionStorage.getItem('user') || '') || '{}')
          sessionStorage.setItem('isAuthenticated','true')
          if (!this.isAuthenticated) this.isAuthenticated=true
      }
    },
    
    async getOrgSchemas ()
    {

        if (this.schemas.length>0 ) return this.schemas

        let url='../api/get-org-schemas/'+ this.currentUser?.orgId +'/';

        const api_main = KatalyoApi()
        const api = api_main.KatalyoApiClass  
  
        const api_response:any= await api.get(this.api.server+url);   

        if (api_response.status===200 ) { 
          this.schemas =  api_response.data    
          return this.schemas   
            //     return {data: api_response.data, status: api_response.data.status}  
           } else {
                    //return {data:api_response.data ,status:api_response.status}
           }

  },

  async getEnvInfo (orgId:number, caching=false) 
  {
          let url='../api/get-env-info/'+orgId+'/';
         
          const api_main = KatalyoApi()
          const api = api_main.KatalyoApiClass         
          const api_response:any= await api.get(this.api.server+url);   

          if (api_response.status===200 ) {          
                      return {data: api_response.data, status: api_response.data.status}  
           } else {
                    return {data:api_response.data ,status:api_response.status}
           }
  },

    
  async getUserInfo (orgId:number, caching=false) 
  {
          let url='../api/get-user-info/'+orgId+'/';
          const api_main = KatalyoApi()
          const api = api_main.KatalyoApiClass         
          const api_response:any= await api.get(this.api.server+url);   

          if (api_response.status===200 ) {          
                      return {data: api_response.data, status: api_response.data.status}  
           } else {
                    return {data:api_response.data ,status:api_response.status}
           }
  },

   clearSessionData () 
  {
        this.isAuthenticated=false
  },
  clearAuthData () 
  {
        this.isAuthenticated=false
        
        if (this.currentUser?.sessionid) localStorage.removeItem(this.currentUser.sessionid)
        if (this.currentUser?.userid)  localStorage.removeItem('user_'+this.currentUser?.userid)
        
        sessionStorage.removeItem('isAuthenticated')
        sessionStorage.removeItem('authExpiryDate')
        sessionStorage.removeItem('user')
        sessionStorage.removeItem('server_sessionid')
 
        this.currentUser=getInitialCurrentUser()
  },
  redirect2Login () 
  {
    if (this.isAuthenticated){
      const appStore = useApplicationStore()
      if (router.currentRoute?.value?.name) sessionStorage.setItem('returnUrl',router.currentRoute?.value?.name.toString())
      if (router.currentRoute?.value?.params) sessionStorage.setItem('urlParams',JSON.stringify(router.currentRoute?.value?.params))
      router.push({name: 'login',query:{app_ref:appStore.currentApp.uid,env:appStore.currentEnv}})
    }
  
  },
  setUserData(user:any,api_main:any)
  {

    const auth_data = 'Token '+user.token

    let csrf_token:string= cookies.get('csrftoken')
    let session_id:string= cookies.get('sessionid')// 
    sessionStorage.setItem('server_sessionid', session_id)//move session to session storage, TODO - delete "sessionid" from COOKIES ??
    cookies.remove('sessionid')

    api_main.add_auth_token(csrf_token, auth_data)
    api_main.set_env( user.uep.defaultenv ) 
    let resp_handlers:any = {}
    resp_handlers[401] = this.redirect2Login
    api_main.set_response_handlers( resp_handlers ) 
    
    // update pinia state
    this.sessionId = session_id
   
    this.currentUser = user
    if (!this.currentUser) return
    else this.currentUser.sessionid = session_id
    this.currentUser.authdata = auth_data
    this.currentUser.langObj = {id:this.currentUser.lang?.id,name:this.currentUser.lang?.Name,Code:this.currentUser.lang?.Code}
    this.currentUser.lang = this.currentUser.lang?.id
    this.isAuthenticated = true
    this.currentUser.authExpiryDate = new Date(new Date().setDate(new Date().getDate() + 1))
    // store user details including token in local storage to keep user logged in between page refreshes
    //don't need user data in session storage - only pointer to local storage

    sessionStorage.setItem('user' , 'user_'+this.currentUser.userid.toString())
    localStorage.setItem('user_' + this.currentUser.userid.toString(), JSON.stringify(this.currentUser))
    localStorage.setItem(this.sessionId, 'user_' + this.currentUser.userid);
    let all_users:Array<string> = JSON.parse(localStorage.getItem('katalyo_users') || '[]')

    if (!all_users.find((item)=> item==='user_' + this.currentUser?.userid.toString()))
    {  
      all_users.push('user_' + this.currentUser.userid.toString())
      localStorage.setItem('katalyo_users', JSON.stringify(all_users))
    }

    // store isAuthenticated in local storage keep user logged in between page refreshes
    sessionStorage.setItem('isAuthenticated', this.isAuthenticated.toString())
    //sessionStorage.setItem('env', this.currentUser.uep?.defaultenv.toString())
    sessionStorage.setItem('authExpiryDate', new Date(new Date().setDate(new Date().getDate() + 1)).toString())

  
  }
  }//$actions
 // return { login, logout, currentUSer };
});