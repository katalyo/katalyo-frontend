//import { ref, computed } from "vue"; - check later if ref is needed
import { defineStore } from "pinia"
import { useCookies } from "vue3-cookies"
import KatalyoApi from '@/g_libs/kt-api'
import { useAlertStore,alertMessage } from '@/g_stores/alert'
import { useApplicationStore } from '@/g_stores/application'

/*
const api_main = KatalyoApi(true)
const api = api_main.KatalyoApiClass
*/



export const useUserStore = defineStore({

  id: "User",
  state: () => ({
        users: [] as Array<any>,
        groups: {} as Record<PropertyKey,Array<any>>,
        fetchUserInProgress: false as boolean,
        usersLoaded: false as boolean,
        groupsLoaded: {} as Record<PropertyKey,boolean>,
        savingInProgress: false as boolean,
        fetchGroupsInProgress: false as boolean,
        api:{} as Record<PropertyKey,any>,
    }),
  getters: {
     getUsers: (state) => {return (displayTemplate:string) =>{

          let userList:any 
          
          if (displayTemplate){
          userList = state.users.map((user:any) => {
            return {
              ...user,
              name: displayTemplate.replace('{{first_name}}',user.first_name).replace('{{last_name}}',user.last_name).replace('{{username}}',user.username).replace('{{id}}',user.id)
            }
          })}
          else userList = state.users
          return userList
        }
     },
     getGroups: (state) => {
      const appStore = useApplicationStore()
      return state.groups[appStore.currentEnv]
    },
     getUserById: (state) => {return (userId:number) => state.users.filter((u)=> u.id==userId)[0]},
     getSelectedUser: (state) => {  
      return (isSelected:Record<PropertyKey,boolean>,displayTemplate:string) =>   {

            let selUser:Array<any> = []
            selUser = state.users.filter((item)=>isSelected[item.id])
            if (displayTemplate && selUser?.length) return [{ ...selUser[0],name: displayTemplate.replace('{{first_name}}',selUser[0].first_name).replace('{{last_name}}',selUser[0].last_name).replace('{{username}}',selUser[0].username).replace('{{id}}',selUser[0].id)}]
            else return selUser

      }   },
     getUsersLoaded: (state) => {return state.usersLoaded},
     getGroupsLoaded: (state) => {  
      const appStore = useApplicationStore()
      return state.groupsLoaded[appStore.currentEnv]},
    },
  actions: {

    async sendInvitaton(post_data:any) 
    {               
      let url = this.api.server+ 'join-invitation/'
      const applicationStore = useApplicationStore()
      const api_main = KatalyoApi()
      const api = KatalyoApi().KatalyoApiClass
      const response:any= await api.post(url,post_data,applicationStore.currentEnv) 
              
      if (response.status === 200) {
                    //debugger          
                  //deferred.resolve({msg: response.data, status: response.status});
                        
              } else {
                  //deferred.reject({msg:response.data,status:response.status});
        }
        return response
  },
    

    async getUsersGroups(auth_type:string) {

      const appStore = useApplicationStore()
      try{
        if (auth_type=="users") {
                if (this.users.length>0) {
                            return {data: this.users, status: 200}
                }
        }

        if (auth_type=="groups") {     
          if (!this.groups[appStore.currentEnv]) this.groups[appStore.currentEnv] = []
                if (this.groups[appStore.currentEnv].length>0) {  
                        return {data: this.groups[appStore.currentEnv], status: 200}
                }
        }
        let fetchInProgress:boolean = false
        
        if (auth_type=='users') fetchInProgress=this.fetchUserInProgress
        else fetchInProgress=this.fetchGroupsInProgress
        
          if (!fetchInProgress){
                const api_main = KatalyoApi()
                const applicationStore = useApplicationStore()
                const api = api_main.KatalyoApiClass
                let url = this.api.server+auth_type+'/';

                if (auth_type=='users') this.fetchUserInProgress= true
                else this.fetchGroupsInProgress= true
                const response:any= await api.get(url,applicationStore.currentEnv);  
                if (response.status === 200) {
                        if (auth_type=="users")
                          {
                            this.users = response.data;
                            this.fetchUserInProgress= false
                            this.usersLoaded = true
                          }
                        if (auth_type=="groups")   
                          {
                            this.groups[appStore.currentEnv] = response.data;
                            this.fetchGroupsInProgress= false
                            this.groupsLoaded[appStore.currentEnv] = true
                          }
                        return  {'data':response.data,'status':response.status}
                  }  else {
                          return {'data':response,'status':response.status}
           }
        
        }
        return {}
    } catch (error:any) {
      const alertStore = useAlertStore();
      const errObject:alertMessage = new alertMessage('Error changing password',error.message,error.status)
      alertStore.error(errObject);
      this.fetchUserInProgress= false
      this.fetchGroupsInProgress= false

  }
},

async getGroupsExtended () {
                  
  let url = this.api.server+'groupsextended/';
  const api_main = KatalyoApi()
  const applicationStore = useApplicationStore()
  const api = api_main.KatalyoApiClass
  const response:any= await api.get(url,applicationStore.currentEnv);   
  if (response.status === 200) {
           return {data:response.data,status:response.status}
   }                                 
        
  return {data:response.data,status:response.status}
},

async getUsersExtended () {
                   
  let url = this.api.server+'usersextended/';
  const api_main = KatalyoApi()
  const applicationStore = useApplicationStore()
  const api = api_main.KatalyoApiClass
  const response:any= await api.get(url,applicationStore.currentEnv); 
      if (response.status === 200) {
                 return {data:response.data,status:response.status}     
       }     

      return {data: response.data, status:response.status}
},


async getUserObjectById (userId:string) {

  if (userId==null) userId="";
  if (this.users.length==0) {
        this.getUsersGroups('users').then(  () => {
             var pos = this.users.map(function(e) { return e.id.toString() }).indexOf(userId.toString())
              if (pos>=0) {
                    return this.users[pos]
               } else  return null        
         })

  }
  
  var pos = this.users.map(function(e) { return e.id.toString(); }).indexOf(userId.toString())
  if (pos>=0){
      return this.users[pos]
    } else return null     

    return this.users[pos]
  
},

 
async getGroupObjectById (groupId:string) {
        
   if (groupId==null)   groupId=""  
   const appStore = useApplicationStore()
   if (this.groups[appStore.currentEnv] && this.groups[appStore.currentEnv].length==0){
    
    const response:any= await  this.getUsersGroups('groups')   
   }
    var pos = this.groups[appStore.currentEnv].map(function(e) { return e.id.toString(); }).indexOf(groupId.toString());
    if (pos>=0) {
         return this.groups[appStore.currentEnv][pos]
    } else return null
      
},
async SaveGroup (group:string) {

  this.savingInProgress=true
  var http_method="POST";
  var url=this.api.server+'save-group/';
  const api_main = KatalyoApi()
  const appStore = useApplicationStore()
  const api = api_main.KatalyoApiClass
  const response:any= await api.post(url,group,appStore.currentEnv) 
  let resp:any
   if (response.status === 200) {
            this.groups[appStore.currentEnv]=response.data.groups; 
            this.savingInProgress=false
            resp = response
            //debugger   
          // return {data: response.data,status:response.data.status}
      } else resp = {data: response.data,status:response.data.status}
    this.savingInProgress=false  
    return  resp
},  

async SaveUser (user:string) {
        this.savingInProgress=true
        let url=this.api.server+'save-user/';

        const api_main = KatalyoApi()
        const applicationStore = useApplicationStore()
        const api = api_main.KatalyoApiClass
        const response:any= await api.post(url,user,applicationStore.currentEnv) 
        
        if (response.status === 200)   this.users=response.data.users;    
        this.savingInProgress=false        
        return response 
},

async getOrganisationInfo (organisationId:any){
     
  if (organisationId == undefined) return
    
  const api_main = KatalyoApi()
  const applicationStore = useApplicationStore()
  const api = KatalyoApi().KatalyoApiClass

  let url=this.api.server+'organisations/'+organisationId+'/'
  
  const response:any= await api.get(url,applicationStore.currentEnv) 
  if (response.status==200)  return response.data
  return response         
  
},
 

async saveOrganisationInfo (organisation:any )
{

  const alertStore = useAlertStore()

  if (organisation.id == undefined) return
  
  let url=this.api.server + 'organisations/'+organisation.id+'/'

  const api_main = KatalyoApi()
  const applicationStore = useApplicationStore()
  const api = KatalyoApi().KatalyoApiClass
  const response:any= await api.post(url,organisation,applicationStore.currentEnv) 
  if (response.status==200) {
    alertStore.info(new alertMessage(response.data.msg,response.data.msg,response.status ) )
    return response.data
  } 
       
  else  alertStore.error(new alertMessage('Error changing password!','Error changing password!',response.status ) )
  
  return response
   			  		
} 



}//actions

})