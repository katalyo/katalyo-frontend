import { createRouter, createWebHistory, type LocationQueryValue } from "vue-router";
//import DashboardView from "@/views/DashboardView.vue";
import { useAuthUserStore } from '@/m_auth/stores/authUser'
//import { useApplicationStore } from '@/g_stores/application'
import { useCookies,globalCookiesConfig } from "vue3-cookies";

globalCookiesConfig({
  expireTimes: "1d",
  path: "/",
  //domain: "*.katalyo.com",
  secure: false,
  sameSite: "None",
});


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
   
    {
      // route level code-splitting
      // this generates a separate chunk (Login.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      path: "/login",
      name: "login",
      component: () => import("../views/LoginView.vue"),
      meta: {
        layout: 'AppNoMenuLayout'
      }
    },   
    {
      path: "/logout",
      name: "logout",
      component: () => import("../views/LogoutView.vue"),
       meta: {
        layout: 'AppNoMenuLayout'
      }
    },
    {
      path: "/users",
      name: "app.menu.users",
      component: () => import("../views/UsersDefinitionView.vue"),
    },
    {
      path: "/user",
      name: "app.menu.user.details",
      component: () => import("../views/UserDetailsView.vue"),
      meta: {
        layout: 'AppNoFooterLayout'
      }
    },
     {
      path: "/groups",
      name: "app.menu.groups",
      component: () => import("../../views/DashboardView.vue"),
    },
  ],
});

export default router;
