//import { ref, computed } from "vue"; - check later if ref is needed
import { watch } from "vue"
import { defineStore } from "pinia";
//import KatalyoApi from '@/g_libs/kt-api';
import { useAlertStore,alertMessage } from '@/g_stores/alert';
import { usePageStore } from "../../m_page/stores/page";
import { useResourceFormStore } from '@/m_resource/stores/resourceForm';
import { useRdbmsResourceStore } from '@/m_resource/stores/rdbmsResource'
import  { parse, evaluate } from 'jse-eval'
import Utility from '@/g_libs/kt-utility';
import type { promises } from "dns";
//import  { parse, evaluate, compile, jsep, registerPlugin } from 'jse-eval'


class resourceFlow {

  id:string;
  event:string;
  eventFn:string;
  widgetRef:string;   
  eventData:any;
  actions:Array<action>;
  orderGroup:number;
  flowData:any;
  actRes:any;
  //actionOrderGroupPromises:Array<Array<any>>;
  //actionOrderGroupIdx:Array<Array<number>>;
  resolve:any;
  reject:any;
  constructor(flow:any) {

    this.id=flow.id
    this.event=flow.event
    this.eventFn=flow.eventFn
    this.widgetRef=flow.widgetRef
    this.eventData=flow.eventData
    this.orderGroup=0
    this.flowData={}
    this.actRes={actionRes:{}}
    //this.actionOrderGroupPromises=[]
    //this.actionOrderGroupIdx=[]
    if (!this.event) this.event=flow.EventName
    if (!this.eventFn) this.eventFn=flow.EventFn
    if (!this.widgetRef) this.widgetRef=flow.EventWidgetId
    this.actions=[]
    for(let i=0; i < flow.actions.length; i++) this.actions.push(new action(flow.actions[i],this))
      
 }
  async execute(flowData:any) {
      
      if (flowData.results[this.id]==undefined) flowData.results[this.id]={}
      
      //here prepare actions for execution
      const formStore = useResourceFormStore()
      const pageStore = usePageStore()
      flowData.inParams={} //migor
      for(let i=0; i < this.actions.length; i++) {
        flowData.inParams[i] = this.actions[i].inParams //migor     this.actions[l].id
       // flowData.inParams = this.actions[i].inParams //migor

        await this.actions[i].setPromise(i)
        //actions.push(this.actions[i].promise)
         
      }
      

      this.flowData=flowData
      let flowResults:Array<any> = []
      let fStatus:number = 0
      let actionError:any = {}
      try{
        let utility = new Utility()
        let cond_check:boolean = true

        for (let j=0;j<this.orderGroup+1;j++)
        {
          if (fStatus==0)
          {
            let promiseArray: Array<any> = []
            //get action index
            
            for (let l=0;l<this.actions.length;l++)
            {
              
              //check conditions here and remove actions that don't satisfy conditions
              if (this.actions[l].orderGroup==j){

                let condition:any = this.actions[l]?.condition
               cond_check = true
               
                if (condition) 
                  try{
                    condition = JSON.parse(condition)
                  }catch (error) {
                    console.log(error)
                  }
               
                if (condition && Array.isArray(condition)) 
                {

                 
                    condition.forEach((element:any) => {
                      if (cond_check) cond_check = utility.check_condition(element.operator,element.result,this.actions[element.action-1].result)
                      
                      
                    });
                  
                  
                }  else if (condition) 
                  {
                    let eventData:any={},actionData:any={}
                    
                    eventData = formStore.getFormData(this.widgetRef.substring(0,36),pageStore.currentPage.instanceId)
                    
                    actionData = formStore.getFormData(this.actions[l].widgetId.substring(0,36),pageStore.currentPage.instanceId)
                  
                    const contextData:any = { 
                      ...eventData,
                      ...actionData
                    }
                    cond_check = utility.evaluate_condition(contextData,this.actRes,condition,parse,evaluate)
                  }
                  
                let actionResult:any
                if (cond_check) 
                {
                    this.flowData['actionIdx']=l
                    this.flowData['actionId']=this.actions[l].id
                    actionResult = await this.actions[l].promise(this,this.id)
                    if (actionResult){
                      this.actions[actionResult.actionIdx].result = actionResult.result
                      this.actRes.actionRes[actionResult.actionIdx] = actionResult.result
                      this.actions[actionResult.actionIdx].finished=true

                      if (actionResult?.status!=undefined && actionResult?.status!=0) fStatus=actionResult.status
                      flowResults.push(actionResult)
                    }
                }

              }

            }
        
          }
        }
      }
      catch(err){
        fStatus=1
        actionError = err
      }
      finally{
        return {status:fStatus,results:flowResults,error:actionError}
      }
     
  }
  
}


class action{
  
    id:string;
    flow:resourceFlow;
    promise:any;
    type:string;
    name:string;
    widgetId:string;
    waitPromises:any;
    waitfor:Array<number>;
    dependsOn:number;
    definition: any;
    status: string;
    condition:any;
    inParams:string;
    finished: boolean;
    result:any;
    reject:any;
    resolve:any;
    data:any;
    orderGroup:number;
    actionTypes:Array<string>;

    constructor (action:any,flow:resourceFlow){
      
     this.id=action?.id
     const utility = new Utility()
     if (!this.id) this.id = utility.uuidv4()
     this.flow = flow
     this.definition=action?.definition
     if (action?.action) this.type = action?.action.value
     else this.type = action.value
     this.name = action?.widgetActionName
     if (!this.name) this.name = this.type
     this.widgetId = action?.WidgetId || action?.widgetId || action?.widgetRef
     if (!this.widgetId) this.widgetId = this.flow.widgetRef
     this.status='created'
     this.condition=action?.condition
     this.waitfor=action?.waitfor
     this.inParams = action?.inParams
     this.orderGroup = 0
    //check for values
    if (!this.widgetId) this.widgetId = action?.WidgetId
    if (!this.name) this.name = action?.ActionName
    if (!this.condition) this.condition = action?.Condition
    if (!this.inParams) {
      if (action?.InputParams?.length)  this.inParams = JSON.parse(action?.InputParams)
    }
    
    if (!this.type) this.type = 'widgetaction'
    this.dependsOn = action.DependsOn
    if (this.waitfor==undefined && action?.DependsOn!=undefined) this.waitfor=[action?.DependsOn]
     if (this.waitfor==undefined) this.waitfor=[]
     this.finished=false
     this.promise = null
     this.waitPromises = []
     this.result=null
     this.data = {}
     this.actionTypes=['widgetaction','procwidgets','openpage','backbutton']
     
    }
   async setPromise(index:number){
      
      this.id = this.id+'-'+index.toString()
      let orderGroup:number = 0
      for(let i=0; i < this.waitfor.length; i++) {
        if (this.waitfor[i]>orderGroup) orderGroup=this.waitfor[i] 
       }
      this.setActionPromise()
      this.orderGroup = orderGroup
      this.flow.orderGroup = orderGroup
      
    }

    setActionPromise(){
      
      const pageStore = usePageStore() 
      let actionPromise:any,fun:any
      fun= pageStore.currentPage.pDef?.widgets[this.widgetId]?.actions[this.name]
      if (!fun) {
        watch( () => pageStore.currentPage.pDef?.widgets[this.widgetId],(newValue,oldValue) => {
          if  (newValue) {
            fun= pageStore.currentPage.pDef?.widgets[this.widgetId]?.actions[this.name]
            
            if (!fun) {
              const alertStore = useAlertStore()
              alertStore.error(new alertMessage("Function "+this.name+" for widget "+this.widgetId+" is not defined","Function "+this.name+" for widget "+this.widgetId+" is not defined",-1,"Resource flow setActionPromise fn"))
           
            }else this.promise = fun
            
          }else {
            const alertStore = useAlertStore()
            alertStore.error(new alertMessage("Function "+this.name+" for widget "+this.widgetId+" is not defined","Function "+this.name+" for widget "+this.widgetId+" is not defined",-1,"Resource flow setActionPromise fn"))
           
          }
          
        })
      } 
      else this.promise =  fun

    }
    
    /*    NOT USED !!!!   */
    async executeAction(flowData:any) {
     
      //this.buildDependencies(flowData)
      return Promise.all(this.waitPromises).then((res:any)=>{
        console.log(res)
        let procAction:boolean=true
        if (Array.isArray(res))
        {
          for (let i=0;i<res.length;i++)
            {
              if (!res[i]?.status) procAction=false
            }
        }
        if (!procAction) return res
         

        if (this.definition!=undefined)
        {
          let ast:any = parse(this.definition)
          return evaluate(ast,flowData)
        }
        else if (this.actionTypes.includes(this.type))
        {
          //execute action here
          const pageStore = usePageStore()
          //let result:any = await 
          pageStore.currentPage.pDef?.widgets[this.widgetId]?.actions[this.name](flowData,this.flow?.id)
          .then((result:any)=>{

            return result

          })
          
        }else return null

      },(err:any)=>{
        console.log(err)
        return err
      })
      
      
    }
  }


export const useResourceFlowStore = defineStore({
  
  id: "resourceFlow",

  state: () => ({
        flows:{} as Record<PropertyKey,resourceFlow>,
        //api:{} as Record<PropertyKey,any>,

    }),
 
  getters: {
    getFlow: (state) => {return (flowId:string)=> state.flows[flowId]},
    
    },

  actions: {
    

        async executeFlow(flowId:string) {
            
          let flowData:any,flowResult:any
          try {

            let flow = this.flows[flowId]
            const pageStore = usePageStore()
            const formStore = useResourceFormStore()
            const rdbmsStore = useRdbmsResourceStore()

            flowData={'pageStore':pageStore,'formStore':formStore,'rdbmsStore':rdbmsStore,results:{}}

            flowResult = await flow.execute(flowData)
              
          } catch (err:any) {
                const alertStore = useAlertStore()
                alertStore.error(new alertMessage(err.message,err.message,err.status))

            }finally{
              return flowResult
            }
        },

        addFlow(inFlow:any) {
          
          let flow:resourceFlow | undefined
          try {

          
            flow = new resourceFlow(inFlow)
            this.flows[inFlow.id] = flow
            
          } catch (err:any) {
              const alertStore = useAlertStore()
              alertStore.error(new alertMessage(err.message,err.message,err.status))

          }finally{
            return flow
          }
      },
        
  }
});