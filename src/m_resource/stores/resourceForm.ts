//import { ref, computed } from "vue"; - check later if ref is needed
import { computed } from "vue"
import { defineStore } from "pinia";
import { useCookies } from "vue3-cookies";
import KatalyoApi from '@/g_libs/kt-api';
import { useAlertStore,alertMessage } from '@/g_stores/alert';
import { useVuelidate } from '@vuelidate/core';
import { usePageStore } from "../../m_page/stores/page";
import { useEventBusStore } from '@/g_stores/eventBus'


export const useResourceFormStore = defineStore({
  
  id: "resourceForm",

  state: () => ({
        widgets:{} as Record<PropertyKey,any>,
        formWidgets:{} as Record<PropertyKey,any>,
        //processWidgetsInProgress: {} as Record<PropertyKey,boolean>,
        widgetsLoaded: {} as Record<PropertyKey, boolean>,
        widgetDataLoaded:{} as Record<PropertyKey, Record<PropertyKey,Record<PropertyKey,boolean>>>,
        formData:{} as Record<PropertyKey,any>,
        validationRules:{} as Record<PropertyKey,any>,
        api:{} as Record<PropertyKey,any>,

    }),
 
  getters: {
    getWidget: (state) => {return (widgetId:string)=> state.widgets[widgetId]},
    getFormWidgets: (state) => {return (pageInstanceId:string)=> state.formWidgets[pageInstanceId]},
    //getDatasetRecords: (state) => {return (widgetId:string)=> state.widgets[widgetId]?.dataRecords},
    getWidgetPresentationId: (state) => {return (widgetId:string)=> state.widgets[widgetId].PresentationId},
    getFormLoadCompleted: (state) => {return (pageId:string,widgetId:string)=> {
        if (state.formWidgets[pageId]) return state.formWidgets[pageId][widgetId]?.dataLoadCompleted || false
        else return false
      }
    },
    getFormDataLoaded: (state) => {return (pageId:string,widgetId:string)=> {
      if (state.formData[pageId]) return state.formData[pageId][widgetId]?.dataLoadCompleted || false
      else return false
    }
  },
  getFormLoadDetails: (state) => {return (pageId:string,widgetId:string)=> {
    if (state.formWidgets[pageId]) return state.widgetDataLoaded[pageId][widgetId] || {}
    else return {}
  }
},
getFormLoadNotDone: (state) => {return (pageId:string,widgetId:string)=> {
  let fItems:any=new Set()
  for (const [key, value] of Object.entries(state.widgetDataLoaded[pageId][widgetId])) {
    if (!value) fItems.add(key)
  }
 
    return fItems
  }
},
    getFormValidations: (state) => {return (widgetId:string,fieldName:string)=> 
      {
        const pageStore = usePageStore()
        let validations:any = state.formData[pageStore.currentPage.instanceId][widgetId]?.$v[fieldName]
        if (validations==undefined) return {}
        else return validations
      }
    },
    getWidgetResourceDefinition: (state) => {return (widgetId:string)=> state.widgets[widgetId]?.publishedResource},
    getResourceFormDefinition: (state) => {return (widgetId:string)=> state.widgets[widgetId]?.FormDef},
    getFormData: (state) => {return (widgetId:string,pageInstanceId:string)=>{
        if (!state.formData[pageInstanceId]) return []
        else return state.formData[pageInstanceId][widgetId]?.data || []
      }
    
    },
    getFirstRec: (state) => {return (widgetId:string,pageInstanceId:string)=>{
       
      if (!state.formData[pageInstanceId][widgetId]?.data) return {}
      if (Array.isArray(state.formData[pageInstanceId][widgetId]?.data)){
           
          if (state.formData[pageInstanceId][widgetId].data.length) return state.formData[pageInstanceId][widgetId].data[0]
          else return {}
      }else return state.formData[pageInstanceId][widgetId].data
    }
  
  },
    getFormDataObject: (state) => {return (widgetId:string,pageInstanceId:string)=>{
      if (!state.formData[pageInstanceId]) return {}
      else return state.formData[pageInstanceId][widgetId] || {}
    }
  
   },
  getResourceRecordId: (state) => {return (pageInstanceId:string,widgetId:string,fieldId:string)=>{
    if (!state.formData[pageInstanceId][widgetId]) return 0
    else return state.formData[pageInstanceId][widgetId]?.data[fieldId] || 0
    }

  },
    getFormValid: (state) => {return (widgetId:string)=> state.formWidgets[usePageStore().currentPage.instanceId][widgetId].$v.$valid},
    getWidgetLoaded: (state) => {return (widgetId:string) => state.widgetsLoaded[widgetId]},
    getWidgetSrcType: (state) => {return (pageInstanceId:string,widgetId:string)=> state.formWidgets[pageInstanceId][widgetId].dataSrcType},
    getSkipProcess: (state) => {return (widgetId:string,pageInstanceId:string)=> state.formWidgets[pageInstanceId]?.[widgetId]?.skipProcessWidget},
    getCallBackFn: (state) => {return (widgetId:string,pageInstanceId:string)=> state.formWidgets[pageInstanceId]?.[widgetId]?.callBacks},
    },

  actions: {
    

        registerWidget(fd: Record<PropertyKey,any>,dataSrcType:string) {
            
            try {

              this.widgets[fd.UuidRef] = fd
              this.widgets[fd.UuidRef].dataSrcType=dataSrcType
              
              //setup form validation
              this.formData[fd.UuidRef] = {}
              
              this.validationRules[fd.UuidRef] = {}
              
            } catch (err:any) {
                const alertStore = useAlertStore()
                alertStore.error(new alertMessage(err.message,err.message,err.status))

            }
        },
        initFormWidgets(widgets:Array<any>,pageId:string,parentId:string) {
            
          try {
          let i:number
          for (i=0;i<widgets.length;i++)
          { 
            if (widgets[i].Dbtype?.length) this.widgetDataLoaded[pageId][parentId][widgets[i].UuidRef]=false
          }

          } catch (err:any) {
              const alertStore = useAlertStore()
              alertStore.error(new alertMessage(err.message,err.message,err.status))

          }
      },
      registerResource(resourceId:string)
      {
        if (this.formWidgets[resourceId]==undefined) this.formWidgets[resourceId]={}
        if (this.formData[resourceId]==undefined) this.formData[resourceId]={}
        if (!this.widgetDataLoaded[resourceId]) this.widgetDataLoaded[resourceId] = {}  
      },
        registerFormWidget(uuidref: string ,pageId:string, dataSrcType:string,skip?:boolean) {

          try {
            if (!skip) skip=false
           
            if (!this.widgetDataLoaded[pageId][uuidref]) this.widgetDataLoaded[pageId][uuidref] = {}  
              //this.formWidgets[pageId][fd.UuidRef] = fd

            this.formWidgets[pageId][uuidref] = {dataSrcType:dataSrcType,dataLoadCompleted:false,skipProcessWidget:skip}
            //setup form validation
            this.formData[pageId][uuidref] = {data:{},validationRules:{}}
            const $v = useVuelidate({},this.formData[pageId][uuidref].data)
            this.formData[pageId][uuidref]['$v'] = $v
            
          } catch (err:any) {
              const alertStore = useAlertStore()
              alertStore.error(new alertMessage(err.message,err.message,err.status))
          }
      },
      registerFormWidgetMultiple(uuidref: string ,pageId:string, dataSrcType:string,skip?:boolean) {

        try {
          if (!skip) skip=false
         
          if (!this.widgetDataLoaded[pageId][uuidref]) this.widgetDataLoaded[pageId][uuidref] = {}  
            //this.formWidgets[pageId][fd.UuidRef] = fd

          this.formWidgets[pageId][uuidref] = {dataSrcType:dataSrcType,dataLoadCompleted:false,skipProcessWidget:skip}
          //setup form validation
          this.formData[pageId][uuidref] = {data:[],validationRules:[]}
          //const $v = useVuelidate({},this.formData[pageId][uuidref][0].data)
          //this.formData[pageId][uuidref][0]['$v'] = $v
          
        } catch (err:any) {
            const alertStore = useAlertStore()
            alertStore.error(new alertMessage(err.message,err.message,err.status))
        }
    },
    newMultipleFormSetup(uuidref:string,pageId:string) {

      try {
        this.formData[pageId][uuidref].data.push({})
        const idx:number = this.formData[pageId][uuidref].data.length-1
        const $v = useVuelidate({},this.formData[pageId][uuidref][idx].data)
        this.formData[pageId][uuidref][idx]['$v'] = $v
        
      } catch (err:any) {
          const alertStore = useAlertStore()
          alertStore.error(new alertMessage(err.message,err.message,err.status))
      }
  },
      removeFormWidget(uuidref: string ,pageId:string){
        
        for (const [key, value] of Object.entries(this.formData[pageId])) {
          if (key.includes(uuidref) && key!=uuidref) delete this.formData[pageId][key]
        }
 
        
    },
    extendFormData(pageId:string,uuidref:string,extdata:any) {

      try {
        if (!this.formData[pageId][uuidref]) this.formData[pageId][uuidref] = {data:{}}
          this.formData[pageId][uuidref].data = {...this.formData[pageId][uuidref].data,...extdata}
        
      } catch (err:any) {
          const alertStore = useAlertStore()
          alertStore.error(new alertMessage(err.message,err.message,err.status,'resourceForm.extendFormData'))
      }
  },
      registerWidgetCallBackFn(uuidref: string ,pageId:string, callBacks:any) {

        try {

          this.formWidgets[pageId][uuidref]['callBacks']=callBacks
          
        } catch (err:any) {
            const alertStore = useAlertStore()
            alertStore.error(new alertMessage(err.message,err.message,err.status,'resourceForm.registerWidgetCallBackFn'))
        }
    },
      addValidationRule(widgetId: string,fieldId:string,rule:any) {
            
            try {
              const pageStore = usePageStore()

              if (!this.formData[pageStore.getCurrentPage.instanceId][widgetId]) this.setFormData(pageStore.getCurrentPage.instanceId,widgetId,{})
              
              if (this.formData[pageStore.getCurrentPage.instanceId][widgetId]?.data && this.formData[pageStore.getCurrentPage.instanceId][widgetId]?.data[fieldId]===undefined) this.formData[pageStore.getCurrentPage.instanceId][widgetId].data[fieldId] = null
                
              if (!this.formData[pageStore.getCurrentPage.instanceId][widgetId]?.validationRules) this.formData[pageStore.getCurrentPage.instanceId][widgetId].validationRules ={}
              if (this.formData[pageStore.getCurrentPage.instanceId][widgetId]?.validationRules[fieldId] ===undefined) this.formData[pageStore.getCurrentPage.instanceId][widgetId].validationRules[fieldId] ={}

                const new_validations = {...this.formData[pageStore.getCurrentPage.instanceId][widgetId]?.validationRules,...rule}
                
                this.formData[pageStore.getCurrentPage.instanceId][widgetId].validationRules= new_validations
                const rules = computed(() => {
                    return this.formData[pageStore.getCurrentPage.instanceId][widgetId]?.validationRules
                })
                const $v = useVuelidate(rules,this.formData[pageStore.getCurrentPage.instanceId][widgetId]?.data)
                this.formData[pageStore.getCurrentPage.instanceId][widgetId]['$v'] = $v
              

            } catch (err:any) {
                const alertStore = useAlertStore()
                alertStore.error(new alertMessage(err.message,err.stack,err.status))

            }
        },
        addValidationRuleWithData(widgetId: string,fieldId:string,data:any,rule:any) {
            
          try {
            const pageStore = usePageStore()
            
            if (this.formData[pageStore.getCurrentPage.instanceId][widgetId]?.validationRules[fieldId] ===undefined) this.formData[pageStore.getCurrentPage.instanceId][widgetId].validationRules[fieldId] ={}

            const new_validations = {...this.formData[pageStore.getCurrentPage.instanceId][widgetId]?.validationRules,...rule}
            
            this.formData[pageStore.getCurrentPage.instanceId][widgetId].validationRules= new_validations
            const rules = computed(() => {
                return this.formData[pageStore.getCurrentPage.instanceId][widgetId]?.validationRules
            })
            const $v = useVuelidate(rules,data)
            this.formData[pageStore.getCurrentPage.instanceId][widgetId]['$v'] = $v

          } catch (err:any) {
              const alertStore = useAlertStore()
              alertStore.error(new alertMessage(err.message,err.message,err.status))

          }
      },
        registerElement(widgetId: string,fieldId:string) {
            
          try {
            const pageStore = usePageStore()
            
            if ( this.formData[pageStore.getCurrentPage.instanceId][widgetId][fieldId]===undefined)  this.formData[pageStore.getCurrentPage.instanceId][widgetId][fieldId] = null

          } catch (err:any) {
              const alertStore = useAlertStore()
              alertStore.error(new alertMessage(err.message,err.message,err.status))

          }
      },
   validateForm(widgetId: string,pageInstanceId:string) {
            
            try {
              if  (this.formData[pageInstanceId]?.[widgetId]) this.formData[pageInstanceId][widgetId].$v.$validate()
              else return false
              return this.formData[pageInstanceId][widgetId].$v.$invalid
            } catch (err:any) {
                const alertStore = useAlertStore()
                alertStore.error(new alertMessage(err.message,err.message,err.status))
                return false
            }

        },
    setDataObject(widgetId: string) {
            
          try {
             
              this.formData[widgetId] = {}
            } catch (err:any) {
              
              const alertStore = useAlertStore();
              alertStore.error(new alertMessage(err.message,err.message,err.status))
            }
    },

    setFormData(pageInstanceId:string,widgetId:string,data: Object) {
      
      const eventBus = useEventBusStore()
      try {
          
          if (!this.formData[pageInstanceId][widgetId]) this.formData[pageInstanceId][widgetId] = {}
          this.formData[pageInstanceId][widgetId].data = data
          this.formData[pageInstanceId][widgetId].dataLoadCompleted = true
          eventBus.emit(pageInstanceId+'-dataLoaded-'+widgetId,data)

        } catch (err:any) {
          
          const alertStore = useAlertStore();
          alertStore.error(new alertMessage(err.message,err.message,err.status))
        }
},
    setWidgetDataLoaded(pageId:string,widgetId: string,parentWidgetId: string) {
      const eventBus = useEventBusStore()
      try
      {
          if (this.widgetDataLoaded[pageId] && this.widgetDataLoaded[pageId][parentWidgetId]) 
          {
              this.widgetDataLoaded[pageId][parentWidgetId][widgetId]=true
              eventBus.emit(pageId+'-dataLoaded-'+parentWidgetId+'-'+widgetId,true)
          }
          
          let dataLoadCompleted:boolean = false
          if (this.widgetDataLoaded[pageId] && this.widgetDataLoaded[pageId][parentWidgetId]) 
          {
              for (const [key, value] of Object.entries(this.widgetDataLoaded[pageId][parentWidgetId])) 
              {
                if (value) dataLoadCompleted = true
                else {
                  dataLoadCompleted = false
                  break
                }

                              }
          }
          
          
            if (this.formWidgets[pageId] && this.formWidgets[pageId][parentWidgetId]) {
              if (!this.formWidgets[pageId][parentWidgetId].dataLoadCompleted) this.formWidgets[pageId][parentWidgetId].dataLoadCompleted = dataLoadCompleted
            }
            else 
            {
              this.formWidgets[pageId][parentWidgetId] = {'dataLoadCompleted' : dataLoadCompleted}
            }
          
      } catch (err:any) {
          const alertStore = useAlertStore()
          alertStore.error(new alertMessage(err.message,err.message,err.status))
      }
  },

  setWidgetDataLoadedDirect(pageId:string,widgetId: string,dlc:boolean) {
            
    try
    {
        if (this.formWidgets[pageId] && this.formWidgets[pageId][widgetId]) this.formWidgets[pageId][widgetId].dataLoadCompleted=dlc
        else if (this.formWidgets[pageId]) this.formWidgets[pageId][widgetId]={'dataLoadCompleted' : dlc}
      
    } catch (err:any) {
        const alertStore = useAlertStore()
        alertStore.error(new alertMessage(err.message,err.message,err.status,'formStore.setWidgetDataLoadedDirect'))
    }
},
resetFormData(pageId:string,widgetId: string) {
            
  try
  {
      this.setFormData(pageId,widgetId,{})
      
    
  } catch (err:any) {
      const alertStore = useAlertStore()
      alertStore.error(new alertMessage(err.message,err.message,err.status,'formStore.resetFormData'))
  }
},
    async fetchPublishedResourceForm(widgetId: string) {
            
            try {

                if (this.widgets[widgetId].publishedResource===undefined)
                { 
                  const publishId:number | undefined =  this.widgets[widgetId].Parameters?.PublishedResourceId
                  const api_main = KatalyoApi()
                  const api = api_main.KatalyoApiClass
                  if (publishId!==undefined)
                  {
                      const api_response:any = await api.get(this.api.server+'get-published-resource-by-id/'+publishId+'/');   

                    
                      if (api_response?.status===200)
                      {
                        this.widgets[widgetId].publishedResource = api_response?.data
                        this.widgetsLoaded[widgetId] = true
                       
                      }else{
                      //store error in alertStore
                      const alertStore = useAlertStore();
                      alertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status))
                      }
                  }else this.widgets[widgetId].publishedResource = {}
                }

              } catch (err:any) {
                
                const alertStore = useAlertStore();
                alertStore.error(new alertMessage(err.message,err.message,err.status))

              }finally{
                return this.widgets[widgetId].publishedResource
              }
    },
  }
});