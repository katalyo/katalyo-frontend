import { defineStore } from "pinia";
import KatalyoApi from '@/g_libs/kt-api';
import { useAlertStore,alertMessage,  } from '@/g_stores/alert'; 
import { useAuthUserStore } from '@/m_auth/stores/authUser';
import { useResourceFormStore } from '@/m_resource/stores/resourceForm';
//import {  reactive,toRef,ref,computed,defineAsyncComponent } from "vue";
import { useApplicationStore } from '@/g_stores/application'

const getApi = ()=>{ return KatalyoApi().KatalyoApiClass }
export const useRdbmsResourceStore = defineStore({
  
  id: "rdbmsResource",

  state: () => ({  
        resourceData: {} as Record<PropertyKey, any>,
        resourceDataFull: {} as Record<PropertyKey, any>,
        staleFlag: {} as Record<PropertyKey, number>,
        widgetData: {} as Record<PropertyKey, any>,
        fetchDataInProgress: {} as Record<PropertyKey, boolean> ,
        dataLoaded: {} as Record<PropertyKey, boolean>,
        api:{} as Record<PropertyKey,any>,
    }),
 
  getters: {
      getApiPinia: (state) => {
          return KatalyoApi().KatalyoApiClass 
      },
      getAlertStore: (state) => {
        return useAlertStore() 
    },
    getAuthStore: (state) => {
      return useAuthUserStore() 
     },
      getResourceData: (state) => {return (dataRef:string) => state.resourceData[dataRef] || {}},
      getResourceDataRecord: (state) => {return (resourceId:string,recId:string | number) => {
        let id_key = 'resource'+resourceId.split('-').join('')+'_id'
        if (state.resourceData[resourceId]){
            let rec:any = state.resourceData[resourceId].filter((item:any)=>item[id_key]==recId)
            if (rec.length) return rec[0]
            else return {}
        }else return {}
      }},
      getWidgetData: (state) => {return (widgetId:string) => state.widgetData[widgetId]},
      getElementData: (state) => {return (dataRef:string,fieldName:string) => state.resourceData[dataRef]?.data[fieldName]},
      getWidgetElementData: (state) => {return (widgetId:string,fieldName:string) => state.widgetData[widgetId]?.data[fieldName]},
      getResourceDataLoaded: (state) => {return (dataRef:string) => state.dataLoaded[dataRef]},
      getStaleFlag: (state) => {return (resourceDefId:string) => state.staleFlag[resourceDefId]},
      getFetchDataInProgress:(state) => {return (widgetId:string) => state.fetchDataInProgress[widgetId]},

    },

  actions: {

    
  async saveApplication (app:any) {
       
    var url='../api/save-application/';
    let post_data = app;
    let api_response= await getApi().post(this.api.server + url, post_data)

    if (api_response.status === 200 || api_response.status === 201 ) { // if successful, bind success message to message
                  //deferred.resolve({data: response.data,status:response.status});    
       } else {
                  //deferred.reject({msg: 'Save application failed ! --> saveApplication'+response.data,status:response.status});     
    } 
    
     return api_response
  },
  
    async getResourceSubscriptions(subscriptionType:any):Promise<any> {
      
      let url='../api/resource-subscriptions/'+subscriptionType+'/'
      
      const appStore = useApplicationStore()

      let api_response= await getApi().get(this.api.server + url,appStore.currentEnv)
       
    if (api_response.status === 200 || api_response.status === 201 ) {
                    //  deferred.resolve({data: response.data,status:response.status});        
                } else {
                    //deferred.reject({msg: 'Get resource subscriptions failed ! --> GetResourceSubscriptions',status:response.status});     
                }   
      
       return api_response
    },
    
    async  getResourceAccess  (app:any):Promise<any> {
  
      let id = app.uid ? app.uid : 0
      let ver = app.version ? app.version: 0
   
      var url='../api/get-resource-access/'+ id +'/' + ver + '/';
      let response= await getApi().get(this.api.server + url)
     
       if (response.status === 200 || response.status === 201 ) {       
                    //deferred.resolve({data: response.data,status:response.status});           
         } else {
                   // deferred.reject({msg: 'Get published resources failed! ' + response.msg,status:response.status});     
         }        
        
        return response
    },

    async  saveResourceAccess  (resource:any, post_data:any) 
  {
    let resId =  resource.uid ?  resource.uid : 0
    var url='../api/save-resource-access/'+ resId +'/' + resource.version + '/' ;

    let api_response= await getApi().post(this.api.server + url, post_data)
        if (api_response.status === 200 || api_response.status === 201 ) {       
          //deferred.resolve({data: response.data,status:response.status});           
        } else {
          //deferred.reject({msg: 'SaveResourceAccess failed ! - ' + response.data, status:response.status});     
        }   
        
        return api_response
    },

    async  saveMenuAccess  (resource:any, post_data:any) 
  {
    let resId =  resource.uid ?  resource.uid : 0
    var url='../api/save-menu-access/'+ resId +'/' + resource.version + '/' ;

    let api_response= await getApi().post(this.api.server + url, post_data)
        if (api_response.status === 200 || api_response.status === 201 ) {       
          //deferred.resolve({data: response.data,status:response.status});           
        } else {
          //deferred.reject({msg: 'SaveResourceAccess failed ! - ' + response.data, status:response.status});     
        }   
    
        return api_response
    },

    async getResourceFields  (resourceId:any ) {
      
      var url='../api/get-resource-fields/'+resourceId+'/'+this.getAuthStore.getCurrentUserLangId+'/';
      let api_response= await getApi().get(this.api.server + url)
  
       if (api_response.status === 200 || api_response.status === 201 ) {
                    //deferred.resolve({data: response.data , status:response.status});       
                    return api_response.data
      } else {
                    //deferred.reject({msg: 'Get resource fields failed ! --> getResourceFields'+ response.data,status:response.status});     
       }  

    },

    async  getResourceDefListByType (type:any, useCache?:boolean)
    {
      let api_response:any
    try {    
      let url = '../api/get-resource-def-list-bylang-bytype/'+ this.getAuthStore.getCurrentUserLangId +'/'+type+'/';
      api_response= await getApi().get(this.api.server + url)

      if (api_response?.status===200) {    
        //do something
        } else {
            this.getAlertStore.error(new alertMessage(api_response.msg,api_response.msg,api_response.status))
        }

    } catch (err:any) {
        this.getAlertStore.error(new alertMessage(err.message,err.message,err.status))
    } finally{
        return api_response
      }
},

    async fetchResourceRecord(widgetId: string, pageId: string, taskInstanceId: string, prevResourceId: string, resourceInstanceId:string,formType: string,resourceDefId: string,recordId: number,resourceVersion:number,qType?:string) {
      
      if (!recordId) recordId=0
      let recId=resourceDefId+'_'+recordId.toString()
      let index:number=-1
      try {
         if (!qType) qType='view'
          this.fetchDataInProgress[widgetId] = true   
            if (prevResourceId==undefined) prevResourceId='0'
            if (taskInstanceId==undefined) taskInstanceId='0'
            if (formType==undefined) formType='i'
            if (resourceDefId==undefined) resourceDefId=''
            if (!recordId) recordId=0
            //if (recordId==undefined || recordId==0) throw Error('Cannot fetch record with no id! => '+widgetId)
            if (resourceInstanceId==undefined) resourceInstanceId='0'
            if (resourceVersion==undefined) resourceVersion=0
            let url:string = this.api.server+'get-task-view-resource/'+widgetId+'/'+pageId+'/'+taskInstanceId+'/'+prevResourceId+'/'+resourceInstanceId+'/'+formType+'/'+resourceDefId+'/'+recordId+'/'+resourceVersion+'/'
            if (qType==='update')  url=this.api.server+'get-task-update-resource/'+widgetId+'/'+pageId+'/0/'+prevResourceId+'/'+resourceInstanceId+'/'+formType+'/'+resourceDefId+'/'+recordId+'/'+resourceVersion+'/';
            const api_response:any = await getApi().get(url);   
            //get filter in here

            if (api_response?.status===200) {
              const formStore  = useResourceFormStore()
              if (!this.resourceData[resourceDefId]) this.resourceData[resourceDefId]=[]
              if (recordId>0){
                if (Array.isArray(api_response?.data?.data)){
                  let errMsg= 'Fetch retrieved more than one record for id='+recordId
                  this.getAlertStore.error(new alertMessage(errMsg,errMsg,api_response.status))
                }else
                {
                 
                  let id_key:string = 'resource'+resourceDefId+'_id'
                  
                  index= this.resourceData[resourceDefId].map(function(x:any) {return x[id_key]; }).indexOf(recordId)

                  if (index>=0) Object.assign( this.resourceData[resourceDefId][index],api_response?.data?.data)
                  else {
                    this.resourceData[resourceDefId].push(api_response?.data.data)
                    index = this.resourceData[resourceDefId].length-1
                  }

                  this.dataLoaded[recId] = true
                  formStore.setFormData(resourceInstanceId,widgetId,this.resourceData[resourceDefId][index])
   
                }
              }else{

                if (Array.isArray(api_response?.data?.data)){
                  let errMsg= 'Fetch retrieved more than one record'
                  this.getAlertStore.error(new alertMessage(errMsg,errMsg,api_response.status))
                }else
                {
                  let id_key:string = 'resource'+resourceDefId+'_id'
                  let record:any = api_response?.data.data
                  recordId = record[id_key]
                  index= this.resourceData[resourceDefId].map(function(x:any) {return x[id_key]; }).indexOf(recordId)

                  if (index>=0) Object.assign( this.resourceData[resourceDefId][index],record)
                  else {
                    this.resourceData[resourceDefId].push(record)
                    index = this.resourceData[resourceDefId].length-1
                  }

                  this.dataLoaded[widgetId] = true
                  formStore.setFormData(resourceInstanceId,widgetId,this.resourceData[resourceDefId][index])
                
                }
             }
              this.staleFlag[widgetId] = new Date().getTime()
              
            } else{
              this.getAlertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status))
            }
          
        } catch (err:any) {   
          this.getAlertStore.error(new alertMessage(err.message,err.message,err.status))
        } finally{
          this.fetchDataInProgress[widgetId] = false
          return this.resourceData[resourceDefId][index]
        }
},

async fetchResourceList(widgetId:string,resourceId: string,presentationId: number,parentResourceId: string,parentInstanceId: string,version:number,rversion:number) {      
      
  try {
          this.fetchDataInProgress[widgetId] = true
          let widgetKey:string = parentInstanceId+'-'+widgetId
          if (!rversion) rversion=0
          if (this.resourceData[widgetId]===undefined || !this.dataLoaded[widgetId] || this.staleFlag[resourceId]>this.staleFlag[widgetId])  { 
            const api_response:any = await getApi().get(this.api.server+'kt-task-dataset-list/'+ resourceId +'/'+presentationId+'/'+parentResourceId+'/'+widgetId+'/'+rversion+'/'+version+'/');   
            const formStore  = useResourceFormStore()
            if (api_response?.status===200) {
               //this.resourceData[widgetId] = api_response?.data //lost reactivity
             // api_response.data.data=reactive( api_response?.data.data) //not needed - already reactive
             Object.assign(this.resourceData[widgetKey], api_response?.data) //  to retain reactivity
              this.staleFlag[widgetKey] = new Date().getTime()
              this.dataLoaded[widgetKey] = true
              formStore.setFormData(parentInstanceId,widgetId,this.resourceData[widgetKey]) 
            } else
          {
            let msg='Error in kt-task-dataset-list call'
            if (api_response.msg) msg=api_response.msg
            else if (api_response.data) msg=api_response.data

            this.getAlertStore.error(new alertMessage(msg,msg,api_response.status))
          }
        }
            

        } catch (err:any) {
          this.getAlertStore.error(new alertMessage(err.message,err.message,err.status))
        } finally{
          this.fetchDataInProgress[widgetId] = false
          this.dataLoaded[widgetId] = true 
          return this.resourceData[widgetId]

        }
},      
    
async  fetchResourceListFiltered (widgetId:string, resourceId:string,presentationId:number, taskId:string,pageInstanceId:string,filter:any,form_layout:any,columns_for_query:any,ver:number,rversion:number, child?:Record<PropertyKey,any>,useCache?:boolean)
{
  this.fetchDataInProgress[widgetId] = true
  let widgetKey:string = pageInstanceId+'-'+widgetId
  try {    
      let post_data:Record<PropertyKey,any> = {'filter':filter,'form':form_layout,'columns':columns_for_query};
      if (child) post_data['child'] = child
      
      if (!useCache) this.dataLoaded[widgetKey] = false
      
      if (this.resourceData[widgetKey]===undefined || !this.dataLoaded[widgetKey] || this.staleFlag[resourceId]>this.staleFlag[widgetKey] || !useCache)  { 
        
          const api_response:any = await getApi().post(this.api.server + 'kt-task-dataset-list/'+ resourceId +'/'+presentationId+'/'+taskId+'/'+widgetId+'/'+ver+'/'+rversion+'/', post_data)
          const formStore  = useResourceFormStore()
        if (api_response?.status===200) {
            
            Object.assign(  this.resourceData[widgetKey], api_response?.data) 
            this.staleFlag[widgetKey] = new Date().getTime()
            this.dataLoaded[widgetKey] = true
            formStore.setFormData(pageInstanceId,widgetId,this.resourceData[widgetKey]) 
        } else {
            const alertStore = useAlertStore();
            this.getAlertStore.error(new alertMessage(api_response.msg,api_response.msg,api_response.status))
        }
      }
  } catch (err:any) {
      this.getAlertStore.error(new alertMessage(err.message,err.message,err.status))
  } finally{
      this.fetchDataInProgress[widgetId] = false
      return this.resourceData[widgetKey]
    }
},

async getResourceDataFn(resourceId: string,widgetId:string,pageInstanceId: string,version:number,filter:any,child:any={}) {      
  let widgetData:any = {}
  try {
      this.fetchDataInProgress[widgetId] = true
      this.dataLoaded[widgetId] = false
      if (this.resourceData[widgetId]===undefined || !this.staleFlag[resourceId] || this.staleFlag[resourceId]>this.staleFlag[widgetId])  { 
       
        //delete null and undefined values - for now
        for (const [key, value] of Object.entries(filter)) {
          if (!value) delete filter[key]
        }
        const api_response:any = await getApi().post(this.api.server+'get-dataset-data/'+ resourceId +'/'+ version +'/',{'filter':filter,'child':child});   
        const formStore  = useResourceFormStore()
        if (api_response?.status===200) {
          if (!this.resourceData[resourceId]) this.resourceData[resourceId]=[]
          widgetData = api_response?.data
          // check if data exists and replace
          if (Array.isArray(api_response?.data)) {
            this.resourceData[resourceId].push(...api_response?.data)
            
          }

          else  this.resourceData[resourceId].push(api_response?.data)

          this.staleFlag[widgetId] = new Date().getTime()
          this.dataLoaded[widgetId] = true    
          formStore.setFormData(pageInstanceId,widgetId,widgetData)
 

        } else{
          let msg='Error in get-dataset-data call'
          if (api_response.msg) msg=api_response.msg
          else if (api_response.data) msg=api_response.data

          this.getAlertStore.error(new alertMessage(msg,msg,api_response.status))
        }

        
      }

    } catch (err:any) {
      this.getAlertStore.error(new alertMessage(err.message,err.message,err.status))
    } finally{
      this.fetchDataInProgress[widgetId] = false
      return widgetData

    }
},   

async fetchResourceDataListAll(widgetId:string,pageInstanceId:string,resourceId: string,presentationId: number,lang:number,post_data:any) {      
  
   
  try {
          this.fetchDataInProgress[widgetId] = true
          
          if  (!this.resourceData[widgetId])  this.resourceData[widgetId]={data:[]}
          const formStore  = useResourceFormStore()
          if (this.resourceData[widgetId]===undefined || !this.dataLoaded[widgetId] || this.staleFlag[resourceId]>this.staleFlag[widgetId])  { 
            const api_response:any = await getApi().post(this.api.server+'get-resource-data-list-all/'+ resourceId +'/'+ presentationId +'/'+lang+'/',post_data);   
            if (api_response?.status===200) {
               //this.resourceData[widgetId] = api_response?.data //lost reactivity
             // api_response.data.data=reactive( api_response?.data.data) //not needed - already reactive
             Object.assign( this.resourceData[widgetId].data, api_response?.data) //  to retain reactivity
              this.staleFlag[widgetId] = new Date().getTime()
              this.dataLoaded[widgetId] = true
              formStore.setFormData(pageInstanceId,widgetId,this.resourceData[widgetId].data)
            } else
          {
            let msg='Error in get-resource-data-list-all call'
            if (api_response.msg) msg=api_response.msg
            else if (api_response.data) msg=api_response.data

            this.getAlertStore.error(new alertMessage(msg,msg,api_response.status))
          }
        }else  formStore.setFormData(pageInstanceId,widgetId,this.resourceData[widgetId].data)
            

        } catch (err:any) {
          this.getAlertStore.error(new alertMessage(err.message,err.message,err.status))
        } finally{
          this.fetchDataInProgress[widgetId] = false
          this.dataLoaded[widgetId] = true 
          return this.resourceData[widgetId]

        }
},      
    
async addResourceRecord(resourceDefId: string,widgetId: string, record:any,resourceVersion:number,resourceInstanceId:string, prevResourceId: string ) {
  
        
  let response:any=null
  try {
     
        this.fetchDataInProgress[widgetId] = true   
        if (prevResourceId==undefined) prevResourceId='0'
        if (resourceDefId==undefined) resourceDefId=''
        if (resourceInstanceId==undefined) resourceInstanceId='0'
        if (resourceVersion==undefined) resourceVersion=0
        const api_response:any = await getApi().post(this.api.server+'kt-task-add-resource/'+resourceDefId+'/'+widgetId+'/'+resourceVersion+'/'+resourceInstanceId+'/'+prevResourceId+'/',record);   
        //get filter in here
        //const api_response:any = await api.post(this.api.server+'get-rdbms-resource-record/'+prevResourceId+'/'+resourceInstanceId+'/'+resourceDefId+'/'+recordId+'/');   
        
        if (api_response?.status===200) {
          response = api_response?.data
          let rmsg:string = response?.ResponseMsg
          if (!rmsg) rmsg="Record created successfuly!"
        
          this.getAlertStore.success(new alertMessage(rmsg,rmsg,api_response.status))
        } else{
          this.getAlertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status))
        }

    } catch (err:any) {   
      this.getAlertStore.error(new alertMessage(err.message,err.message,err.status))
    } finally{
      this.fetchDataInProgress[widgetId] = false
      return response
    }
},
async addResourceRecordWithChild(resourceDefId: string,widgetId: string, record:any,resourceVersion:number,resourceInstanceId:string, prevResourceId: string ) {
  
        
  let response:any=null
  try {
     
        this.fetchDataInProgress[widgetId] = true   
        if (prevResourceId==undefined) prevResourceId='0'
        if (resourceDefId==undefined) resourceDefId=''
        if (resourceInstanceId==undefined) resourceInstanceId='0'
        if (resourceVersion==undefined) resourceVersion=0
        const api_response:any = await getApi().post(this.api.server+'kt-task-add-resource-with-child/'+resourceDefId+'/'+widgetId+'/'+resourceVersion+'/'+resourceInstanceId+'/'+prevResourceId+'/',record);   
        //get filter in here
        //const api_response:any = await api.post(this.api.server+'get-rdbms-resource-record/'+prevResourceId+'/'+resourceInstanceId+'/'+resourceDefId+'/'+recordId+'/');   
        
        if (api_response?.status===200) {
          response = api_response?.data
          let rmsg:string = response?.ResponseMsg
          if (!rmsg) rmsg="Record created successfuly!"
        
          this.getAlertStore.success(new alertMessage(rmsg,rmsg,api_response.status))
        } else{
          this.getAlertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status))
        }

    } catch (err:any) {   
      this.getAlertStore.error(new alertMessage(err.message,err.message,err.status))
    } finally{
      this.fetchDataInProgress[widgetId] = false
      return response
    }
},
async updateResourceRecord(resourceDefId: string,pageDefId: string,widgetId: string, record:any,resourceVersion:number,resourceInstanceId:string, prevResourceId: string ) {
  
        
  let response:any=null
  try {
     
        this.fetchDataInProgress[widgetId] = true   
        if (prevResourceId==undefined) prevResourceId='0'
        if (resourceDefId==undefined) resourceDefId=''
        if (resourceInstanceId==undefined) resourceInstanceId='0'
        if (resourceVersion==undefined) resourceVersion=0
        const api_response:any = await getApi().post(this.api.server+'kt-task-update-resource/'+pageDefId+'/'+widgetId+'/'+resourceVersion+'/',record);   
        //get filter in here
        //const api_response:any = await api.post(this.api.server+'get-rdbms-resource-record/'+prevResourceId+'/'+resourceInstanceId+'/'+resourceDefId+'/'+recordId+'/');   
        
        if (api_response?.status===200) {
          response = api_response?.data
          let rmsg:string = response?.ResponseMsg
          if (!rmsg) rmsg="Record updated successfuly!"
          this.staleFlag[resourceDefId] = new Date().getTime()
          let id_key:string = 'resource'+resourceDefId+'_id'
          let recordId:number = record[id_key]
          let index:number= this.resourceData[resourceDefId].map(function(x:any) {return x[id_key]; }).indexOf(recordId)

                  if (index>=0) Object.assign( this.resourceData[resourceDefId][index],api_response?.data?.record)
                  else {
                    this.resourceData[resourceDefId].push(api_response?.data.record)
                    index = this.resourceData[resourceDefId].length-1
                  }

          this.getAlertStore.success(new alertMessage(rmsg,rmsg,api_response.status))
        } else{
          this.getAlertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status))
        }

    } catch (err:any) {   
      this.getAlertStore.error(new alertMessage(err.message,err.message,err.status))
    } finally{
      this.fetchDataInProgress[widgetId] = false
      return response
    }
},
async processR2rMapping(widgetId:string,resourceId:string,resourceVersion:number,mappings:any,filter_data:any,resourceInstanceId:string,prevResourceId:string){
  
  let response:any={}
  try {
     
    this.fetchDataInProgress[widgetId] = true   
    if (prevResourceId==undefined) prevResourceId='0'
    if (resourceId==undefined) resourceId=''
    if (resourceInstanceId==undefined) resourceInstanceId='0'
    if (resourceVersion==undefined) resourceVersion=0

    let post_data = {'mappings':mappings,'filter':filter_data};
    const api_response:any = await getApi().post(this.api.server+'process-r2r-mapping/'+widgetId+'/'+resourceId+'/'+resourceVersion+'/'+resourceInstanceId+'/'+prevResourceId+'/',post_data);   
    //get filter in here
    //const api_response:any = await api.post(this.api.server+'get-rdbms-resource-record/'+prevResourceId+'/'+resourceInstanceId+'/'+resourceDefId+'/'+recordId+'/');   
    
    if (api_response?.status===200) {
      response = api_response?.data
      //let rmsg:string = response?.ResponseMsg
      //if (!rmsg) rmsg="Record updated successfuly!"
      //this.getAlertStore.success(new alertMessage(rmsg,rmsg,api_response.status))
    } else{
      this.getAlertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status))
    }

  } catch (err:any) {   
    this.getAlertStore.error(new alertMessage(err.message,err.message,err.status))
  } finally{
    this.fetchDataInProgress[widgetId] = false
    return response
  }

},
setRecordById(widgetId: string,resourceDefId:string,recordId:number) {
            
  try {
    
      if (recordId){
        let recId:string = resourceDefId+'_'+recordId.toString()
        if (!this.resourceData[recId]){
          this.resourceData[recId] = this.resourceData[widgetId]
          this.dataLoaded[recId] = true
        }
      }
    } catch (err:any) {
      
      const alertStore = useAlertStore();
      this.getAlertStore.error(new alertMessage(err.message,err.message,err.status))
    }
},
setDataObject(recId: string,pageInstanceId:string | undefined) {
            
  try {
      if (!this.resourceData[recId]) this.resourceData[recId] = {data:{}}
      const formStore  = useResourceFormStore()
      if (pageInstanceId) formStore.setFormData(pageInstanceId,recId,this.resourceData[recId].data)
    } catch (err:any) {
      
      const alertStore = useAlertStore();
      this.getAlertStore.error(new alertMessage(err.message,err.message,err.status))
    }
},
setDataObjectArray(recId: string) {
            
  try {
      if (!this.resourceData[recId]) this.resourceData[recId] = {data:[]}
    } catch (err:any) {
      
      const alertStore = useAlertStore();
      this.getAlertStore.error(new alertMessage(err.message,err.message,err.status))
    }
},
  }

});