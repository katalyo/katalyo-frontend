//import { ref, computed } from "vue"; - check later if ref is needed
import { defineStore } from "pinia";
import KatalyoApi from '@/g_libs/kt-api';
import { useAlertStore,alertMessage } from '@/g_stores/alert';
import { usePageStore } from '@/m_page/stores/page';
import Utility from "../../g_libs/kt-utility";
//using options API for now (if there's a need switch to composition API)

export const useResourceDefinitionStore = defineStore({
  
  id: "resourceDefinition",

  state: () => ({
        
        resourceDefinitions: {} as Record<PropertyKey, any> ,
        publishedResources: {} as Record<PropertyKey, any> ,
        fetchRDInProgress: {} as Record<PropertyKey, boolean> ,
        resourcesLoaded: {} as Record<PropertyKey, boolean>,
        publishedResourceLoaded: {} as Record<PropertyKey, boolean>,
        resourceState: {} as Record<PropertyKey, any>,
        resourceFormToolbox: {} as Record<PropertyKey, any> ,
        resourceToolboxLoaded: {} as Record<PropertyKey, boolean>,
        api:{} as Record<PropertyKey,any>,
  
    }),
 
  getters: {
    getResourceDefinition: (state) => {return (resourceId:string) => state.resourceDefinitions[resourceId]},
    getResourceForm: (state) => {return (resourceId:string) => state.resourceDefinitions[resourceId]?.form},
    getPublishedResource: (state) => {return (resourceId:string,publishType:string,version:number) => state.publishedResources[resourceId+'_'+publishType+'_'+version.toString()]?.form},
    getState: (state) => {return (resourceId:string) => state.resourceState[resourceId]},
    getWidgetState: (state) => {return (resourceId:string,widgetId:string) => state.resourceState[resourceId]?.[widgetId]},
    getResourceFormToolbox: (state) => {return (resourceType:string) => state.resourceFormToolbox[resourceType]},
    getToolboxItem: (state) => {return (resourceType:string,elementType:string) => state.resourceFormToolbox[resourceType].filter((item:any)=>item.ElementType===elementType)},
    getResourceLoaded: (state) => {return (resourceId:string) => state.resourcesLoaded[resourceId]},
    getResourceToolboxLoaded: (state) => {return (resourceType:string) => state.resourceToolboxLoaded[resourceType]},


    },

  actions: {

     
    async fetchResourceDefinition(resourceId: string,lang: number) {
            
            try {
                
                const pageStore = usePageStore()
                let utility:Utility = new Utility()
                let widgets:Array<any> =[]
                let i:number
                if (this.resourceDefinitions[resourceId]===undefined)
                { 
                  this.resourcesLoaded[resourceId] = false
                  const api_main = KatalyoApi()
                  const api = api_main.KatalyoApiClass
                  //const api_response:{} | { status: number; msg: any; data: any; } = await api.get(this.api.server+'get-resource-form/'+resourceId+'/i/'+lang+'/0/');   
                  const api_response:any = await api.get(this.api.server+'get-resource-form/'+resourceId+'/i/'+lang+'/0/');   
                
                  if (api_response?.status===200)
                  {
                    this.resourceDefinitions[resourceId] = {'definition':api_response?.data?.resourceDefinition,'form':api_response?.data?.widgets}
                   
                    
                    widgets = utility.processForm(this.resourceDefinitions[resourceId].form)
                    pageStore.addWidgetsToPage(widgets)
                      
                    this.resourcesLoaded[resourceId] = true
                    
                  }else{
                  //store error in alertStore
                  const alertStore = useAlertStore();
                  alertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status))
                  }
                }else{

                 
                 widgets = utility.processForm(this.resourceDefinitions[resourceId].form)
                 pageStore.addWidgetsToPage(widgets)
                 
                }

              } catch (err:any) {
                
                const alertStore = useAlertStore();
                alertStore.error(new alertMessage(err.message,err.message,err.status))

              }finally{
                return this.resourceDefinitions[resourceId]
              }
    },
    async fetchWidgetToolbox(resourceType: string) {
            
      try {

          if (this.resourceFormToolbox[resourceType]===undefined)
          { 
            const api_main = KatalyoApi()
            const api = api_main.KatalyoApiClass
            //const api_response:{} | { status: number; msg: any; data: any; } = await api.get(this.api.server+'get-resource-form/'+resourceId+'/i/'+lang+'/0/');   
            const api_response:any = await api.get(this.api.server+'get-widget-toolbox/'+resourceType+'/');   

          
            if (api_response?.status===200)
            {
              this.resourceFormToolbox[resourceType] =api_response?.data
              this.resourceToolboxLoaded[resourceType] = true

            }else{
              //store error in alertStore
              const alertStore = useAlertStore();
              alertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status))
            }
          }

        } catch (err:any) {
          
          const alertStore = useAlertStore();
          alertStore.error(new alertMessage(err.message,err.message,err.status))

        }finally{
          return this.resourceFormToolbox[resourceType]
        }
},
async fetchPublishedResourceVersion(resourceId:string,publishType:string,version:number,envType?:string | undefined) {
  
  const keyId:string = resourceId+'_'+publishType+'_'+version.toString()
  if (!envType) envType='dev'
  try {
     
      if (this.publishedResources[keyId]===undefined)
      { 
        
        const api_main = KatalyoApi()
        const api = api_main.KatalyoApiClass
            const api_response:any = await api.get(this.api.server+'get-published-resource-version/'+resourceId+'/'+publishType+'/'+envType+'/'+version+'/');   

          
            if (api_response?.status===200)
            {
              this.publishedResources[keyId] = api_response?.data
              this.publishedResourceLoaded[keyId] = true
             
            }else{
            //store error in alertStore
            const alertStore = useAlertStore();
            alertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status))
            }
        }

    } catch (err:any) {
      
      const alertStore = useAlertStore();
      alertStore.error(new alertMessage(err.message,err.message,err.status))

    }finally{
      return this.publishedResources[keyId]
    }
},
addWidgetToForm(item: any,resourceId:string) {
            
    try {

          if (this.resourceDefinitions[resourceId]!== undefined) this.resourceDefinitions[resourceId]?.form.push(item)
       
    } catch (err:any) {
      
      const alertStore = useAlertStore();
      alertStore.error(new alertMessage(err.message,err.message,err.status))

    }
},
setState(resourceId: string,resourceState:any) {
            
  try {

        const pageStore = usePageStore()
        if (pageStore.currentPage.pageState===undefined) pageStore.currentPage.pageState={}
        pageStore.currentPage.pageState[resourceId]=resourceState
        this.resourceState[resourceId] = resourceState
     
  } catch (err:any) {
    
    const alertStore = useAlertStore();
    alertStore.error(new alertMessage(err.message,err.message,err.status))

  }
},
    createCssLink (api_url:string,pageId:string)
    {
        let head = document.getElementsByTagName('HEAD')[0]

        // Create new link Element
        let link = document.createElement('link')
 
        // set the attributes for link element
        link.rel = 'stylesheet'
     
        link.type = 'text/css'
     
        link.href = `${api_url}${pageId}.css`
        
        // Append link element to HTML head
        head.appendChild(link);
    }

  }

});