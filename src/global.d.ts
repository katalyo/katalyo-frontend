// src/global.d.ts
import { ComponentCustomProperties } from 'vue';

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $myGlobalProperty:  Record<string, any>,
    $apiconfig:  Record<string, any>
  }
}

