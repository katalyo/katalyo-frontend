import { createRouter, createWebHistory, type LocationQueryValue } from "vue-router"
import AppDefaultLayout from '@/g_layouts/AppDefaultLayout.vue'
import { useAuthUserStore } from '@/m_auth/stores/authUser'
import { useApplicationStore } from '@/g_stores/application'
import { useAppMenuStore } from '@/g_stores/appMenu'
import { useCookies,globalCookiesConfig } from "vue3-cookies"
import KatalyoApi from '@/g_libs/kt-api'

globalCookiesConfig({
  expireTimes: "1d",
  path: "/",
  //domain: "*.katalyo.com",
  secure: false,
  sameSite: "None",
});


declare global {
  interface Window {     grecaptcha: any;    }
}
declare global {
  interface Window {     recaptchaCallback: any;    }
}

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: () => import("../g_views/IndexView.vue"),
      meta: { layout: AppDefaultLayout } ,
    },
    {
      path: "/home",
      name: "app.navbar.home",
      component: () => import("../g_views/DashboardView.vue"),
    },
    {
      path: "/login",
      name: "login",
      component: () => import("../m_auth/views/LoginView.vue"),
      meta: { layout: AppDefaultLayout } ,
    },
    {
      path: "/register/:signup_key",
      name: "register",
      component: () => import("../m_auth/views/RegisterView.vue"),
      meta: { layout: AppDefaultLayout } ,
    },
    {
      path: "/changepasswordrequest",
      name: "changepasswordrequest",
      component: () => import("../m_auth/views/ChangePasswordRequest.vue"),
      meta: { layout: AppDefaultLayout } ,
    }, 
    {
      path: "/changepass/:digest",
      name: "changepass",
      component: () => import("../m_auth/views/ChangePassword.vue"),
      meta: { layout: AppDefaultLayout } ,
    }, 
    {
      path: "/signup",
      name: "signup",
      component: () => import("../m_auth/views/SignupView.vue"),
      meta: { layout: AppDefaultLayout } ,
    },  
    {
      path: "/onboarding",
      name: "onboarding",
      component: () => import("../m_auth/views/OnboardingView.vue"),
      meta: { layout: AppDefaultLayout } ,
    },  
     {
      path: "/dashboard",
      name: "dashboard",
      component: () => import("../g_views/DashboardView.vue"),
    },
    {
      path: "/helpedit",
      name: "helpedit",
      component: () => import("../g_views/HelpEditView.vue"),
    },
    {
      path: "/selectenv/:type",
      name: "app.navbar.selectenv",
      component: () => import("../g_views/SelectEnvView.vue"),
    },
     {
      path: "/logout",
      name: "logout",
      component: () => import("../m_auth/views/LogoutView.vue"),
      meta: { layout: AppDefaultLayout } ,
    },
    {
      path: "/form-definition/:resource_id",
      name: "form.definition",
      component: () => import("../g_views/ResourceFormDefinitionView.vue"),
    },
     {
      path: "/login",
      name: "app.login",
      component: () => import("../m_auth/views/LoginView.vue"),
    },
    {
      path: "/users/:page_id",
      name: "app.navbar.users",
      component: () => import("../m_auth/views/UsersDefinitionView.vue"),
    },
    {
      path: "/groups/:page_id",
      name: "app.navbar.groups",
      component: () => import("../m_auth/views/GroupsDefinitionView.vue"),
    },
    {
      path: "/codes/:page_id",
      name: "app.navbar.codes.list",
      component: () => import("../m_auth/views/CodesView.vue"),
    },
    {
      path: "/codesnew/:code_id",
      name: "app.navbar.codes.new",
      component: () => import("../m_auth/views/CodesNew.vue"),
    },
    {
      path: "/user",
      name: "app.navbar.user.details",
      component: () => import("../m_auth/views/UserDetailsView.vue"),
    },
     {
      path: "/vpage/:page_id/:page_version",
      name: "app.navbar.vpage",
      component: () => import("../g_views/ResourcePageView.vue")
    },
     {
      path: "/new-resource/:page_id",
      name: "app.navbar.resources.new",
      component: () => import("../g_views/NewResourceView.vue"),
    },
    {
      path: "/resources-list/:page_id",
      name: "app.navbar.resources.list",
      component: () => import("../g_views/ResourceListView.vue"),
    },
    
    {
      path: "/community/:page_id",
      name: "app.navbar.organisation",
      component: () => import("../g_views/CommunityView.vue"),
    },
    {
      path: "/permissions/:page_id",
      name: "app.navbar.appsettings",
      component: () => import("../g_views/SettingsView.vue"),
    } ,
    {
      path: "/settings",
      name: "app.navbar.settings",
      component: () => import("../g_views/SettingsView.vue"),
    },
    {
      path: "/app-messages/:uid",
      name: "app-messages",
      component: () => import("../g_views/AppMessagesView.vue"),
    }
  ],
});



router.beforeEach(async (to, from) => {
  
  if (from.path===to.path && from.query.rinstance!="0" && to.query.rinstance=="0") return false
 
  const appStore = useApplicationStore()
  const menuStore = useAppMenuStore()
  const {cookies} = useCookies()
  const api_main = KatalyoApi()
  const authStore = useAuthUserStore();
  const csrf_token= cookies.get('csrftoken')

  /* // if not authenticated redirect the user to the login page, this is most probably not needed and causes issues with angular
  
  if (!userStore.getIsAuthenticated && to.name !== 'login') {
      if (sessionStorageSessionId==undefined) {  // if opened new TAB
            const loginResult = await authStore.loginNewTab( session_data);
      }
  }     */
 
  
  let final_env:string = to.query.env?.toString() ||  appStore.currentEnv || authStore.getCurrentUser?.uep?.defaultenv
  
  if (to.name==='logout') final_env=''
  //api_main.set_env(final_env)

  //set session data for open in new tab
  if (!sessionStorage.getItem('server_sessionid') && to?.query?.session_id)   
      authStore.setCurrentUserBySessionId(to?.query?.session_id.toString())
    
  //setup header for api calls
  api_main.add_auth_token(csrf_token, authStore.getCurrentUser?.authdata) 
  if (api_main.get_env()!=final_env) api_main.set_env(final_env)
  
  if (final_env!=appStore.currentEnv) 
  {
      appStore.currentEnv=final_env
      appStore.fetchApplicationList()
  }
  let resp_handlers:any = {}
  resp_handlers[401] = authStore.redirect2Login
  api_main.set_response_handlers( resp_handlers ) 

  let app_start:any

  if (to.path!=from.path) menuStore.hideAllDropwdowns()

  //load app if "app_ref" exists
  if (  (to.query.app_ref || from.query.app_ref)
        && appStore.getCurrentApp.uid === undefined
        && to.name !=='logout'  
        && to.name !=='login'
        && from.name !=='logout'
        &&  appStore.getCurrentApp.uid !== to.query.app_ref
      )  
      app_start = await  appStore.startApplication({ uid: to.query.app_ref || from.query.app_ref}, from.name != 'login')
  //else if ((!to.query.app_ref && !from.query.app_ref) || to.name=='dashboard')  menuStore.setDefaultMenu()
    
      let v_app_ref:LocationQueryValue | LocationQueryValue[]
      if (!to.query.app_ref) v_app_ref=from.query?.app_ref
      else v_app_ref = to.query.app_ref
  
   if ( to.query.env==undefined && final_env ) return {...to, query: { ...to.query, app_ref: v_app_ref,session_id: to.query.session_id  , env: final_env} }
  
  

  return true

})

export default router;
