import { createApp,markRaw } from "vue";
import { createPinia } from "pinia";
import VueCookies from 'vue3-cookies';
import config,{setconf} from './config';
import App from "./App.vue";
import router from "./g_router";
import "./g_assets/main_angular.css";
import "./g_assets/main.css";
import "./g_assets/master.css";
import { QuillEditor } from '@vueup/vue-quill'
import '@vueup/vue-quill/dist/vue-quill.snow.css';

export const app = createApp(App);
const pinia = createPinia()

app.config.performance = true

const loadLabels = async () => {
  const response = await fetch("/config/labels.json");
  const label = await response.json();
  app.config.globalProperties.$myGlobalProperty = label
 
}

const loadConfig = async () => {

  await loadLabels()
 
  app.config.globalProperties.$apiconfig =  await setconf()
  app.config.globalProperties.$config = config
  pinia.use(app.config.globalProperties.$config)

  pinia.use(({ store }) => {
    store.$router = markRaw(router)
  });
  
  app.use(pinia);
  
  app.use(router);
  
  app.use(VueCookies);
  app.component('QuillEditor', QuillEditor)
  const components:any = import.meta.glob('./g_components/widgets/*.vue', { eager: true })

  Object.entries(components).forEach(([path, definition]:[string,any]) => {
    // Get name of component, based on filename
    // "./components/Fruits.vue" will become "Fruits"
    const componentName:string | undefined = path?.split('/')?.pop()?.replace(/\.\w+$/, '')

    // Register component on this Vue instance
    if (definition !==undefined && componentName!==undefined) app.component(componentName, definition?.default)
  })
  app.mount("#app");
}

loadConfig()













