//import config from 'config.json' 

let config:any
export default function configuration()
{

		const env:string = config.env || 'development'
		//const env:string = process.env.NODE_ENV || 'miro'
		const development:Record<PropertyKey,any> = config.development

		const production:Record<PropertyKey,any> = config.production

		const conf:Record<PropertyKey,any> = {
		development,
		production,
		}
		return conf[env]
}

async function setconf(){
	const response = await fetch("/config/config.json");
	config = await response.json();
	return config
}

export {setconf}
