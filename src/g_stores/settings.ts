//import { ref, computed } from "vue"; - check later if ref is needed
import { defineStore } from "pinia";
import KatalyoApi from '@/g_libs/kt-api';
import { useAlertStore,alertMessage } from '@/g_stores/alert';
import { usePageStore } from '@/m_page/stores/page';
import { useAuthUserStore } from '@/m_auth/stores/authUser';


class codeItem {
  id: number | string;
  name:string;
  value:string;
  active:boolean;
 
  constructor (_id:number | string,_name:string,_value:string,_active:boolean){
    this.id = _id
    this.name = _name
    this.value = _value
    this.active = _active
  }
}

interface ICodeHead {
  CodeName?: string,
  CodesHeadId?: string,
  Description?: string,
  Lang?:string
}


const useSettingsStore = defineStore({
  id: "settings",
  state: () => ({    
        organisation: {} as Record<PropertyKey,any> ,
        balances: [] as Array<any>,
        transactions: [] as Array<any>,
        itemsLoaded:{} as Record<PropertyKey,boolean>,
        fetchInProgress: {} as Record<PropertyKey,boolean> ,
        loadBalanceStatus:403 as Number,
        api:{} as Record<PropertyKey,any>,
    }),
 
  getters: {
        getApi: (state) => {
            return KatalyoApi().KatalyoApiClass 
        },
        getAlertStore: (state) => {
            return useAlertStore() 
        },
        getAuthStore: (state) => {
          return useAuthUserStore() 
        }
    },

  actions: {

    
    async getOrganisation(orgid:number) {
            
        try {
            if (!this.itemsLoaded['organisation']){

            
            let url = '../api/organisations/'+orgid+'/';
            let api_response= await this.getApi.get(this.api.server + url)
      
            if (api_response?.status===200) {    
              this.organisation = api_response.data
              this.itemsLoaded['organisation'] = true
              return this.organisation

            } else {
                  this.getAlertStore.error(new alertMessage(api_response.msg,api_response.msg,api_response.status,'settingsStore.getOrganisation'))
            }
            }else return this.organisation

        } catch (err:any) {
          
          this.getAlertStore.error(new alertMessage(err.message,err.message,err.status,'settingsStore.getOrganisation'))
  
        }
      },

      async getCredits(balanceType:string) {
            
        try {
        
            if (balanceType == undefined)  balanceType='user'
                
            if (!this.itemsLoaded['balances']){

            
                let url = '../api/get-credits/'+balanceType+'/';
                let api_response= await this.getApi.get(this.api.server + url)
                this.loadBalanceStatus = api_response.status
                if (api_response?.status===200) {    
                  this.balances = api_response.data?.balances
                  this.transactions = api_response.data?.trans
                  this.itemsLoaded['balances'] = true
                  return true
    
                } else {
                      this.getAlertStore.error(new alertMessage(api_response.data?.msg,api_response.data?.msg,api_response.status,'Api','settingsStore.getCredits'))
                      if (api_response.status==403)  {
                        this.itemsLoaded['balances'] = true
                        this.loadBalanceStatus = api_response.status
                      }
                      return false
                    }
            }else return true

        } catch (err:any) {
          
          const alertStore = useAlertStore();
          alertStore.error(new alertMessage(err.message,err.message,err.status))
  
        }
      },
    
      async saveOrganisation() {
            
      try {
        let url = '../api/organisations/'+this.organisation.id+'/';
        let api_response= await this.getApi.post(this.api.server + url,this.organisation)
        
        if (api_response?.status===200) {    
            this.getAlertStore.success(new alertMessage(api_response.data?.msg,api_response.data?.msg,api_response.status,'settingsStore.saveOrganisation'))
            return true

          } else {
                this.getAlertStore.error(new alertMessage(api_response.msg,api_response.msg,api_response.status,'settingsStore.saveOrganisation'))
                return false
          }
      } catch (err:any) {
        
        
        this.getAlertStore.error(new alertMessage(err.message,err.message,err.status))

      }
    }

  }

});

export {useSettingsStore} 
