//import { ref, computed } from "vue"; - check later if ref is needed
import { defineStore } from "pinia";
import KatalyoApi from '@/g_libs/kt-api';
import { useAlertStore,alertMessage } from '@/g_stores/alert';
import { useApplicationStore } from '@/g_stores/application'


class codeItem {
  id: number | string;
  name:string;
  value:string;
  active:boolean;
 
  constructor (_id:number | string,_name:string,_value:string,_active:boolean){
    this.id = _id
    this.name = _name
    this.value = _value
    this.active = _active
  }
}

interface ICodeHead {
  CodeName?: string,
  CodesHeadId?: string,
  Description?: string,
  Lang?:string
}


const useCodesStore = defineStore({
  id: "codes",
  state: () => ({    
        codeList: {} as Record<PropertyKey,Array<any>> ,
        codeHeadList: [] as Array<ICodeHead>,
        codesLoaded:{} as Record<PropertyKey,boolean>,
        codesStale:{} as Record<PropertyKey,boolean>,
       // codeHeadList: {} as Record<PropertyKey,Array<codeItem>> ,
        codesHeadLoaded:{} as Record<PropertyKey,boolean>,
        codesHeadStale:{} as Record<PropertyKey,boolean>,
        fetchCodesInProgress: {} as Record<PropertyKey,boolean> ,
        fetchCodeHeadInProgress: {} as Record<PropertyKey,boolean> ,
        api:{} as Record<PropertyKey,any>,
    }),
 
  getters: {
    getCodeHeadListFiltered: (state) => {  
      return (searchValue:string) =>   {
          return state.codeHeadList.filter( (code) =>  
            {
            if (searchValue==undefined || searchValue=='') return true
            if (code?.CodeName) 
                return   code.CodeName.includes(searchValue)
            else 
                return false
          })
     }   },
    getCodeList: (state) => {  
       return (codeId:string ,lang:string,condition:string="") =>   {
        let codes:Array<any>=[]
        
        if (condition && condition.length) codes = condition.split(";")
        
        let list:Array<any> 
        const appStore = useApplicationStore()
        if (codes.length) list = state.codeList[codeId+'-'+lang.toString()+'-'+appStore.currentEnv].filter((item)=>codes.includes(item.id.toString()))
        else list = state.codeList[codeId+'-'+lang.toString()+'-'+appStore.currentEnv]
        
        if (list) 
          return  list 
        else 
          return [] 
      }   },

    getCodeListSelected: (state) => {  
        return (codeId:string ,lang:string,isSelected:Record<PropertyKey,boolean>) =>   {
          const appStore = useApplicationStore()
          let list = state.codeList[codeId+'-'+lang.toString()+'-'+appStore.currentEnv] 
         if (list) return list.filter((item)=>isSelected[item.id])
         else return [] 
       }   },
    getCodeByUid: (state) => {  
        return (codeId:string ,lang:string,codeItemUid:string) =>   {
        const appStore = useApplicationStore()
        let list = state.codeList[codeId+'-'+lang.toString()+'-'+appStore.currentEnv]
         let itemList:Array<any> = []
         let cid:number

         if (list) itemList = list.filter((item:any)=>item.uid==codeItemUid)
         
        if (itemList.length) return itemList[0].id
        else return null

       }   },
    getCodesLoaded: (state) => {return (key:string) => state.codesLoaded[key]},
    getFetchCodesInProgress: (state) => {return (codeId:string ,lang:number) => {
      const appStore = useApplicationStore()
      state.fetchCodesInProgress[codeId.toString()+'-'+lang.toString()+'-'+appStore.currentEnv]
      }
    },
    getFetchCodeHeadInProgress: (state) => {return (codeId:string ,lang:number) => {
      const appStore = useApplicationStore()
      state.fetchCodeHeadInProgress[codeId.toString()+'-'+lang.toString()+'-'+appStore.currentEnv]
      }
    },
    getCodeListItemById: (state) => {return  (codeId:string ,lang:number,codeElementId:number) => {
      const appStore = useApplicationStore()
      return state.codeList[codeId.toString()+'-'+lang.toString()+'-'+appStore.currentEnv].find((codeElement)=>codeElement.id==codeElementId)
    }
  }
  
},

  actions: {


    async getCodeHeadLang (uid:any, lang:any )
    {
     const api = KatalyoApi().KatalyoApiClass
     let url = '../api/get-codehead-lang/'+uid+'/'+lang+'/';
     
     let response = await api.get(this.api.server + url)
     if (response.status === 200 || response.status===201) {    
                  // deferred.resolve({data:response.data, status: response.status}); 
           } else {
              // deferred.reject({data:{msg:"Download failed  -> getCodesDetails  ! "+response.status}, status:response.status});
           }
     return response  
  } ,
   async getCodesDetails (id:any, lang:any )
   {
    const api = KatalyoApi().KatalyoApiClass
    let url = '../api/get-codes-detail-bycodeidlang/'+id+'/'+lang+'/';
    
    let response = await api.get(this.api.server + url)
    if (response.status === 200 || response.status===201) {    
                 // deferred.resolve({data:response.data, status: response.status}); 
          } else {
             // deferred.reject({data:{msg:"Download failed  -> getCodesDetails  ! "+response.status}, status:response.status});
          }
    return response  
 } ,          
  
 async saveCodesDetails (codeHead:any, lang:any){
    
  let currentCodeList =  this.getCodeList(codeHead.uid,lang)

  var url = '../api/save-codes-detail/'+codeHead.id+'/'+lang+'/';
  const api = KatalyoApi().KatalyoApiClass
  const alertStore = useAlertStore()
  const appStore = useApplicationStore()
  const api_response:any = await api.send(this.api.server+ url, 'POST', currentCodeList)  
  
    if (api_response.status === 200) {
        this.codesStale[codeHead.uid+'-'+lang.toString()+'-'+appStore.currentEnv] =true
        this.fetchCodesById(codeHead.uid, lang)
        alertStore.info(new alertMessage(api_response.data.msg,api_response.data.msg, api_response.status))
      } else {
             // if not successful, bind errors to error variables
             alertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status))
                               
     }
     
      return api_response
},
  
addNewCodeItem (codeId:string , lang:string,item:any)   {
   const appStore = useApplicationStore()
   this.codeList[codeId+'-'+lang.toString()+'-'+appStore.currentEnv].push(item) 
   this.codesStale[codeId+lang+'-'+appStore.currentEnv] = true
 },
   async addUpdateCodesDetails (type:any,codesDetailLang:any)
   {
    
    //debugger
    const api = KatalyoApi().KatalyoApiClass

    var url = '../api/save-codes-detail/';
    var http_method="";
    let api_response:any ={}

    if (type=="New") { 
            http_method="POST"; 
            api_response = await api.post(this.api.server+ url, codesDetailLang)    
    } else { 
      var id= codesDetailLang.id;
      http_method="PUT";
      url=url+id+'/';
      api_response = await api.put(this.api.server+ url, codesDetailLang)
    }
                  
     if (api_response.status === 201 && type=="New") {
                  // deferred.resolve({msg:"New code item created successfuly !",status:response.status});            
               
        } else if (api_response.status==200 && type=="Edit") {
                        // deferred.resolve({msg:"Code item changed successfuly!",status:response.status});                              
         } else {
                     // deferred.reject({msg:"Save failed ! "+response.status,status:response.status});     
        }


  },

     async CodesGet (lang:string){
    
      let url = '../api/codes/'+lang+'/';

      const api = KatalyoApi().KatalyoApiClass
      const api_response:any = await api.get(this.api.server+ url)
      const alertStore = useAlertStore();
    
       if (api_response.status === 200) {
              
              this.codeHeadList = api_response.data
                    //alertStore.info(new alertMessage(api_response.data,api_response.data,api_response.status))
                    //deferred.resolve({data:response.data, status: response.status});
  
                } else {
                  alertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status))
                   // deferred.reject({data:{msg:"Download failed  -> CodesGet  ! "+response.status}, status:response.status});
                }
           
         return api_response
    },
    async getCodesHead (codeId:string,lang:string){
    
      let url = '../api/codes-head/'+ codeId + '/'+ lang+'/';

      const api = KatalyoApi().KatalyoApiClass
      const api_response:any = await api.get(this.api.server+ url)
      const alertStore = useAlertStore();
    
       if (api_response.status === 200) {
                    //alertStore.info(new alertMessage(api_response.data,api_response.data,api_response.status))
                    //deferred.resolve({data:response.data, status: response.status});
             } else {
                  alertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status))
                   // deferred.reject({data:{msg:"Download failed  -> CodesGet  ! "+response.status}, status:response.status});
                }
           
         return api_response
    },

    async CodesPost (codeHead:any, lang:string)
    {
    
      let url = '../api/codes/'+lang+'/';
      const api = KatalyoApi().KatalyoApiClass
      const api_response:any = await api.post(this.api.server+ url, codeHead)
      
      if (api_response.status === 200 || api_response.status === 201) {
                       return {data: api_response.data.data, status: api_response.status, msg: api_response.data.msg }
           } else   
           return {data: api_response.data, status: api_response.status, msg: api_response.data }
                        
    },

    async fetchCodesById(codeId: string,lang: string) {

            const appStore = useApplicationStore()
            const codeKey:string=codeId.toString() + '-' + lang.toString()+'-'+appStore.currentEnv
            
            try {
              
              if (!this.fetchCodesInProgress[codeKey] && (!this.codesLoaded[codeKey]|| this.codesStale[codeKey]))
              {
                  this.fetchCodesInProgress[codeKey] = true          
                  const api_main = KatalyoApi()
                  const api = api_main.KatalyoApiClass
                  const api_response:any = await api.get(this.api.server+'get-codes-detail-byidlang/'+codeId+'/'+lang+'/');   
                    
                  if (api_response?.status===200)
                  {
                      this.codeList[codeKey]=[]
                      const codeData:Array<codeItem> = api_response?.data
                      this.codeList[codeKey] = codeData
                      this.codesLoaded[codeKey] = true
                      this.codesStale[codeKey] = false
                   }else{
                    //store error in alertStore
                    const alertStore = useAlertStore();
                    alertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status))
                   }
                   this.fetchCodesInProgress[codeKey] = false
              }

              return this.codeList[codeKey]

              
            } catch (err:any) {
                this.fetchCodesInProgress[codeKey] = false
                const alertStore = useAlertStore();
                alertStore.error(new alertMessage(err.message,err.message,err.status))
                return null

            }
    },

     async fetchCodesByName(codeName:string,lang: string) {
            
      
            const appStore = useApplicationStore()
            const codeKey:string=codeName.toString() + '-' + lang.toString()+'-'+appStore.currentEnv
            try {
              
                
              if (!this.codesLoaded[codeKey] || this.codesStale[codeKey])
              {
                this.fetchCodesInProgress[codeKey] = true
               
                const api_main = KatalyoApi()
                const api = api_main.KatalyoApiClass
                const api_response:any = await api.get(this.api.server+'get-codes-detail-byname/'+codeName+'/'+lang+'/');   
                
                if (api_response?.status===200)
                {
                  this.codeList[codeKey] = api_response?.data
                  this.codesLoaded[codeKey] = true
                  this.codesStale[codeKey] = false
                }else{
                //store error in alertStore
                const alertStore = useAlertStore();
                alertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status))
                }
              }

            } catch (err:any) {
              
              const alertStore = useAlertStore();
              alertStore.error(new alertMessage(err.message,err.message,err.status))

            }finally{
              this.fetchCodesInProgress[codeKey] = false
              return this.codeList[codeKey]
            }
    },
    
    setCodeStaleById(codeId:number,lang: string,stale:boolean) {
            
      try {
           
            this.codesStale[codeId+lang] = true
        
      } catch (err:any) {
        
        const alertStore = useAlertStore();
        alertStore.error(new alertMessage(err.message,err.message,err.status))

      }
    },

    setCodeStaleByName(codeName:string,lang: string,stale:boolean) {
            
      try {
            this.codesStale[codeName+lang] = true
      } catch (err:any) {
        
        const alertStore = useAlertStore();
        alertStore.error(new alertMessage(err.message,err.message,err.status))

      }
    }

  }

});

export {useCodesStore,codeItem}
export type {ICodeHead}  
