//import { ref, computed } from "vue"; - check later if ref is needed
import { defineStore } from "pinia";
import Utility from '@/g_libs/kt-utility';
import { usePageStore } from "@/m_page/stores/page";
import { useApplicationStore } from "./application";
import type { DateTime } from "luxon";



class alertMessage {
  
  uid:string;
  message: string;
  messageDetail:string;
  source:string;
  fn:string;
  error:any;
  status:number;
  msgType?:string;
  contentType?:string;
  errorCode?:number;
  pageId?:any;
  appId?:any;
  date?:Date;

 
  constructor (_message:string, _messageDetail:string='', _status:number=0, _source:string='',_fn='',_pageId:any=null,_appId:string='',_error:any={}, _msgType:string='',_contentType:string='',_errorCode:number=0){
    
    const pageStore = usePageStore()
    const appStore = useApplicationStore()
    
    this.uid = new Utility().uuidv4()
    if (_message) this.message = _message
    else this.message='Unknown message error'
    if (_pageId) this.pageId = _pageId
    else this.pageId=pageStore.currentPage
    if (_appId) this.appId = appStore.getApplication(_appId)
    else this.appId=appStore.currentApp
    this.error= _error
    this.source= _source || 'No source'
    this.messageDetail = _messageDetail
    this.status = _status
    this.msgType = _msgType
    this.contentType = _contentType
    this.errorCode = _errorCode
    this.date = new Date()
    this.fn=_fn
  }

}

const useAlertStore = defineStore({
  
  id: "alert",

  state: () => ({
        
        successMsgs: [] as Array<alertMessage>, //defined Message class here
        warningMsgs:[] as Array<alertMessage>,  //defined Message class here
        errorMsgs:[] as Array<alertMessage>,  //defined Message class here
        infoMsgs:[] as Array<alertMessage>,  //defined Message class here
        pendingMessages:[] as Array<alertMessage>,  //defined Message class here
        allMessages:[] as Array<alertMessage>,  //defined Message class here
        showPendingMessages:false as boolean,
        msgDuration:4000 as number,
        hideMessage:{} as Record<string,boolean>,

  
    }),
 
  getters: {
    getErrors: (state) => {return state.errorMsgs},
    getLastErrors: (state) => {return (n:number)=> state.errorMsgs.slice(-n)},
    getInfo: (state) => {return state.infoMsgs},
    getLastInfo: (state) => {return (n:number)=> state.infoMsgs.slice(-n)},
    getWarning: (state) => {return state.warningMsgs},
    getLastWarning: (state) => {return (n:number)=> state.warningMsgs.slice(-n)},
    getSuccess: (state) => {return state.successMsgs},
    getLastSuccess: (state) => {return (n:number)=> state.successMsgs.slice(-n)},
    getMessages: (state) => {return state.allMessages},
    getLastMessages: (state) => {return (n:number)=> state.allMessages.slice(-n)},
    getPendingMessages: (state) => {return state.pendingMessages},
    getMessageByUid: (state) => (uid:string) => {
      let fMsg:any = state.allMessages.filter((msg:any)=>msg.uid==uid)
      if (fMsg && fMsg.length) return fMsg[0]
      else return {}
    },
    getHideMessage: (state) => {return (uid:string)=> state.hideMessage[uid]},
    },

  actions: {
    error(err:alertMessage) {
      
      err.msgType='error'
      this.errorMsgs.push(err)
      this.pendingMessages.push(err)
      this.allMessages.push(err)
      console.log(err.error)
      //debugger
  
    },

    info(info:alertMessage) {
      info.msgType='info'
      this.infoMsgs.push(info)
      this.pendingMessages.push(info) 
      let uid:string = info.uid
      setTimeout(()=>{
        this.hideMessage[uid]=true
        setTimeout(()=>{this.closeMessageByUid(uid);this.hideMessage[uid]=true},2000)
        } ,this.msgDuration)
      this.allMessages.push(info)
           
    },
    warning(warning:alertMessage) {
      warning.msgType='warning'
      this.warningMsgs.push(warning)
      this.pendingMessages.push(warning)
      let uid:string = warning.uid
      setTimeout(()=>{
        this.hideMessage[uid]=true
        setTimeout(()=>{this.closeMessageByUid(uid);this.hideMessage[uid]=true},2000)
        } ,this.msgDuration)
      this.allMessages.push(warning)
           
    },  
    success(success:alertMessage) {
      success.msgType='success'
      this.successMsgs.push(success)
      this.pendingMessages.push(success)
      let uid:string = success.uid
      setTimeout(()=>{
        this.hideMessage[uid]=true
        setTimeout(()=>{this.closeMessageByUid(uid);this.hideMessage[uid]=true},2000)
        } ,this.msgDuration)
      this.allMessages.push(success)
           
    },
    showPendingMessagesFn(){
      this.showPendingMessages=true
    },
    setMsgDuration(duration:number){
      this.msgDuration=duration
    },
    setHideMessage(uid:string,hideState:boolean){
      this.hideMessage[uid]=hideState
    },
    closeMessage(messageIndexId:number){

      this.pendingMessages.splice(messageIndexId,1)
    },
    closeMessageByUid(uid:string){

      let elementIndex =this.pendingMessages.findIndex((element) => element.uid ==uid)

      this.pendingMessages.splice(elementIndex,1)

    },
    closeAllMessages(){

      this.pendingMessages.length = 0
    },
    clearAllMessages(){

      this.pendingMessages.length = 0
      this.errorMsgs.length=0
      this.infoMsgs.length=0
      this.warningMsgs.length=0
      this.successMsgs.length=0
      this.allMessages.length=0
    }
     
  }

});

export {useAlertStore,alertMessage}