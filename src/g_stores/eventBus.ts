import { ref } from "vue"; 
import { defineStore } from "pinia"

export const useEventBusStore = defineStore('eventBus', () => {
  const events = ref(new Map());

  function emit(event:any, data:any) {
    if (events.value.has(event)) {
      events.value.get(event).forEach((callback:any) => {if (callback) callback.fn(data,event,callback.sWidget)});
    }
  }

  function on(event:any, callback:any,sourceWidget:string) {
    if (!events.value.has(event)) {
      events.value.set(event, []);
    }
    let found:boolean=false
    events.value.get(event).forEach((element:any) => {
      if (element.fn.toString()==callback.toString() && element.sWidget==sourceWidget) found=true
    });

    //const index = events.value.get(event).indexOf(callback);
    if (callback && !found) events.value.get(event).push({fn:callback,sWidget:sourceWidget});
  }

  function off(event:any, callback:any) {
    if (events.value.has(event)) {
      //const index = events.value.get(event).indexOf(callback.toString)
      let found:boolean=false
      let idx:number=-1
      events.value.get(event).forEach((element:any,index:number) => {
        if (element.toString()==callback.toString()) 
          {
            found=true
            idx=index
          }
      });
      if (idx !== -1) {
        events.value.get(event).splice(idx, 1);
      }
    }
  }

function subscribe_2_events(inEvents:Array<any>,functions:Record<PropertyKey,any>,pageId:string,sourceWidgetId:string){
    for (let i=0;i<inEvents.length;i++)
    {
        //subscribe to event
        on(pageId+'-'+inEvents[i].EventName+'-'+inEvents[i].EventWidgetId,functions[inEvents[i].EventFn],sourceWidgetId)
    }
}
  return { emit, on, off,subscribe_2_events };
})