//import { ref, computed } from "vue"; - check later if ref is needed
import { computed } from "vue"
import { defineStore } from "pinia";
import { useCookies } from "vue3-cookies";
import KatalyoApi from '@/g_libs/kt-api';
import { useAlertStore,alertMessage } from '@/g_stores/alert';
import { useVuelidate } from '@vuelidate/core'


export const useDatasetStore = defineStore({
  
  id: "dataset",

  state: () => ({
        widgets:{} as Record<PropertyKey,any>,
        //startTaskInProgress: {} as Record<PropertyKey,boolean>,
        widgetsLoaded: {} as Record<PropertyKey, boolean>,
        formData:{} as Record<PropertyKey,any>,
        validationRules:{} as Record<PropertyKey,any>,
        api:{} as Record<PropertyKey,any>,

    }),
 
  getters: {
    getWidget: (state) => {return (widgetId:string)=> state.widgets[widgetId]},
    getDatasetRecords: (state) => {return (widgetId:string)=> state.widgets[widgetId]?.dataRecords},
    getWidgetPresentationId: (state) => {return (widgetId:string)=> state.widgets[widgetId].PresentationId},
    getFormValidations: (state) => {return (widgetId:string,fieldName:string)=> state.widgets[widgetId].$v[fieldName]},
    getWidgetResourceDefinition: (state) => {return (widgetId:string)=> state.widgets[widgetId]?.publishedResource},
    getResourceFormDefinition: (state) => {return (widgetId:string)=> state.widgets[widgetId]?.FormDef},
    getFormData: (state) => {return (widgetId:string)=> state.formData[widgetId]},
    getFormValid: (state) => {return (widgetId:string)=> state.widgets[widgetId].$v.$valid},
    getWidgetLoaded: (state) => {return (widgetId:string) => state.widgetsLoaded[widgetId]},

    },

  actions: {
    

        registerWidget(fd: Record<PropertyKey,any>) {
            
            try {

              this.widgets[fd.UuidRef] = fd
              //setup form validation
              this.formData[fd.UuidRef] = {}
              this.validationRules[fd.UuidRef] = {}
              
            } catch (err:any) {
                const alertStore = useAlertStore()
                alertStore.error(new alertMessage(err.message,err.message,err.status))

            }
        },
      addValidationRule(widgetId: string,fieldId:string,rule:any) {
            
            try {
              
              if (this.formData[widgetId][fieldId]===undefined) this.formData[widgetId][fieldId] = ''
              if (this.validationRules[widgetId][fieldId] ===undefined) this.validationRules[widgetId][fieldId] ={}

              const new_validations = {...this.validationRules[widgetId],...rule}
              
              this.validationRules[widgetId] = new_validations
              const rules = computed(() => {
                  return this.validationRules[widgetId]
              })

              const $v = useVuelidate(rules,this.formData[widgetId])
              this.widgets[widgetId]['$v'] = $v

            } catch (err:any) {
                const alertStore = useAlertStore()
                alertStore.error(new alertMessage(err.message,err.message,err.status))

            }
        },

     validateForm(widgetId: string) {
            
            try {
              
              this.widgets[widgetId].$v.$validate()
              return this.widgets[widgetId].$v.$invalid
            } catch (err:any) {
                const alertStore = useAlertStore()
                alertStore.error(new alertMessage(err.message,err.message,err.status))
                return false
            }

        },
    async fetchPublishedResourceForm(widgetId: string) {
            
            try {

                if (this.widgets[widgetId].publishedResource===undefined)
                { 
                  const publishId:number | undefined =  this.widgets[widgetId].Parameters?.PublishedResourceId
                  const api_main = KatalyoApi()
                  const api = api_main.KatalyoApiClass
                  if (publishId!==undefined)
                  {
                      const api_response:any = await api.get(this.api.server+'get-published-resource-by-id/'+publishId+'/');   

                    
                      if (api_response?.status===200)
                      {
                        this.widgets[widgetId].publishedResource = api_response?.data
                        this.widgetsLoaded[widgetId] = true
                       
                      }else{
                      //store error in alertStore
                      const alertStore = useAlertStore();
                      alertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status))
                      }
                  }else this.widgets[widgetId].publishedResource = {}
                }

              } catch (err:any) {
                
                const alertStore = useAlertStore();
                alertStore.error(new alertMessage(err.message,err.message,err.status))

              }finally{
                return this.widgets[widgetId].publishedResource
              }
    },
  }
});