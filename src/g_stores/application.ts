//import { ref, computed } from "vue"; - check later if ref is needed
import { defineStore } from "pinia";
import { useCookies } from "vue3-cookies";
import KatalyoApi from '@/g_libs/kt-api';
import { useAlertStore,alertMessage } from '@/g_stores/alert';
import { useResourceDefinitionStore } from '@/m_resource/stores/resourceDefinition';
import { useAppMenuStore } from '@/g_stores/appMenu';
import { useAuthUserStore } from '@/m_auth/stores/authUser';
import { usePageStore } from '@/m_page/stores/page';
import type { Router } from 'vue-router';
import Utility from '@/g_libs/kt-utility';

//using options API for now (if there's a need switch to composition API)

export const useApplicationStore = defineStore({
  
  id: "application",

  state: () => ({
        currentApp:{}  as Record<PropertyKey,any>,
        currentEnv:'' as string ,
        debugMode:false as boolean,
        currentAppDefaultPage:{}  as Record<PropertyKey,any>,
        applications:{}  as Record<PropertyKey,any>,
        fetchAppsInProgress : false as  boolean,
        fetchAppListInProgress : false as  boolean,
        startAppInProgress : {} as Record<PropertyKey,boolean>,
        appList:[] as Array<Record<PropertyKey,any>>,
        api:{} as Record<PropertyKey,any>,
        $router:{} as Router,
    }),
 
  getters: {
    getCurrentApp: (state) => {return state.currentApp},
    getApplication: (state) => {return (appId:string)=> state.applications[appId]},
    getAppList: (state) => {return state.appList},
    getSubscribedAppList: (state) => {return state.appList.filter((app) => app.ActiveSub===true && app.uniqueId!='ktlyo_builder')},
    getUnsubscribedAppList: (state) => {return state.appList.filter((app)=>!app.ActiveSub && app.uniqueId!='ktlyo_builder')},
    getStartAppInProgress: (state) => {return (appId:string) => state.startAppInProgress[appId]},
    getFetchAppListInProgress: (state) => {return state.fetchAppListInProgress},
    },

  actions: {
   
       async setCurrentAppById(appId: string,version:number) {
            
            try {

                this.fetchAppsInProgress=true
                const api_main = KatalyoApi()
                const api = api_main.KatalyoApiClass
                if (!version) version=0
                //const api_response = await api.get(this.api.server+'resource-def-all/'+appId+'/');   
                const api_response = await api.get(this.api.server+'load-app/'+appId+'/'+version+'/');   
                
                if (api_response.status===200)
                {
                  const app = api_response.data
                  this.currentApp=app.subscriptions
                  this.applications[appId] = app.subscriptions
                }else{
                  //store error in alertStore
                  const alertStore = useAlertStore()
                  alertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status))
                }

            } catch (err:any) {
                const alertStore = useAlertStore()
                alertStore.error(new alertMessage(err.message,err.message,err.status))

            }finally{
              this.fetchAppsInProgress=false
              return this.currentApp
            }
    },

     async fetchApplicationList() {
            
            try {
                this.fetchAppListInProgress = true
                
                //if (this.appList.length===0)
                //{ 
                  const api_main = KatalyoApi()
                  const api = api_main.KatalyoApiClass
                  const api_response = await api.get(this.api.server+'resource-subscriptions/application/',this.currentEnv);   

                  if (api_response.status===200) {
                    this.appList = api_response.data  
                  } else {   
                    const alertStore = useAlertStore();
                    alertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status))
                  }
               // }

              } catch (err:any) {
                  const alertStore = useAlertStore();
                  alertStore.error(new alertMessage(err.message,err.message,err.status))
              }finally{
                this.fetchAppListInProgress =false
                return this.appList
              }
    },
    
    async startApplication(app: any, skipLoadPage?:boolean) {
      //console.log('start aplication!')
      
              try {
                const appMenuStore = useAppMenuStore()
                const pageStore = usePageStore()
                appMenuStore.setCurrentMenu('')
                this.currentApp={}
                this.startAppInProgress[app.uid] = true
                const userStore = useAuthUserStore()
                if (this.applications[app.uid]!==undefined) { 
                  this.currentApp=this.applications[app.uid]
                  
                } else {
                  this.currentApp = await this.setCurrentAppById(app.uid,app.version)
                
                }
                let version = 0
                const resourceDefStore = useResourceDefinitionStore()
                if (this.currentApp.DefaultPage===undefined) {
                    //get resource links from ResourceExtended
                    const default_page_link = this.currentApp.ResourceExtended.filter((re:any)=>re.LinkType=='default-page')
                    //get from page store
                    
                    let default_page = null
                    if (default_page_link!==undefined){
                      if (default_page_link[0]?.Resource2Version) version=default_page_link[0].Resource2Version
                      default_page = resourceDefStore.getPublishedResource(default_page_link[0].Resource2,'form',version)
                      if (default_page===undefined)   default_page = await resourceDefStore.fetchPublishedResourceVersion(default_page_link[0].Resource2,'form',version)
                    }
                    this.currentApp.DefaultPage = default_page
                  }
                  if (!this.currentApp.HelpPage){
                    const help_page_link = this.currentApp.ResourceExtended.filter((re:any)=>re.LinkType=='help-page')
                    //get from page store
                
                    let help_page = null
                    version = 0
                    if (help_page_link!==undefined){
                      if (help_page_link[0]?.Resource2Version) version=help_page_link[0].Resource2Version
                      //help_page = resourceDefStore.getPublishedResource(help_page_link[0]?.Resource2,'form',version)
                      //if (help_page===undefined && help_page_link[0]?.Resource2)   help_page = await resourceDefStore.fetchPublishedResourceVersion(help_page_link[0].Resource2,'form',version)
                      if (!help_page && help_page_link[0]?.Resource2) help_page={'ResourceDefUid':help_page_link[0].Resource2,'Version':version}
                    }
                    this.currentApp.HelpPage = help_page
                
                  }
                if (this.currentApp.menuId===undefined) {
                   
                    const app_menu_link = this.currentApp.ResourceExtended.filter((re:any) =>re.LinkType=='app-menu')
                    //get from appMenu store
                    let app_menu=null
                    if (app_menu_link!==undefined){  
                       
                        app_menu = appMenuStore.getMenu(app_menu_link[0].Resource2) //TODO - WRONG RESOURCE ???
                        let type:string='0'  
                        if (app.app_type==='external')   {
                          type='e'
                        } else 
                          type='i'
                        this.currentApp.appType=type;
                        
                        this.currentApp.menuId = app_menu_link[0].Resource2
                        this.currentApp.menuVersion = app_menu_link[0].Resource2Version
                        appMenuStore.setCurrentMenu(app_menu_link[0].Resource2)
                        
                      }
                }else appMenuStore.setCurrentMenu(this.currentApp.menuId)
                
                
                //ROUTER PUSH
                if (!skipLoadPage){
                    const guid:string = new Utility().uuidv4()
                    let query_obj:any= {app_ref: app.uid,  rinstance: guid, prinstance: pageStore.currentPage?.instanceId || 0, session_id: userStore.currentUser?.sessionid}  
                          
                    if (this.currentEnv)  query_obj['env'] = this.currentEnv
      
                      this.$router.push({
                        'name': this.currentApp.DefaultPage.Definition?.ResourceExtended?.page?.Target,
                        params: {page_id: this.currentApp.DefaultPage.Definition?.uid, page_version:version },
                        query:   query_obj 
                      })

                  }
                 this.startAppInProgress[app.uid]=false

              } catch (err:any) { 
                  const alertStore = useAlertStore();
                  alertStore.error(new alertMessage(err.message,err.message,err.status, err))
                  this.startAppInProgress[app.uid]=false
                  //return false

              }
    },
    async manageResourceSubscriptions(resource: any, subscriptionType:string,actionType?:string) {

      try {
      
          let post_data:any = { 'resource': resource,'action':actionType};
          const api_main = KatalyoApi()
          const api = api_main.KatalyoApiClass
          const api_response = await api.post(this.api.server+'resource-subscriptions/'+subscriptionType+'/',post_data);   
          const alertStore = useAlertStore()

          if (api_response.status===200) {
            this.appList.map((item:any)=>{
              if (item.SubId==api_response.data?.data?.SubId || item.id==api_response.data?.data?.id) item.ActiveSub=api_response.data?.data?.ActiveSub
            })
            alertStore.success(new alertMessage(api_response.data?.msg,api_response.data?.msg,api_response.status)) 
          } else {   
           
            alertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status,"manageResourceSubsriptions function"))
          }
 

      } catch (err:any) {
          const alertStore = useAlertStore();
          alertStore.error(new alertMessage(err.message,err.message,err.status,"manageResourceSubsriptions function"))
      }finally{
        return this.appList
      }
 
    }
  }

 // return { login, logout, currentUSer };
});