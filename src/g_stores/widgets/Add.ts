import { defineStore } from "pinia";
import { useAlertStore,alertMessage } from '@/g_stores/alert';

//using options API for now (if there's a need switch to composition API)

export const useAddStore = defineStore({
  
  id: "addResource",

  state: () => ({
        
    formDefinition: {} as Record<PropertyKey, Array<any>> ,    
    formElements: {} as Record<PropertyKey, any> ,
        widgetState: {} as Record<PropertyKey, any> 
  
    }),
 
  getters: {
    getElement: (state) => {return (elementId:string) => state.formElements[elementId]},
    getState: (state) => {return (uuidRef:string) => state.widgetState[uuidRef]},
    getFormDef: (state) => {return (uuidRef:string) => state.widgetState[uuidRef].formDefinition},
    getFormData: (state) => {return (uuidRef:string) => state.widgetState[uuidRef].data},
    },

  actions: {

  registerElement(element: any,uuidRef:string) {
            
    try {
        this.formElements[uuidRef] = element
    } catch (err:any) {
      
      const alertStore = useAlertStore();
      alertStore.error(new alertMessage(err.message,err.message,err.status))

    }
},
setState(state:any,uuidRef:string) {
            
  try {
    this.widgetState[uuidRef] = state
     
  } catch (err:any) {
    
    const alertStore = useAlertStore();
    alertStore.error(new alertMessage(err.message,err.message,err.status))

  }
},
initialiseState(uuidRef:string) {
            
    try {
       
        this.widgetState[uuidRef] = {data:{}}
      } catch (err:any) {
        
        const alertStore = useAlertStore();
        alertStore.error(new alertMessage(err.message,err.message,err.status))
      }
},
}
});