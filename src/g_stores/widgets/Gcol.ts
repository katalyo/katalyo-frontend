//import { ref, computed } from "vue"; - check later if ref is needed
import { defineStore } from "pinia";
//import KatalyoApi from '../@/g_libs/kt-api';
import { alertMessage } from '@/g_stores/alert';
import { usePageStore } from '@/m_page/stores/page';
import Utility from '@/g_libs/kt-utility';
// @ts-ignore
import KatalyoPinia from '@/g_libs/mapPiniaStores.js';
//using options API for now (if there's a need switch to composition API)

export const useGridColumnStore = defineStore({
  
  id: "gridColumn",

  state: () => ({
        
        columns: {} as Record<PropertyKey, any> ,
        isOverColumn: {} as Record<PropertyKey, boolean> ,
        lastElementDragOver: {} as Record<PropertyKey, any> ,
        /*fetchRDInProgress: {} as Record<PropertyKey, boolean> ,
        resourcesLoaded: {} as Record<PropertyKey, boolean>,
        resourceFormToolbox: {} as Record<PropertyKey, any> ,
        resourceToolboxLoaded: {} as Record<PropertyKey, boolean>,
        api:{} as Record<PropertyKey,any>,*/
  
    }),
 
  getters: {
    getColumn: (state) => {return (colId:string) => state.columns[colId]},
    getIsOverColumn: (state) => {return (colId:string) => state.isOverColumn[colId]},
    getLastDragOver: (state) => {return (colId:string) => state.lastElementDragOver[colId]},
   /* getResourceForm: (state) => {return (resourceId:number) => state.resourceDefinitions[resourceId]?.form},
    getResourceFormToolbox: (state) => {return (resourceType:string) => state.resourceFormToolbox[resourceType]},
    getResourceLoaded: (state) => {return (resourceId:number) => state.resourcesLoaded[resourceId]},
    getResourceToolboxLoaded: (state) => {return (resourceType:string) => state.resourceToolboxLoaded[resourceType]},
    */

    },

  actions: {

     
    registerColumn(item: any,uuidRef:string) {
        try {

              if (this.columns[uuidRef]=== undefined) this.columns[uuidRef]= item
          
        } catch (err:any) {
          
          const alertStore =  KatalyoPinia().getStore('alert')();
          alertStore.error(new alertMessage(err.message,err.message,err.status))

        }
    },

  addWidget(olditem: any,newItem:any,parentuuidRef:string, addtoindex:number | undefined) {
            
    try {
      const uuid:string = new Utility().uuidv4()
      newItem.UuidRef = uuid
      newItem.id = uuid
      newItem.Parameters.ElementWidth = 8
      let index:number = this.columns[parentuuidRef].layout.findIndex((element:any) => element.UuidRef == olditem.UuidRef)
      if (addtoindex) index = index + addtoindex
      if (this.columns[parentuuidRef]!== undefined && index>-1) this.columns[parentuuidRef].layout.splice(index,0,newItem)
       
    } catch (err:any) {
      
      const alertStore =  KatalyoPinia().getStore('alert')();
      alertStore.error(new alertMessage(err.message,err.message,err.status))

    }
},
addLastWidget(newItem:any,uuidRef:string) {
            
  try {
    const uuid:string = new Utility().uuidv4()
    newItem.UuidRef = uuid
    newItem.id = uuid
    newItem.Parameters.ElementWidth = 8
    if (this.columns[uuidRef]!== undefined) this.columns[uuidRef].layout.push(newItem)
     
  } catch (err:any) {
    
    const alertStore =  KatalyoPinia().getStore('alert')();
    alertStore.error(new alertMessage(err.message,err.message,err.status))

  }
},
setDragOver(parentId:string,uuidRef:string,elementType:string) {
            
  try {

    this.lastElementDragOver[parentId] = {'UuidRef':uuidRef,'element':false,'placeholder':false}
    this.lastElementDragOver[parentId][elementType] = true
     
  } catch (err:any) {
    
    const alertStore =  KatalyoPinia().getStore('alert')();
    alertStore.error(new alertMessage(err.message,err.message,err.status))
  }
},
leaveElement(parentId:string,uuidRef:string,elementType:string) {
            
  try {

    if (this.lastElementDragOver[parentId]['UuidRef']===uuidRef) this.lastElementDragOver[parentId][elementType] = false
    //if (!this.lastElementDragOver[parentId].placeholder && !this.lastElementDragOver[parentId].element) this.lastElementDragOver[parentId]['UuidRef']='none' 
     
  } catch (err:any) {
    
    const alertStore =  KatalyoPinia().getStore('alert')();
    alertStore.error(new alertMessage(err.message,err.message,err.status))
  }
},
resetAll() {
            
  try {

    this.lastElementDragOver = {}
  } catch (err:any) {
    
    const alertStore =  KatalyoPinia().getStore('alert')();
    alertStore.error(new alertMessage(err.message,err.message,err.status))
  }
},
}
});