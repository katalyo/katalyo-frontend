import { defineStore } from "pinia";
import { useAlertStore,alertMessage } from '@/g_stores/alert';
import { reactive,computed } from "vue";

//composition api used here  

export const useWidgetStore = defineStore("widget",() => {

    //state variables
        
    let formDefinition:Record<PropertyKey, Array<any>> = reactive({})
    let formElements:Record<PropertyKey, any> = reactive({})
    let widgetState:Record<PropertyKey, any> = reactive({})
  

 
  //getters
    const getElement = computed(() => {return (elementId:string) => formElements[elementId]})
    const getState = computed(() => {return (uuidRef:string) => widgetState[uuidRef]})
    const getFormDef = computed(() => {return (uuidRef:string) => widgetState[uuidRef].formDefinition})
    const getFormData = computed(() => {return (uuidRef:string) => widgetState[uuidRef].data})



    //actions
    function registerElement(element: any,uuidRef:string) {
            
        try {
            formElements[uuidRef] = element
        } catch (err:any) {
      
        const alertStore = useAlertStore();
        alertStore.error(new alertMessage(err.message,err.message,err.status))

        }
    }
    function setState(state:any,uuidRef:string) {
                
    try {
        widgetState[uuidRef] = state
        
    } catch (err:any) {
        
        const alertStore = useAlertStore();
        alertStore.error(new alertMessage(err.message,err.message,err.status))

    }
    }
    function initialiseState(uuidRef:string) {
                
        try {
        
            widgetState[uuidRef] = {data:{}}
        } catch (err:any) {
            
            const alertStore = useAlertStore();
            alertStore.error(new alertMessage(err.message,err.message,err.status))
        }
    }
    return { formDefinition, formElements, widgetState, getElement,getState,getFormDef,getFormData,registerElement,setState,initialiseState }

});