//import { ref, computed } from "vue"; - check later if ref is needed
import { defineStore } from "pinia";
import { useCookies } from "vue3-cookies";
import KatalyoApi from '@/g_libs/kt-api';
import { useAlertStore,alertMessage } from '@/g_stores/alert';
import { useDatasetStore } from '@/g_stores/dataset';
import { useAuthUserStore } from '@/m_auth/stores/authUser';
//using options API for now (if there's a need switch to composition API)

export const useTaskStore = defineStore({
  
  id: "task",

  state: () => ({
        taskWidgets:{} as Record<PropertyKey,any>,
        childWidgets:{} as Record<PropertyKey,Array<string>>,
        startTaskInProgress: {} as Record<PropertyKey,boolean>,
        executeTaskInProgress: {} as Record<PropertyKey,boolean>,
        api:{} as Record<PropertyKey,any>,
    }),
 
  getters: {
    getTaskWidget: (state) => {return (taskId:string)=> state.taskWidgets[taskId]},
    getStartTaskInProgress: (state) => {return (taskId:string)=> state.startTaskInProgress[taskId]},
    getTaskStatus: (state) => {return (taskId:string)=> state.taskWidgets[taskId]?.status},
    getTaskStatusResponseMsg: (state) => {return (taskId:string)=> state.taskWidgets[taskId]?.taskProcResponse.msg},
    getTaskProcType: (state) => {return (taskId:string)=> state.taskWidgets[taskId]?.taskExeState?.procType},
    getTaskId: (state) => {return (taskId:string)=> state.taskWidgets[taskId]?.Parameters?.taskDefId?.id},
    getChildWidgets: (state) => {return (taskId:string)=> state.childWidgets[taskId]},
    getTaskExeState: (state) => {return (taskId:string)=> state.taskWidgets[taskId]?.taskExeState},
    getTaskExeInstanceId: (state) => {return (taskId:string)=> state.taskWidgets[taskId]?.taskExeState?.taskExecuteInstance[0]?.id},
    getTaskFormDefinition: (state) => {return (taskId:string)=> state.taskWidgets[taskId]?.taskExeState?.widgets},
    },

  actions: {
    

        registerWidget(fd: Record<PropertyKey,any>) {
            
            try {

                this.taskWidgets[fd.UuidRef] = fd
                this.childWidgets[fd.UuidRef] = []
              
            } catch (err:any) {
                const alertStore = useAlertStore()
                alertStore.error(new alertMessage(err.message,err.message,err.status))

            }
        },


      registerChildWidget(UuidRef:string,parentId:string) {
            
            try {

                if (this.childWidgets[parentId]===undefined) this.childWidgets[parentId]=[]
                this.childWidgets[parentId].push(UuidRef)
              
            } catch (err:any) {
                const alertStore = useAlertStore()
                alertStore.error(new alertMessage(err.message,err.message,err.status))

            }
        },

       async startTask(widgetId:string) {
            
            try {

                if (this.taskWidgets[widgetId].taskExeState===undefined)
                {
                  const taskId = this.taskWidgets[widgetId]?.Parameters?.taskDefId?.id
                  const outcomeId = this.taskWidgets[widgetId]?.outcomeId || 0
                  const previousTaskId = this.taskWidgets[widgetId]?.previousTaskId || 0
                  this.startTaskInProgress[widgetId]=true
                  const api_main = KatalyoApi()
                  const api = api_main.KatalyoApiClass
                  const api_response:any = await api.get(this.api.server+'start-task/'+taskId+'/'+outcomeId+'/'+previousTaskId+'/');   

                  
                  if (api_response.status===200)
                  {
                    const task = api_response.data
                    this.taskWidgets[widgetId].taskExeState = task
                  }else{
                    //store error in alertStore
                    const alertStore = useAlertStore()

                    alertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status))
                  }
                }

            } catch (err:any) {
                const alertStore = useAlertStore()
                alertStore.error(new alertMessage(err.message,err.message,err.status))

            }finally{
              this.startTaskInProgress[widgetId]=false
              return this.taskWidgets[widgetId].taskExeState
            }
    },
     async executeTask(widgetId:string) {
            
            try {

                  
                  this.executeTaskInProgress[widgetId]=true
                  
                  //form validation

                  const datasetStore = useDatasetStore()
                  let is_invalid:boolean = false
                  let form_data:any = {taskData:{},outcome:0}
                  for (let i=0;i<this.childWidgets[widgetId].length;i++)
                  {
                    is_invalid=datasetStore.validateForm(this.childWidgets[widgetId][i])
                    if (is_invalid) break
                    else{
                      form_data.taskData[datasetStore.getWidgetPresentationId(this.childWidgets[widgetId][i])]=datasetStore.getFormData(this.childWidgets[widgetId][i])
                    }
                  }
                  if (!is_invalid)
                  {
                    const api_main = KatalyoApi()
                    const api = api_main.KatalyoApiClass
                    
                    const taskId:number = this.taskWidgets[widgetId].taskExeState?.taskExecuteInstance[0].id
                    const userStore = useAuthUserStore()
                    const lang:number = userStore.getCurrentUser?.lang?.id
                    
                    const api_response:any = await api.post(this.api.server+'execute-task/'+taskId+'/'+lang+'/',form_data);   

                    
                    if (api_response.status===200)
                    {
                      
                      this.taskWidgets[widgetId].taskExeResponse = api_response.data
                    }else{
                      //store error in alertStore
                      const alertStore = useAlertStore()

                      alertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status))
                    }
                  }else{
                    
                    const alertStore = useAlertStore()
                    alertStore.error(new alertMessage('Form validation failed','Form validation failed',500))

                  }

            } catch (err:any) {
                const alertStore = useAlertStore()
                alertStore.error(new alertMessage(err.message,err.message,err.status))

            }finally{
              this.executeTaskInProgress[widgetId]=false
            }
    },
  }
});