//import { ref, computed } from "vue"; - check later if ref is needed
import { defineStore } from "pinia";
import { useCookies } from "vue3-cookies";
//import { useAuthUserStore } from '@/m_auth/stores/authUser';
import KatalyoApi from '@/g_libs/kt-api';
import { useAlertStore,alertMessage } from '@/g_stores/alert';
//using options API for now (if there's a need switch to composition API)

class default_menu
   {
    DefaultMenu:Array<Record<PropertyKey,any>>=[];
    constructor (){
    this.DefaultMenu = [
       
        {
            "s_menus_id": 230,
            "s_menus_Name": "Home page",
            "s_menus_MenuType": 1,
            "s_menus_Params": {
                "pageid": "1_66",
                "pagestate": "app.navbar.home"
            },
            "s_menus_ParentItem": 230,
            "s_menus_Icon": "fa fa-home",
            "s_menus_Navbar": 230,
            "s_menus_Order": 1,
            "s_menus_Active": true,
            "s_menus_TargetPage_id": 2,
            "s_menus_MenuPosition": "navbar",
            "s_menus_AlwaysNew": null,
            "s_menus_ResourceDefId_id": 359,
            "s_menus_Target": "1_66",
            "s_menus_uid": null,
            "s_menus_guid": null,
            "s_pages_id": 2,
            "s_pages_Target": "app.navbar.home",
            "s_pages_Params": null,
            "s_pages_ResourceDefId_id": 66,
            "s_pages_PageType_id": 116,
            "s_resourcedef_id": 359,
            "s_resourcedef_ResourceType_id": 7,
            "s_resourcedef_Organisation_id": 1,
            "s_resourcedef_Parameters": {},
            "s_resourcedef_Version": 1,
            "s_resourcedef_ResourceSubType": "standard",
            "s_resourcedef_Active": true,
            "s_resourcedef_uid": "1_359"
        },
        {
            "s_menus_id": 200,
            "s_menus_Name": "Settings",
            "s_menus_MenuType": 2,
            "s_menus_Params": {
                "pageid": null
            },
            "s_menus_ParentItem": 200,
            "s_menus_Icon": "fas fa-cog",
            "s_menus_Navbar": 200,
            "s_menus_Order": 5,
            "s_menus_Active": true,
            "s_menus_TargetPage_id": 21,
            "s_menus_MenuPosition": "navbarright",
            "s_menus_ResourceDefId_id": 359,
            "s_menus_Target": null,
            "s_menus_uid": null,
            "s_menus_guid": null,
            "s_pages_id": 21,
            "s_pages_Target": "app.login",
            "s_pages_Params": null,
            "s_pages_ResourceDefId_id": 65,
            "s_pages_PageType_id": null,
            "s_resourcedef_id": 359,
            "s_resourcedef_Status_id": 17,
            "s_resourcedef_Private": false,
            "s_resourcedef_ResourceType_id": 7,
            "s_resourcedef_Organisation_id": 1,
            "s_resourcedef_Parameters": {},
            "s_resourcedef_Version": 1,
            "s_resourcedef_ResourceSubType": "standard",
            "s_resourcedef_Active": true,
            "s_resourcedef_uid": "1_359",
            "SideMenus":[
              {
                "s_menus_id": 208,
                "s_menus_Name": "Users",
                "s_menus_MenuType": 1,
                "s_menus_Params": {
                    "pageid": "1_86",
                    "pagestate": "app.navbar.users"
                },
                "s_menus_ParentItem": 200,
                "s_menus_Icon": "far fa-user",
                "s_menus_Navbar": 200,
                "s_menus_Order": 1,
                "s_menus_Active": true,
                "s_menus_TargetPage_id": 56,
                "s_menus_MenuPosition": "sidemenu",
                "s_menus_ResourceDefId_id": 359,
                "s_menus_Target": "1_86",
                "s_menus_uid": null,
                "s_menus_guid": null,
                "s_pages_id": 56,
                "s_pages_Target": "app.navbar.users",
                "s_pages_Params": null,
                "s_pages_ResourceDefId_id": 86,
                "s_pages_PageType_id": 116,
                "s_resourcedef_id": 359,
                "s_resourcedef_ResourceType_id": 7,
                "s_resourcedef_Organisation_id": 1,
                "s_resourcedef_Parameters": {},
                "s_resourcedef_ResourceSubType": "standard",
                "s_resourcedef_Active": true,
                "s_resourcedef_uid": "1_359"
              },
              {
                "s_menus_id": 207,
                "s_menus_Name": "Groups",
                "s_menus_MenuType": 1,
                "s_menus_Params": {
                    "pageid": "1_85",
                    "pagestate": "app.navbar.groups"
                },
                "s_menus_ParentItem": 200,
                "s_menus_Icon": "fas fa-users",
                "s_menus_Navbar": 200,
                "s_menus_Order": 2,
                "s_menus_Active": true,
                "s_menus_TargetPage_id": 53,
                "s_menus_MenuPosition": "sidemenu",
                "s_menus_AlwaysNew": null,
                "s_menus_ResourceDefId_id": 359,
                "s_menus_Target": "1_85",
                "s_menus_VersionFrom": 1,
                "s_menus_VersionTo": 1,
                "s_menus_WorkVersion": 1,
                "s_menus_uid": null,
                "s_menus_guid": null,
                "s_pages_id": 53,
                "s_pages_Target": "app.navbar.groups",
                "s_pages_Params": null,
                "s_pages_ResourceDefId_id": 85,
                "s_pages_PageType_id": 116,
                "s_resourcedef_id": 359,
                "s_resourcedef_ResourceType_id": 7,
                "s_resourcedef_Organisation_id": 1,
                "s_resourcedef_Parameters": {},
                "s_resourcedef_ResourceSubType": "standard",
                "s_resourcedef_Active": true,
                "s_resourcedef_uid": "1_359"
            },
            
            {
                "s_menus_id": 210,
                "s_menus_Name": "Codes",
                "s_menus_MenuType": 1,
                "s_menus_Params": {
                    "pageid": "1_361",
                    "pagestate": "app.navbar.codes.list"
                },
                "s_menus_ParentItem": 200,
                "s_menus_Icon": "fas fa-barcode",
                "s_menus_Navbar": 200,
                "s_menus_Order": 3,
                "s_menus_Active": true,
                "s_menus_TargetPage_id": 58,
                "s_menus_MenuPosition": "sidemenu",
                "s_menus_AlwaysNew": null,
                "s_menus_ResourceDefId_id": 359,
                "s_menus_Target": "1_361",
                "s_menus_uid": null,
                "s_menus_guid": null,
                "s_pages_id": 58,
                "s_pages_Target": "app.navbar.codes.list",
                "s_pages_Params": null,
                "s_pages_ResourceDefId_id": 361,
                "s_pages_ChangedBy_id": null,
                "s_pages_ChangedDateTime": null,
                "s_pages_CreatedBy_id": null,
                "s_pages_CreatedDateTime": null,
                "s_pages_PageType_id": 116,
                "s_resourcedef_id": 359,
                "s_resourcedef_Status_id": 17,
                "s_resourcedef_Private": false,
                "s_resourcedef_ResourceType_id": 7,
                "s_resourcedef_Organisation_id": 1,
                "s_resourcedef_NewVersion": 1,
                "s_resourcedef_Parameters": {},
                "s_resourcedef_ResourceSubType": "standard",
                "s_resourcedef_Active": true,
                "s_resourcedef_uid": "1_359"
            },
            {
              "s_menus_id": 228,
              "s_menus_Name": "Permissions",
              "s_menus_MenuType": 1,
              "s_menus_Params": {
                  "pageid": "1_86",
                  "pagestate": "app.navbar.appsettings"
              },
              "s_menus_ParentItem": 200,
              "s_menus_Icon": "fas fa-lock",
              "s_menus_Navbar": 200,
              "s_menus_Order": 1,
              "s_menus_Active": true,
              "s_menus_TargetPage_id": 16,
              "s_menus_MenuPosition": "sidemenu",
              "s_menus_ResourceDefId_id": 359,
              "s_menus_Target": "1_86",
              "s_menus_uid": null,
              "s_menus_guid": null,
              "s_pages_id": 56,
              "s_pages_Target": "app.navbar.appsettings",
              "s_pages_Params": null,
              "s_pages_ResourceDefId_id": 86,
              "s_pages_PageType_id": 116,
              "s_resourcedef_id": 359,
              "s_resourcedef_ResourceType_id": 7,
              "s_resourcedef_Organisation_id": 1,
              "s_resourcedef_Parameters": {},
              "s_resourcedef_ResourceSubType": "standard",
              "s_resourcedef_Active": true,
              "s_resourcedef_uid": "1_359",
          },
         
         
          {
            "s_menus_id": 228,
            "s_menus_Name": "Community",
            "s_menus_MenuType": 1,
            "s_menus_Params": {
                "pageid": "1_86",
                "pagestate": "app.navbar.organisation"
            },
            "s_menus_ParentItem": 200,
            "s_menus_Icon": "fas fa-bank",
            "s_menus_Navbar": 200,
            "s_menus_Order": 1,
            "s_menus_Active": true,
            "s_menus_TargetPage_id": 16,
            "s_menus_MenuPosition": "sidemenu",
            "s_menus_ResourceDefId_id": 359,
            "s_menus_Target": "1_86",
            "s_menus_uid": null,
            "s_menus_guid": null,
            "s_pages_id": 56,
            "s_pages_Target": "app.navbar.organisation",
            "s_pages_Params": null,
            "s_pages_ResourceDefId_id": 86,
            "s_pages_PageType_id": 116,
            "s_resourcedef_id": 359,
            "s_resourcedef_ResourceType_id": 7,
            "s_resourcedef_Organisation_id": 1,
            "s_resourcedef_Parameters": {},
            "s_resourcedef_ResourceSubType": "standard",
            "s_resourcedef_Active": true,
            "s_resourcedef_uid": "1_359",
        },
       
          ]
        }
      ]
    }
    clearMenu(){
      this.DefaultMenu = []
    }
  }

export const useAppMenuStore = defineStore({
  
  id: "appMenu",

  state: () => ({
        menus: {} as Record<PropertyKey,any>,
        currentMenuKey: '' as string,
        fetchMenusInProgress:false,
        pageMenuLarge:true,
        showDropdown:[] as Array<boolean>,
        api:{} as Record<PropertyKey,any>,
    }),
 
  getters: {
    getMenu: (state) => {return (menuId:string)=> state.menus[menuId]?.menus},
    getMenuProperties: (state) => {return (menuId:string)=> state.menus[menuId]?.properties},
    getCurrentMenu: (state) => {return state.menus[state.currentMenuKey]?.menus},
    getCurrentMenuProperties: (state) => {return state.menus[state.currentMenuKey]?.properties},
    getCurrentMenuId: (state) => {return state.currentMenuKey},
    getPageMenuSize: (state) => {return state.pageMenuLarge},
    getCurrentMenuHeight: (state) => {return state.menus[state.currentMenuKey]?.menuHeight},
    getCurrentMenuParameters: (state) => {return state.menus[state.currentMenuKey]?.parameters},
    getMenusInProgress: (state) => {return state.fetchMenusInProgress},
    getSideMenus: (state) => {return (parentId:number)=> state.menus[state.currentMenuKey]?.menus.filter((mi:any)=>mi.s_menus_MenuPosition=='navbarrightc' && mi.s_menus_ParentItem==parentId)},
    
    },

  actions: {
    async fetchMenuExe(menuId:string,type:string,version:number,menuType?:string | undefined, donotSetCurrentMenu?:boolean | undefined) {
      let menu:Record<PropertyKey,any> = {menuHeight:'50px'}
      try {
        if (!menuType) menuType='app'
            if (menuId!==undefined) {
              menu = this.menus[menuId]

              if (menuType==='app' && !donotSetCurrentMenu) this.fetchMenusInProgress=true

                const api_main = KatalyoApi()
                const api = api_main.KatalyoApiClass
                const api_response:any = await api.get(this.api.server+'get-menus-exe/'+menuId+'/'+type+'/'+version+'/');  
                
                if (api_response?.status===200) {
                    menu = api_response.data
                    if (! donotSetCurrentMenu) {
                      this.menus[menuId] = menu
                      if (menuType==='app') this.setCurrentMenu(menuId)
                    }
                        
                } else {
                    const alertStore = useAlertStore();
                    alertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status))
                    
                }

                this.fetchMenusInProgress=false
                let container:any = document.getElementById('app-menu-id')
                if (! donotSetCurrentMenu && container?.offsetHeight)   this.menus[menuId].menuHeight=container?.offsetHeight.toString()+'px'
            }
        } catch (err:any) {
            this.fetchMenusInProgress=false
            const alertStore = useAlertStore();
            alertStore.error(new alertMessage(err.message,err.message,err.status))

      } finally {
             
              return menu 
              
      }      

    },

    async fetchMenu(menuId:string,lang:number,type:string,version:number) {

      try {
            if (menuId!==undefined)
            {
              this.fetchMenusInProgress=true
              const api_main = KatalyoApi()
              const api = api_main.KatalyoApiClass
              //const api_response:any = await api.get(this.api.server+'get-menus/'+lang+'/'+type+'/'+menuId+'/'+version+'/');   
              const api_response:any = await api.get(this.api.server+'get-menus-exe/'+menuId+'/'+type+'/'+version+'/');   
                  
                
                if (api_response?.status===200)
                {
                  const menu = api_response.data?.menus
                  this.fetchMenusInProgress=false
                  this.menus[menuId] = menu
                  this.setCurrentMenu(menuId)
                }else{
                  //store error in alertStore
                  const alertStore = useAlertStore();
                  alertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status))
                  this.fetchMenusInProgress=false

                }
            }

        } catch (err:any) {
            this.fetchMenusInProgress=false
            const alertStore = useAlertStore();
            alertStore.error(new alertMessage(err.message,err.message,err.status))
               
      }finally{

              return this.menus[menuId]
      }      

    },
    async fetchMenuByAppId(appId:string,lang:number,type:string,version:number) {
            
            try {

                this.fetchMenusInProgress=true
                const api_main = KatalyoApi()
                const api = api_main.KatalyoApiClass
                const api_response:any = await api.get(this.api.server+'get-menus/'+lang+'/'+type+'/'+appId+'/'+version+'/');   
              
                if (api_response.status===200)
                {
                  const menu = api_response.data
                  this.fetchMenusInProgress=false
                  this.menus[appId] = menu
                  this.setCurrentMenu(appId)
                
                }else{
                  //store error in alertStore
                  const alertStore = useAlertStore();
                  alertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status))
                  

                }

            } catch (err:any) {
                this.fetchMenusInProgress=false
                const alertStore = useAlertStore();
                alertStore.error(new alertMessage(err.message,err.message,err.status))
               
                
              
            }finally{
              
              return this.menus[appId]
            }
    },
    changePageMenuSize (){
      this.pageMenuLarge=!this.pageMenuLarge
    },
    setCurrentMenu(menuId:string) {
      this.currentMenuKey = menuId
    },
    setDefaultMenu() {
      if (!this.currentMenuKey ||  this.currentMenuKey=='0')
      {
        if (!this.menus['0']) this.menus['0'] = {menus:new default_menu().DefaultMenu,properties:{}}
        
      }
      if (this.currentMenuKey!='0') this.setCurrentMenu('0')
    },
    clearCurrentMenu() {
      this.currentMenuKey = ''

    },
    hideDropwdown(idx:number) {
      this.showDropdown[idx] = false

    },
    hideAllDropwdowns() {
      this.showDropdown.length=0

    }
    
  }

 // return { login, logout, currentUSer };
});