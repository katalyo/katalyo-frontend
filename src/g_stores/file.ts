//import { ref, computed } from "vue"; - check later if ref is needed
import { defineStore } from "pinia";
import { useCookies } from "vue3-cookies";
import KatalyoApi from '@/g_libs/kt-api';
import { useAlertStore,alertMessage } from '@/g_stores/alert';
import { useDatasetStore } from '@/g_stores/dataset';
import { useAuthUserStore } from '@/m_auth/stores/authUser';

import {  reactive,toRef,ref,computed,defineAsyncComponent } from "vue";

export const useFileStore = defineStore({
  
  id: "file",

  state: () => ({
        files:{} as Record<PropertyKey,any>,
        dowloadFileInProgress: {} as Record<PropertyKey,boolean>,
        api:{} as Record<PropertyKey,any>,
    }),
 
  getters: {
    getFileUrl: (state) => (fileDefId:number,fileId:string)=> {

       if (state.files[fileDefId] ===undefined) return null
       else return state.files[fileDefId][fileId]
        
      },

    },

  actions: {

    async fileMetaUpdate (fileId:number,fileMetaDefId:number,publishId:number,taskInstanceId:string,prevTaskId:string,parentDatasetDefId:string,parentDatasetId:number,fileResourceData:any,fd:any,page_id:string) 
    {
    
      var http_method="POST";
      
      var url='execute-widget-action/'//'file-meta-data-update/';
      if (prevTaskId==undefined) prevTaskId='0'
      fd.guid=fd.UuidRef
      let post_data = {'command':{'action':'update_meta_data','resource':'file','widget':'ktUploadFile','resource_id':fileMetaDefId,'widgetFd':fd},'data':{'fileId':fileId,'fileMetaDefId':fileMetaDefId,'publishId':publishId,'taskInstanceId':taskInstanceId,'prevTaskId':prevTaskId,'parentDatasetDefId':parentDatasetDefId,'parentDatasetId':parentDatasetId,'fileResourceData':fileResourceData,'pageId':page_id}};
        
      const api_main = KatalyoApi()
      const api = api_main.KatalyoApiClass
      let api_response:any = await api.post(this.api.server + url,post_data)        
      if (api_response.status === 200 || api_response.status === 201 ) {
           return api_response                       
       } else {               
                  const alertStore = useAlertStore()
                  alertStore.error(new alertMessage(api_response.data?.msg,api_response.data?.trace,api_response.status,'Api','fileStore.fileMetaupdate'))
                  return api_response          
        }
              
          //deferred.reject({msg:'File metadata update error (fn fileMetaUpdate) --> status = '+response.status+' statusText = '+response.statusText+' '+ response.data,status:response.status});      
  
    },

      async downloadFile(fileDefId: string,fileId:string,fileType:string) {
              try {

                if (this.files[fileDefId]===undefined)    {
                    this.files[fileDefId]={}
                    this.files[fileDefId][fileId] = null 
                }

                
                  this.dowloadFileInProgress[fileDefId]=true
                  const api_main = KatalyoApi()
                  const api = api_main.KatalyoApiClass
                  let api_response:any = await api.get(this.api.server+'download-file/'+fileDefId+'/'+fileId+'/');   
                  console.log(api_response)
                  
                  if (api_response.status===200)  {

                    let file:any
                    if (api_response?.data?.length) file = api_response.data
                    else file = await api_response.blob()
                    this.files[fileDefId][fileId] = URL.createObjectURL(new Blob([file],{type:fileType}))
                  } else{                
                    const alertStore = useAlertStore()
                    alertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status))          
                  }
                

            } catch (err:any) {
                const alertStore = useAlertStore()
                alertStore.error(new alertMessage(err.message,err.message,err.status))

            }finally{
              this.dowloadFileInProgress[fileDefId]=false
              return true
            }
            
        },
        async downloadFileWithParams(fileDefId: string,fileId:string,fileType:string,params:any) {
          try {

            if (this.files[fileDefId]===undefined)    {
                this.files[fileDefId]={}
                this.files[fileDefId][fileId] = null 
            }

            
              this.dowloadFileInProgress[fileDefId]=true
              const api_main = KatalyoApi()
              const api = api_main.KatalyoApiClass
              let api_response:any = await api.post(this.api.server+'download-file/'+fileDefId+'/'+fileId+'/',params);   
           
              
              if (api_response.status===200)  {

                let file:any
                if (api_response?.data?.length) file = api_response.data
                else file = await api_response.blob()
                if (!fileType) fileType = file.type
                this.files[fileDefId][fileId] = URL.createObjectURL(new Blob([file],{type:fileType}))
              } else{                
                const alertStore = useAlertStore()
                alertStore.error(new alertMessage(api_response.data,api_response.data,api_response.status))          
              }
            

        } catch (err:any) {
            const alertStore = useAlertStore()
            alertStore.error(new alertMessage(err.message,err.message,err.status))

        }finally{
          this.dowloadFileInProgress[fileDefId]=false
          return true
        }
        
    }


  }
});