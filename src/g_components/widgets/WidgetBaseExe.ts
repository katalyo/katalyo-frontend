    import { resolveDynamicComponent, type Ref } from "vue";
    import { usePageStore } from '@/m_page/stores/page';
    import { useAuthUserStore } from '@/m_auth/stores/authUser';
    import { useResourceFormStore } from '@/m_resource/stores/resourceForm';
    import { useResourceFlowStore } from '@/m_resource/stores/resourceFlow';
    import  configuration  from '@/config';
    import Utility from '@/g_libs/kt-utility';
 

    export default function WidgetBaseExe (_fd:Record<PropertyKey,any>,parentId?:string,setWidgetDataLoaded?:boolean,setWidgetLoaded?:boolean,widgetType?:string)
    {
        const fd = _fd.value
        const authStore = useAuthUserStore()
        const flowStore = useResourceFlowStore()
        const me = fd
        let defaultWidget:string = 'WidgetDefault'
        if (widgetType=='placeholder') defaultWidget = 'WidgetDefaultPlaceholder'
        if (setWidgetLoaded===undefined) setWidgetLoaded=true
        if (setWidgetDataLoaded===undefined) setWidgetDataLoaded=true

        const getComponentName = (name:string,compType?:string) => {
            if (compType!==undefined) defaultWidget='WidgetDefaultPlaceholder'
            if (typeof resolveDynamicComponent(name) !== 'string') return name
            else return defaultWidget
        }

        const pageStore = usePageStore()
        const formStore = useResourceFormStore()
        
        if (setWidgetLoaded) pageStore.setWidgetLoaded(fd?.UuidRef)
        if (setWidgetDataLoaded && parentId) formStore.setWidgetDataLoaded(pageStore.currentPage.instanceId,me?.UuidRef,parentId.toString())
        
      
        const createCssLink = () =>
        {
            const utility:Utility = new Utility()
            return utility.createCssLink(configuration().api?.publishurl,fd?.UuidRef)
          
        }
        
        let formStyleId:string | undefined = parentId
        if (me.reduceParentId) formStyleId = (formStyleId || '').substring(0,36)

        const toggleVisibility = async (data:any)=>
        {
            me.ShowField = !me.ShowField
            return {status_message:'ok',status:0,actionIdx:data.flowData?.actionIdx,result:me?.ShowField}
        }

        
        const showField = async (data:any)=>
        {
            me.ShowField = true
            return {status_message:'ok',status:0,actionIdx:data.flowData?.actionIdx,result:me?.ShowField}
        }

        
        const hideField = async (data:any)=>
        {
            me.ShowField = false
            return {status_message:'ok',status:0,actionIdx:data.flowData?.actionIdx,result:me?.ShowField}
        }

        const processEvents = async (data:any,event:string,sourceWidget:string)=>{
            let flowResultError:Array<any> = []
            let flowResult:any = null
            for (let i=0;i<me?.Parameters?.events?.length;i++)
            {
                if (sourceWidget===me.UuidRef && event===pageStore.currentPage.instanceId+'-'+me.Parameters.events[i].EventName+'-'+me.Parameters.events[i].EventWidgetId)
                {
                    const utility:Utility = new Utility()
                    me.Parameters.events[i].id = utility.uuidv4()
                    me.Parameters.events[i].eventData = data
                    flowStore.addFlow(me?.Parameters.events[i])
                    flowResult = await flowStore.executeFlow(me.Parameters.events[i].id)
                    if (flowResult?.status) flowResultError.push(flowResult?.error)
        
                } 
            }
            return {res:flowResult,error:flowResultError}
        }
    
        let actions = {'toggleVisibility':toggleVisibility,'showField':showField,'hideField':hideField,'processEvents':processEvents}
        me.actions = actions
        pageStore.addWidgetToPage(me.UuidRef,me)

        return {createCssLink,authStore,me,getComponentName, formStyleId}
        
    }