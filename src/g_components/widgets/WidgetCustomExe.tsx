import { defineComponent, compile, h } from "vue/dist/vue.esm-bundler.js"

export default defineComponent({
  name: "WidgetCustomExe",
  props: {
    inData: Object,
    widgetTemplate:String
  },
  setup(props:any) {
    // return the render function
    if (props.widgetTemplate) return () => h(compile(props.widgetTemplate), props.inData)
    else return () => h('div')
  }
})
